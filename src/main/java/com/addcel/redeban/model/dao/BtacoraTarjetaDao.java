/**
 * 
 */
package com.addcel.redeban.model.dao;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.redeban.model.vo.TransaccionResVO;
import com.addcel.redeban.model.vo.UsuarioVO;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;


/**
 * @author ELopez
 *
 */
public class BtacoraTarjetaDao extends SqlMapClientDaoSupport {
	
	private static Logger logger = LoggerFactory.getLogger(BtacoraTarjetaDao.class);
	
	public int insertBtacoraTarjeta(TransaccionResVO transVO, SolicitudCompra solicitudCompra, UsuarioVO usuario) {
		Integer status = 0;
		logger.info(Constantes.LOG_INICIO_GUARDANDO_BITACORA_TARJETA_BD, solicitudCompra.getLogin());
		try {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("id_transaccion_pago", transVO.getIdTransaccion());
			map.put("nombre_usuario", solicitudCompra.getIdUsuario());
			map.put("tarjeta", solicitudCompra.getInfoMedioPago().getNumTarjeta());
			map.put("monto_total", solicitudCompra.getInfoCompra().getMontoTotal());
			getSqlMapClientTemplate().insert("insertBtacoraTarjeta", map);
			logger.info(Constantes.LOG_FIN_GUARDANDO_BITACORA_TARJETA_BD+"["+transVO.getIdTransaccion()+"] ["+solicitudCompra.getUsuario().getLogin()+"]");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

}