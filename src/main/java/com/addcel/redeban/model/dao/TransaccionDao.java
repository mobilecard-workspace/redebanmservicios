package com.addcel.redeban.model.dao;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.redeban.model.vo.CompraDirectaVO;
import com.addcel.redeban.model.vo.ExperianUserVO;
import com.addcel.redeban.model.vo.TransaccionResVO;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;

public class TransaccionDao extends SqlMapClientDaoSupport{
		
	private static final Logger logger =  LoggerFactory.getLogger(TransaccionDao.class);
	
	public int insertarTransaccion(SolicitudCompra solicitudCompra){
		logger.info(Constantes.LOG_INICIO_INSERTA_TRANSACCION+"["+solicitudCompra.getLogin()+"]");
		int resp = 0;
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		try{
			if(logger.isDebugEnabled()){
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [MONTO] : "+solicitudCompra.getInfoCompra().getMontoTotal());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [CONCEPTO] : "+solicitudCompra.getInfoCompra().getConcepto());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [ID APLICACION] : "+solicitudCompra.getIdAplicacion());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [ID PRODUCTO] : "+solicitudCompra.getInfoCompra().getIdProducto());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [ID SERVICIO] : "+solicitudCompra.getInfoCompra().getIdServicio());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [TIPO] : "+solicitudCompra.getInfoCompra().getTipo());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [CUOTAS] : "+solicitudCompra.getInfoCompra().getCantidadCuotas());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [REFERENCIA] : "+solicitudCompra.getInfoCompra().getReferencia());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [IVA VALOR] : "+solicitudCompra.getInfoCompra().getInfoImpuestos().getIvaValor());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [IVA] : "+solicitudCompra.getInfoCompra().getInfoImpuestos().getIva());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [MEDIO] : "+solicitudCompra.getPlataforma() + 
						" " + solicitudCompra.getDispositivo());
				logger.debug(Constantes.LOG_INSERTA_TRANSACCION_DATOS+" [MONEDA] : "+solicitudCompra.getInfoCompra().getMoneda());
			}			
			parametros.put("respuesta", "");
			parametros.put("login", solicitudCompra.getLogin());
			parametros.put("password", solicitudCompra.getPassword());
			parametros.put("montoTotal", solicitudCompra.getInfoCompra().getMontoTotal());
			parametros.put("idProducto", solicitudCompra.getInfoCompra().getIdProducto());
			parametros.put("idServicio", solicitudCompra.getInfoCompra().getIdServicio());
			parametros.put("tipo", solicitudCompra.getInfoCompra().getTipo());
			parametros.put("cantidadCuotas", solicitudCompra.getInfoCompra().getCantidadCuotas());
			parametros.put("referencia", solicitudCompra.getInfoCompra().getReferencia());
			parametros.put("ivaValor", solicitudCompra.getInfoCompra().getInfoImpuestos().getIvaValor());
			parametros.put("iva", solicitudCompra.getInfoCompra().getInfoImpuestos().getIva());
			parametros.put("medio", solicitudCompra.getPlataforma() + " " + solicitudCompra.getDispositivo());
			parametros.put("moneda", solicitudCompra.getInfoCompra().getMoneda());
			parametros.put("idAplicacion", solicitudCompra.getIdAplicacion());
			getSqlMapClientTemplate().queryForObject("insertarTransaccion", parametros);		
			solicitudCompra.setRespuesta((String)parametros.get("respuesta"));
			logger.info(Constantes.LOG_RESP_INSERTA_TRANSACCION+"["+solicitudCompra.getLogin()+"] : "+parametros.get("respuesta"));
		}catch(Exception e){
			resp = 102;
			logger.error(Constantes.LOG_ERROR_INSERTA_TRANSACCION+"["+solicitudCompra.getLogin()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_INSERTA_TRANSACCION+"["+solicitudCompra.getLogin()+"]");
		return resp;
	}
	
	public int actualizaCancelacion(SolicitudCompraRespuesta compraRespuesta){
		logger.info(Constantes.LOG_INICIO_ACTUALIZA_CANCELACION+"["+compraRespuesta.getIdTransaccion()+"]");
		int resp = 0;
		try{
			getSqlMapClientTemplate().queryForObject("actualizaCancelacion", compraRespuesta);
			logger.info(Constantes.LOG_RESP_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"] : "
					+compraRespuesta.getRespuesta());
		}catch(Exception e){
			resp = 102;
			logger.error(Constantes.LOG_ERROR_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"]");
		return resp;
	}
	
	public int actualizarTransaccion(SolicitudCompraRespuesta compraRespuesta){
		logger.info(Constantes.LOG_INICIO_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"]");
		int resp = 0;
		try{
			getSqlMapClientTemplate().queryForObject("actualizarTransaccion", compraRespuesta);
			logger.info(Constantes.LOG_RESP_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"] : "
					+compraRespuesta.getRespuesta());
		}catch(Exception e){
			resp = 102;
			logger.error(Constantes.LOG_ERROR_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_ACTUALIZA_TRANSACCION+"["+compraRespuesta.getIdTransaccion()+"]");
		return resp;
	}
	
	public int getDatosIntegrador(TransaccionResVO transVO){
		logger.info(Constantes.LOG_INICIO_CONSULTA_DATOS_INTEGRADOR+"["+transVO.getIdTransaccion()+"]");
		int resp = 0;
		try{
			getSqlMapClientTemplate().queryForObject("getDatosIntegrador", transVO);
			logger.info(Constantes.LOG_RESP_CONSULTA_DATOS_INTEGRADOR+"["+transVO.getIdTransaccion()+"] : "+transVO.getRespuesta());
		}catch(Exception e){
			resp = 103;
			logger.error(Constantes.LOG_INICIO_CONSULTA_DATOS_INTEGRADOR+"["+transVO.getIdTransaccion()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_CONSULTA_DATOS_INTEGRADOR+"["+transVO.getIdTransaccion()+"]");
		return resp;
	}
	
	public int getDatosIntegradorCancelacion(TransaccionResVO transVO){
		logger.info(Constantes.LOG_INICIO_CONSULTA_DATOS_INTEGRADOR_CANCELACION+"["+transVO.getIdTransaccion()+"]");
		int resp = 0;
		try{
			getSqlMapClientTemplate().queryForObject("getDatosIntegradorCancelacion", transVO);
			logger.info(Constantes.LOG_RESP_CONSULTA_DATOS_INTEGRADOR_CANCELACION+"["+transVO.getIdTransaccion()+"] : "+transVO.getRespuesta());
		}catch(Exception e){
			resp = 103;
			logger.error(Constantes.LOG_ERROR_CONSULTA_DATOS_INTEGRADOR_CANCELACION+"["+transVO.getIdTransaccion()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_CONSULTA_DATOS_INTEGRADOR_CANCELACION+"["+transVO.getIdTransaccion()+"]");
		return resp;
	}
	
	public double getComisionXProducto(int idAplicacion, int idProducto, double monto){
		logger.info(Constantes.LOG_INICIO_CONSULTA_COMISION+"["+idAplicacion+"] ID PRODUCTO: "
					+idProducto+" - MONTO: "+monto);
		double comision = 0.0;
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		try{
			parametros.put("comision", "");
			parametros.put("idAplicacion", String.valueOf(idAplicacion));
			parametros.put("idProducto", String.valueOf(idProducto));
			parametros.put("monto", String.valueOf(monto));
			getSqlMapClientTemplate().queryForObject("getComisionXProducto", parametros);
			comision = Double.valueOf((String) parametros.get("comision"));
			logger.info(Constantes.LOG_RESP_CONSULTA_COMISION+"["+comision+"] : Id Aplicacion"+idAplicacion
					+", idProducto: "+idProducto);
		}catch(Exception e){
			logger.error(Constantes.LOG_ERROR_CONSULTA_COMISION+"["+idAplicacion+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_CONSULTA_COMISION+"["+idAplicacion+"]");
		return comision;
	}

	
	public int getConsultaServicio(SolicitudCompra solicitudCompra){
		logger.info(Constantes.LOG_INICIO_CONSULTA_SERVICIO+"["+solicitudCompra.getIdTransaccion()+"]");
		int resp = 0;
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		try{
			if(logger.isDebugEnabled()){
				logger.debug(Constantes.LOG_CONSULTA_SERVICIO_DATOS+"[LOGIN]               : "+solicitudCompra.getLogin()+"]");
				logger.debug(Constantes.LOG_CONSULTA_SERVICIO_DATOS+"[REFERENCIA]          : "+solicitudCompra.getReferencia()+"]");
				logger.debug(Constantes.LOG_CONSULTA_SERVICIO_DATOS+"[REFERENCIA SERVICIO] : "+solicitudCompra.getReferenciaServicio()+"]");
				logger.debug(Constantes.LOG_CONSULTA_SERVICIO_DATOS+"[ID SERVICIO]         : "+solicitudCompra.getIdServicio()+"]");
				logger.debug(Constantes.LOG_CONSULTA_SERVICIO_DATOS+"[ID APLICACION]       : "+solicitudCompra.getIdAplicacion()+"]");
				logger.debug(Constantes.LOG_CONSULTA_SERVICIO_DATOS+"[ID USUARIO]          : "+solicitudCompra.getIdUsuario()+"]");
			}			
			parametros.put("respuesta", "");
			parametros.put("referencia", solicitudCompra.getReferencia());
			parametros.put("referenciaServicio", solicitudCompra.getReferenciaServicio());
			parametros.put("idServicio", solicitudCompra.getIdServicio());
			parametros.put("idAplicacion", solicitudCompra.getIdAplicacion());
			parametros.put("idUsuario", solicitudCompra.getIdUsuario());
			
			getSqlMapClientTemplate().queryForObject("consultaServicio", parametros);		
			logger.info(Constantes.LOG_RESP_CONSULTA_SERVICIO+"["+parametros.get("respuesta")+"]");
			solicitudCompra.setRespuesta((String)parametros.get("respuesta"));
		}catch(Exception e){
			resp = 102;
			logger.error(Constantes.LOG_ERROR_CONSULTA_SERVICIO+"["+solicitudCompra.getIdTransaccion()+"] ", e);
		}
		logger.info(Constantes.LOG_INICIO_CONSULTA_SERVICIO+"["+solicitudCompra.getIdTransaccion()+"]");
		return resp;
	}

	public void insertarTransaccionDirecta(CompraDirectaVO compraDirectaVO) {
		logger.info(Constantes.LOG_INICIO_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"]");
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		try{
			if(logger.isDebugEnabled()){
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[TARJETA CIFRADA]  : "+compraDirectaVO.getTarjetaCif());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[TARJETA]          : XXXX-XXXX-XXXX"+
						compraDirectaVO.getTarjeta().substring(compraDirectaVO.getTarjeta().length()-4, compraDirectaVO.getTarjeta().length()));
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[TARJETA NOMBRE]   : "+compraDirectaVO.getNombres()+
						" "+compraDirectaVO.getPrimerApellido()+" "+compraDirectaVO.getSegundoApellido());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[DOCUMENTO]        : "+compraDirectaVO.getDocumento());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[TIPO DOCUMENTO]   : "+compraDirectaVO.getTipoDocumento());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[TIPO DOCUMENTO]   : "+compraDirectaVO.getTipoDocumento());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[ID PRODUCTO]      : "+compraDirectaVO.getInfoCompra().getIdProducto());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[ID SERVICIO]      : 1");
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[TIPO TRANSACCION] : "+compraDirectaVO.getInfoCompra().getTipo());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[CUOTAS]           : "+compraDirectaVO.getInfoCompra().getCantidadCuotas());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[REFERENCIA]       : "+compraDirectaVO.getInfoCompra().getCantidadCuotas());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[VALOR IMPUESTO]   : "+compraDirectaVO.getInfoCompra().getInfoImpuestos().getIvaValor());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[IMPUESTO]         : "+compraDirectaVO.getInfoCompra().getInfoImpuestos().getIva());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[MEDIO]            : "+compraDirectaVO.getMedio());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[DIVISA]           : "+compraDirectaVO.getDivisa());
				logger.debug(Constantes.LOG_TRANSACCION_DIRECTA_DATOS+"[ID APLICACION]    : "+compraDirectaVO.getIdAplicacion());
			}
			parametros.put("respuesta", "");
			parametros.put("tarjetaCif", compraDirectaVO.getTarjetaCif());
			parametros.put("tarjeta", compraDirectaVO.getTarjeta());
			parametros.put("vigencia", compraDirectaVO.getVigencia());
			parametros.put("nombre", compraDirectaVO.getNombres()+" "+compraDirectaVO.getPrimerApellido()+" "+compraDirectaVO.getSegundoApellido());
			parametros.put("documento", compraDirectaVO.getDocumento());
			parametros.put("tipoDocumento", compraDirectaVO.getTipoDocumento());
			parametros.put("monto", compraDirectaVO.getInfoCompra().getMontoTotal());
			parametros.put("idProducto", compraDirectaVO.getInfoCompra().getIdProducto());
			parametros.put("idServicio", "1");
			parametros.put("tipo", compraDirectaVO.getInfoCompra().getTipo());
			parametros.put("cuotas", compraDirectaVO.getInfoCompra().getCantidadCuotas());
			parametros.put("referencia", compraDirectaVO.getInfoCompra().getReferencia());
			parametros.put("valorImpuesto", compraDirectaVO.getInfoCompra().getInfoImpuestos().getIva());
			parametros.put("impuesto", compraDirectaVO.getInfoCompra().getInfoImpuestos().getIvaValor());
			parametros.put("medio", compraDirectaVO.getMedio());
			parametros.put("divisa", compraDirectaVO.getDivisa());
			parametros.put("idAplicacion", compraDirectaVO.getIdAplicacion());
			getSqlMapClientTemplate().queryForObject("insertarTransaccionDirecta", parametros);		
			compraDirectaVO.setRespuesta((String)parametros.get("respuesta"));
			logger.info(Constantes.LOG_RESP_TRANSACCION_DIRECTA+"["+parametros.get("respuesta")+"]");
		}catch(Exception e){
			logger.error(Constantes.LOG_ERROR_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"]");
	}

	public void enviaTransaccionDirecta(CompraDirectaVO compraDirectaVO) {
		logger.info(Constantes.LOG_INICIO_ENVIA_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"]");
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		try{
			if(logger.isDebugEnabled()){
				logger.debug(Constantes.LOG_ENVIA_TRANSACCION_DIRECTA_DATOS+"[ID TRANSACCION] : "+compraDirectaVO.getPuntoInteraccion().getIdTransaccion());
				logger.debug(Constantes.LOG_ENVIA_TRANSACCION_DIRECTA_DATOS+"[ID INTEGRADOR]  : "+compraDirectaVO.getPuntoInteraccion().getIdIntegrador());
			}
			parametros.put("respuesta", "");
			parametros.put("idTransaccion", Long.valueOf(compraDirectaVO.getPuntoInteraccion().getIdTransaccion()).intValue());
			parametros.put("idIntegrador", compraDirectaVO.getPuntoInteraccion().getIdIntegrador());
			getSqlMapClientTemplate().queryForObject("enviaTransaccionDirecta", parametros);		
			compraDirectaVO.setRespuesta((String)parametros.get("respuesta"));
			logger.info(Constantes.LOG_RESP_ENVIA_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"] "+parametros.get("respuesta"));
		}catch(Exception e){
			logger.error(Constantes.LOG_ERROR_ENVIA_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_ENVIA_TRANSACCION_DIRECTA+"["+compraDirectaVO.getIdUsuarioComercio()+"]");
	}

	public ExperianUserVO getDatosExperianUsers(String idAplicacion) {
		logger.info(Constantes.LOG_INICIO_CONSULTA_DATOS_EXPERIAN+"["+idAplicacion+"]");
		ExperianUserVO experianUser = new ExperianUserVO();
		try{
			experianUser.setIdAplicacion(String.valueOf(idAplicacion));
			getSqlMapClientTemplate().queryForObject("experianUsuarios", experianUser);
			logger.info(Constantes.LOG_INICIO_CONSULTA_DATOS_EXPERIAN+"["+idAplicacion+"] : "+experianUser.getRespuesta());
		}catch(Exception e){
			logger.error(Constantes.LOG_ERROR_CONSULTA_DATOS_EXPERIAN+"["+idAplicacion+"] ", e);
		}
		logger.info(Constantes.LOG_FIN_CONSULTA_DATOS_EXPERIAN+"["+idAplicacion+"]");
		return experianUser;
	}
	
}
