/**
 * TipoInfoRespuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.model.vo;

public class AbstractVO {
   
	private int idError;
	private String mensajeError;
	private String respuesta;
	private String urlConfirmacion;
	
	public String getUrlConfirmacion() {
		return urlConfirmacion;
	}

	public void setUrlConfirmacion(String urlConfirmacion) {
		this.urlConfirmacion = urlConfirmacion;
	}

	public AbstractVO(){}
	
	public AbstractVO(int idError, String mensajeError){
		this.idError = idError;
		this.mensajeError = mensajeError;
	}
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
}
