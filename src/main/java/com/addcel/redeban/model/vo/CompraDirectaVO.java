package com.addcel.redeban.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.redeban.ws.servicios.vo.InfoPuntoInteraccion;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CompraDirectaVO extends AbstractVO{

	private int idUsuarioComercio;
	
	private String tarjeta; 
	
	private String tarjetaCif;
	
	private String cvv2;
	
	private String vigencia;
	
	private String idAplicacion;
	
	private String plataforma;
	
	private int idPersona;
	
	private InfoCompraVO infoCompra;
	
	private String infoRefCancelacion;
	
	private String codRespuesta;
	
	private String nombres;
	
	private String primerApellido;
	
	private String segundoApellido;
	
	private int documento;
	
	private String tipoDocumento;
	
	private String monto;
	
	private String idProducto;
	
	private String idServicio;
	
	private String tipo;
		
	private String medio;
	
	private String divisa;
	
	private InfoPuntoInteraccion puntoInteraccion;
	
	private String codigoRespuestaDC;

	public int getIdUsuarioComercio() {
		return idUsuarioComercio;
	}

	public void setIdUsuarioComercio(int idUsuarioComercio) {
		this.idUsuarioComercio = idUsuarioComercio;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public int getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public InfoCompraVO getInfoCompra() {
		return infoCompra;
	}

	public void setInfoCompra(InfoCompraVO infoCompra) {
		this.infoCompra = infoCompra;
	}

	public String getInfoRefCancelacion() {
		return infoRefCancelacion;
	}

	public void setInfoRefCancelacion(String infoRefCancelacion) {
		this.infoRefCancelacion = infoRefCancelacion;
	}

	public String getCodRespuesta() {
		return codRespuesta;
	}

	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMedio() {
		return medio;
	}

	public void setMedio(String medio) {
		this.medio = medio;
	}

	public String getDivisa() {
		return divisa;
	}

	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	public int getDocumento() {
		return documento;
	}

	public void setDocumento(int documento) {
		this.documento = documento;
	}

	public String getTarjetaCif() {
		return tarjetaCif;
	}

	public void setTarjetaCif(String tarjetaCif) {
		this.tarjetaCif = tarjetaCif;
	}

	public InfoPuntoInteraccion getPuntoInteraccion() {
		return puntoInteraccion;
	}

	public void setPuntoInteraccion(InfoPuntoInteraccion puntoInteraccion) {
		this.puntoInteraccion = puntoInteraccion;
	}

	public String getCodigoRespuestaDC() {
		return codigoRespuestaDC;
	}

	public void setCodigoRespuestaDC(String codigoRespuestaDC) {
		this.codigoRespuestaDC = codigoRespuestaDC;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

}
