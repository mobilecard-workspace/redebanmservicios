package com.addcel.redeban.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class InfoImpuestosVO {

	private double iva;
	
	private double ivaValor;
	
	private double devolucionIva;
	
	private double montoAdicional;
	
	private double descuento;
	
	private double consumo;

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getIvaValor() {
		return ivaValor;
	}

	public void setIvaValor(double ivaValor) {
		this.ivaValor = ivaValor;
	}

	public double getDevolucionIva() {
		return devolucionIva;
	}

	public void setDevolucionIva(double devolucionIva) {
		this.devolucionIva = devolucionIva;
	}

	public double getMontoAdicional() {
		return montoAdicional;
	}

	public void setMontoAdicional(double montoAdicional) {
		this.montoAdicional = montoAdicional;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getConsumo() {
		return consumo;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}
	
}
