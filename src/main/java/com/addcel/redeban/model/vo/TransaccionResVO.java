package com.addcel.redeban.model.vo;

public class TransaccionResVO extends AbstractVO{
	
	private long idTransaccion;
	private int idIntegrador;
	
	public long getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public int getIdIntegrador() {
		return idIntegrador;
	}
	public void setIdIntegrador(int idIntegrador) {
		this.idIntegrador = idIntegrador;
	}
	
}
