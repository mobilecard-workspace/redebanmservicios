package com.addcel.redeban.model.vo;

public class TransaccionRespuesta extends AbstractVO{
	
	private long idTransaccion;
	
	private int idIntegrador;
	
	private String cvv2;
	
	private String fechaExpiracion;
	
	public long getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public int getIdIntegrador() {
		return idIntegrador;
	}
	public void setIdIntegrador(int idIntegrador) {
		this.idIntegrador = idIntegrador;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getFechaExpiracion() {
		return fechaExpiracion;
	}
	public void setFechaExpiracion(String fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}
	
}
