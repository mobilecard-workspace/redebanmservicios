package com.addcel.redeban.model.vo;

public class UsuarioVO extends AbstractVO{
	
	private long idUsuario;
	private String login;
	private String password;
	private String telefono;
	private String email;
	private String nombres;
	private String apellidoPrim;
	private String apellidoSeg;
	
	private String tarjeta;
	private String vigencia;
	
	private int franquicia;
	private String franquiciaDesc;
	
	private int status;
	
	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPrim() {
		return apellidoPrim;
	}

	public void setApellidoPrim(String apellidoPrim) {
		this.apellidoPrim = apellidoPrim;
	}

	public String getApellidoSeg() {
		return apellidoSeg;
	}

	public void setApellidoSeg(String apellidoSeg) {
		this.apellidoSeg = apellidoSeg;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public int getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(int franquicia) {
		this.franquicia = franquicia;
	}

	public String getFranquiciaDesc() {
		return franquiciaDesc;
	}

	public void setFranquiciaDesc(String franquiciaDesc) {
		this.franquiciaDesc = franquiciaDesc;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
