package com.addcel.redeban.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.redeban.utils.UtilsService;
import com.addcel.redeban.ws.clientes.multiCompra.TipoFranquicia;
import com.addcel.redeban.ws.clientes.multiCompra.TipoIdPersona;
import com.addcel.redeban.ws.clientes.multiCompra.TipoIdTarjetaCredito;
import com.addcel.redeban.ws.clientes.multiCompra.TipoInfoImpuestos;
import com.addcel.redeban.ws.clientes.multiCompra.TipoMontoDetallado;
import com.addcel.redeban.ws.clientes.multiCompra.TipoTipoDocumento;
import com.addcel.redeban.ws.clientes.multiCompra.TipoTipoImpuesto;
import com.addcel.redeban.ws.clientes.multiCompra.TipoTipoMontoDetallado;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoCapacidadPIN;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersion;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoPuntoInteraccion;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoModoCapturaPAN;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoTipoTerminal;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoCabeceraSolicitud;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompra;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoMedioPago;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoSolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.IdPersona;
import com.addcel.redeban.ws.servicios.vo.InfoCompra;
import com.addcel.redeban.ws.servicios.vo.InfoDispersiones;
import com.addcel.redeban.ws.servicios.vo.InfoMedioPago;
import com.addcel.redeban.ws.servicios.vo.InfoPuntoInteraccion;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;

public class MultiCompraCommonService {

	private static final Logger logger = LoggerFactory.getLogger(RedebanCommonService.class);

	public TipoSolicitudCompra getTipoSolicitudCompra(SolicitudCompra solicitudCompra){
		TipoSolicitudCompra compraProcesarSolicitud = null;
		try{
			compraProcesarSolicitud = new TipoSolicitudCompra();
			
			//agrenado datos al vo principal
			compraProcesarSolicitud.setCabeceraSolicitud(getTipoCabeceraSolicitud(solicitudCompra.getInfoPuntoInteraccion()));
			//agrega dato IdPersona
			compraProcesarSolicitud.setIdPersona(getTipoIdPersona(solicitudCompra.getIdPersona()));
			//agregando datos de tarjeta
			compraProcesarSolicitud.setInfoMedioPago(getTipoInfoMedioPago(solicitudCompra.getInfoMedioPago()));
			//agregando datos tipo compra
			
			setInfoDispersion(compraProcesarSolicitud, solicitudCompra);
//			compraProcesarSolicitud.setInfoCompra(getTipoInfoCompra(solicitudCompra.getInfoCompra()));
			
		}catch(Exception e){
			logger.error("Ocurio un erro general en el llenado getTipoSolicitudCompra: ", e);
		}
		return compraProcesarSolicitud;
	}

	private List<InfoDispersiones> getDispersionesDummys(){
		List<InfoDispersiones> list = new ArrayList<InfoDispersiones>();
		InfoDispersiones dispersion = new InfoDispersiones();
		dispersion.setIdComercio(1);
		dispersion.setIdDispersion(45);
		dispersion.setIdAdquiriente("0010881563");
		dispersion.setIdTerminal("ESB00055");
		dispersion.setTipoImpuesto("IVA");
		dispersion.setMontoImpuesto(0.00);
		dispersion.setMontoTotal(5000);
		list.add(dispersion);
        /**    "idComercio": 5,
            "idDispersion": 46,
            "montoTotal": 0.00,
            "idAdquiriente": 0010881563,
            "idTerminal": ESB00001,
            "tipoImpuesto": IVA,
            "montoImpuesto": 0.00 **/
		dispersion = new InfoDispersiones();
		dispersion.setIdComercio(5);
		dispersion.setIdDispersion(46);
		dispersion.setIdAdquiriente("0010881563");
		dispersion.setIdTerminal("ESB00001");
		dispersion.setTipoImpuesto("IVA");
		dispersion.setMontoImpuesto(0.00);
		dispersion.setMontoTotal(1300);
		list.add(dispersion);
		return list;
		
	}
	

	private void setInfoDispersion(TipoSolicitudCompra compraProcesarSolicitud, SolicitudCompra solicitudCompra) {
		TipoInfoDispersion dispersionesArray[] = new TipoInfoDispersion[2];
		TipoInfoDispersion dispersionesCompra = null;
		List<InfoDispersiones> dispersioneslist = solicitudCompra.getInfoPuntoInteraccion().getDispersiones();
		logger.info("*******************************************************");
		logger.info("*******************************************************");
		dispersioneslist = getDispersionesDummys();
		logger.info("Total de dispersiones es: "+dispersioneslist.size());
		
		int i = 0; 
		for(InfoDispersiones dispersion : dispersioneslist){
			dispersionesCompra = new TipoInfoDispersion();
			TipoInfoCompra infoCompra = new TipoInfoCompra();
			TipoInfoImpuestos infoImpuestosArray[] = new TipoInfoImpuestos[1];
			
			TipoInfoImpuestos infoImpuestos = new TipoInfoImpuestos(
						new TipoTipoImpuesto(dispersion.getTipoImpuesto()), 
						new BigDecimal(dispersion.getMontoImpuesto()), 
						new BigDecimal(0));
			infoImpuestosArray[0] = infoImpuestos;
			infoCompra.setInfoImpuestos(infoImpuestosArray);
			
			dispersionesCompra.setIdAdquiriente(dispersion.getIdAdquiriente());
			dispersionesCompra.setIdTerminal(dispersion.getIdTerminal());
		
			infoCompra.setMontoTotal(new BigDecimal(dispersion.getMontoTotal()));
			infoCompra.setCantidadCuotas(solicitudCompra.getInfoCompra().getCantidadCuotas());
			
//			TipoMontoDetallado montoDet = new TipoMontoDetallado();
//			montoDet.setMonto(new BigDecimal(9));
//			infoCompra.setMontoDetallado(new TipoMontoDetallado[1]);
//			infoCompra.setMontoDetallado(0, montoDet);
			
			dispersionesCompra.setInfoCompra(infoCompra);
			
			dispersionesArray[i] =  dispersionesCompra;
			
			logger.info("IdDispersion: "+dispersion.getIdDispersion());
			logger.info("Monto Total: "+infoCompra.getMontoTotal());
			logger.info("idAdquiriente: "+dispersionesCompra.getIdAdquiriente());
			logger.info("IdTerminal: "+dispersionesCompra.getIdTerminal());
			i++;
		}
//		dispersion.setIdAdquiriente(solicitudCompra.getInfoPuntoInteraccion().getIdAdquiriente());
		
		compraProcesarSolicitud.setInfoDispersion(dispersionesArray);
		return ;
	}


	private TipoCabeceraSolicitud getTipoCabeceraSolicitud(InfoPuntoInteraccion puntoInteraccion){
		TipoCabeceraSolicitud cabeceraSolicitud = null;
	    TipoInfoPuntoInteraccion infoPuntoInteraccion = null;
	    try{
	    	if(puntoInteraccion == null){
	    		logger.error("No hay informacion InfoPuntoInteraccion.");
	    	}else{
		    	//datos de terminal de pago
				cabeceraSolicitud = new TipoCabeceraSolicitud();
				infoPuntoInteraccion = new TipoInfoPuntoInteraccion();
				
				if(puntoInteraccion.getTipoTerminal() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._WEB)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.WEB);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._OrdenTelefonico)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.OrdenTelefonico);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._Movil)){	
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.Movil);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._SmartPhone)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.SmartPhone);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._Tablet)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.Tablet);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._POS)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.POS);
				}else {
					logger.error("TipoTerminal no soportada.");
				}
					
				infoPuntoInteraccion.setIdTerminal(puntoInteraccion.getIdTerminal());
				infoPuntoInteraccion.setIdAdquiriente(puntoInteraccion.getIdAdquiriente());
				infoPuntoInteraccion.setIdTransaccionTerminal(puntoInteraccion.getIdTransaccionTerminal());
								
				if(puntoInteraccion.getModoCapturaPAN() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value1)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value1);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value2)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value2);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value3)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value3);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value4)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value4);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value5)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value5);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value6)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value6);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value7)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value7);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value8)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value8);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value9)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value9);
				}else{
					logger.error("TipoModoCapturaPAN no soportada.");
				}
				
				if(puntoInteraccion.getCapacidadPIN() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Desconocido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Desconocido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Permitido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Permitido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._NoPermitido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.NoPermitido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._FueraDeServicio)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.FueraDeServicio);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Verificado)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Verificado);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._NoPermitidoBatch)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.NoPermitidoBatch);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Virtual)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Virtual);
				}else{
					logger.error("TipoCapacidadPIN no soportada.");
				}
				
				cabeceraSolicitud.setInfoPuntoInteraccion(infoPuntoInteraccion);
	    	}
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoCabeceraSolicitud: ", e);
	    }
	    
		return cabeceraSolicitud;
	}
	
	
	private TipoIdPersona getTipoIdPersona(IdPersona idPersona){
		TipoIdPersona tipoIdPersona = null;
		try{
			
			if(idPersona != null){
				tipoIdPersona = new TipoIdPersona();
				
				if(idPersona.getTipoDocumento() == null){
					logger.error("No hay informacion TipoDocumento.");
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._CC)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CC);
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._CE)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CE);
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._TI)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.TI);
				}else{
					logger.error("TipoDocumento no soportada.");
				}
				
				tipoIdPersona.setNumDocumento(idPersona.getNumDocumento());
			}else{
				tipoIdPersona = new TipoIdPersona();
				tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CC);
				tipoIdPersona.setNumDocumento(123456789);
			}
		}catch(Exception e){
			logger.error("Ocurio un erro general en el llenado getTipoIdPersona: ", e);
		}
		
		
		return tipoIdPersona;
	}
	
	
	private TipoInfoMedioPago getTipoInfoMedioPago(InfoMedioPago infoMedioPago){
		TipoInfoMedioPago tipoInfoMedioPago = null;
	    TipoIdTarjetaCredito idTarjetaCredito = null;
	    
	    try{
	    	tipoInfoMedioPago = new TipoInfoMedioPago();
	    	idTarjetaCredito = new TipoIdTarjetaCredito();
	    	
	    	if(infoMedioPago.getFranquicia() == null){
	    		logger.error("No hay informacion de Franquicia.");
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._VISA)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.VISA);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._AmEx)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.AmEx);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._MasterCard)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.MasterCard);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._DinersClub)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.DinersClub);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._UnionPay)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.UnionPay);
	    	}else{
	    		logger.error("Franquicia no soportada.");
	    	}
	    	
	    	idTarjetaCredito.setNumTarjeta(infoMedioPago.getNumTarjeta());
	    	idTarjetaCredito.setFechaExpiracion(UtilsService.getFechaExpDate(infoMedioPago.getFechaExpiracion()));
	    	idTarjetaCredito.setCodVerificacion(infoMedioPago.getCodVerificacion());
	    	tipoInfoMedioPago.setIdTarjetaCredito(idTarjetaCredito);
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoInfoMedioPago: ", e);
	    }
	    
		return tipoInfoMedioPago;
	}
	
	private TipoInfoCompra getTipoInfoCompra(InfoCompra infoCompra){
		TipoInfoCompra tipoInfoCompra = null;
		TipoInfoImpuestos[] infoImpuestos = null;
		TipoMontoDetallado[] montoDetallado = null;

	    try{
	    	tipoInfoCompra = new TipoInfoCompra();
	    	tipoInfoCompra.setMontoTotal(new BigDecimal(String.valueOf(infoCompra.getMontoTotal())));
	    	tipoInfoCompra.setCantidadCuotas(infoCompra.getCantidadCuotas() == 0? 1: infoCompra.getCantidadCuotas());
//	    	if(!UtilsService.isEmpty(infoCompra.getReferencia())){
//	    		tipoInfoCompra.setReferencia(infoCompra.getReferencia());
//	    	}

	    	infoImpuestos = new TipoInfoImpuestos[1];
	    	infoImpuestos[0] = new TipoInfoImpuestos();
	    	infoImpuestos[0].setTipoImpuesto(TipoTipoImpuesto.IVA);
	    	infoImpuestos[0].setMonto(new BigDecimal(String.valueOf(infoCompra.getInfoImpuestos().getIvaValor())));
	    	tipoInfoCompra.setInfoImpuestos(infoImpuestos);
	    	
	    	if(infoCompra.getInfoImpuestos().getDevolucionIVA() > 0){
	    		montoDetallado = new TipoMontoDetallado[1];
	    		montoDetallado[0] = new TipoMontoDetallado();
	    		montoDetallado[0].setTipoMontoDetallado(TipoTipoMontoDetallado.BaseDevolucionIVA);
	    		montoDetallado[0].setMonto(new BigDecimal(infoCompra.getInfoImpuestos().getDevolucionIVA()));
		    	tipoInfoCompra.setMontoDetallado(montoDetallado);
	    	}
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoInfoCompra: ", e);
	    }
	    
		return tipoInfoCompra;
	}
	
	/**
	private TipoInfoRefCancelacion getInfoRefCancelacion(InfoRefCancelacion infoRefCancelacion){
		TipoInfoRefCancelacion itipoRefCancelacion = null;

	    try{
	    	if(infoRefCancelacion == null){
	    		logger.error("No hay informacion de InfoRefCancelacion.");
	    	}else{
	    		itipoRefCancelacion = new TipoInfoRefCancelacion();
	    		itipoRefCancelacion.setNumAprobacion(infoRefCancelacion.getNumAprobacion());
		    	itipoRefCancelacion.setIdTransaccionAutorizador(infoRefCancelacion.getIdTransaccionAutorizador());
	    	}
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getInfoRefCancelacion: ", e);
	    }
	    
		return itipoRefCancelacion;
	} **/
	
}
