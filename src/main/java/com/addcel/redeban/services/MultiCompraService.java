package com.addcel.redeban.services;

import java.util.Calendar;

import javax.xml.namespace.QName;

import org.apache.axis.AxisFault;
import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.addcel.redeban.utils.AxisSSLSocketFactory;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.utils.UtilsService;
import com.addcel.redeban.ws.clientes.multiCompra.TipoInfoRespuesta;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.MultiCompraElectronica_MsgSetPortTypeProxy;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompraResp;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoRespuesta;
import com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoSolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;

public class MultiCompraService {

	private static final Logger logger = LoggerFactory.getLogger(MultiCompraService.class);
	
	private static MultiCompraElectronica_MsgSetPortTypeProxy multiCompraElectrProxy = null;
	
	static{
		multiCompraElectrProxy = new MultiCompraElectronica_MsgSetPortTypeProxy();
		
		AxisSSLSocketFactory.setKeystorePassword("redebankeypass");
		AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/redeban/utils/Redebankey.jks");
		AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.redeban.utils.AxisSSLSocketFactory");
	}
	
	
	public SolicitudCompraRespuesta compraProcesar(SolicitudCompra solicitudCompra){
		MultiCompraCommonService commonService = new MultiCompraCommonService();
		TipoRespuesta tipoRespuesta = null;
		TipoSolicitudCompra compraProcesarSolicitud = null;
	    SolicitudCompraRespuesta compraProRes = null;
	    double total = 0;
		try{
			total = solicitudCompra.getInfoCompra().getMontoTotal();
			solicitudCompra.getInfoCompra().setMontoTotal(total+solicitudCompra.getInfoCompra().getComision());
			compraProcesarSolicitud = commonService.getTipoSolicitudCompra(solicitudCompra);
			
			logger.info("Inicio del llamado a Redeban, compra..");
			
			tipoRespuesta = multiCompraElectrProxy.multiCompraProcesar(compraProcesarSolicitud);
//			tipoRespuesta = new TipoRespuesta();
//			tipoRespuesta.setInfoRespuesta(new TipoInfoRespuesta());
//			tipoRespuesta.getInfoRespuesta().setCodRespuesta("00");
//			tipoRespuesta.getInfoRespuesta().setDescRespuesta("Aprobado");
//			tipoRespuesta.getInfoRespuesta().setEstado("A");
//			
//			tipoRespuesta.setInfoCompraResp(new TipoInfoCompraResp());
//			tipoRespuesta.getInfoCompraResp().setNumAprobacion(1080);
//			tipoRespuesta.getInfoCompraResp().setFechaTransaccion(Calendar.getInstance());
//			tipoRespuesta.setIdTransaccionAutorizador(123456l);
			
			
			compraProRes = new SolicitudCompraRespuesta();
			
			if(Constantes.CODIGO_APROVADO.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				logger.info("EXITO en la COMPRA.");
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				TipoInfoDispersionResp[] dispersionResp =  tipoRespuesta.getInfoDispersionResp();
				logger.info("NumAprobacion: " + dispersionResp[0].getInfoCompraResp().getNumAprobacion());
				logger.info("IdTransaccionAutorizador: " + dispersionResp[0].getIdTransaccionAutorizador());
				logger.info("FechaTransaccion: " + dispersionResp[0].getInfoCompraResp().getFechaTransaccion());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				compraProRes.setNumAprobacion(dispersionResp[0].getInfoCompraResp().getNumAprobacion());
				compraProRes.setFechaTransaccion(UtilsService.getFechaLarga(dispersionResp[0].getInfoCompraResp().getFechaTransaccion()));
				
				compraProRes.setIdTransaccionAutorizador(dispersionResp[0].getIdTransaccionAutorizador());
				compraProRes.setMontoTotal(total);
				compraProRes.setComision(solicitudCompra.getInfoCompra().getComision());
			}else if(Constantes.CODIGO_ERROR.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				
				logger.info("Error en la compra, inicia proceso de reversa.");
				logger.info("CodigoRespuesta: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setFechaTransaccion(UtilsService.getFechaActual());
				
				compraProRes.setIdError(Integer.parseInt(tipoRespuesta.getInfoRespuesta().getCodRespuesta()));
				
				logger.info("Inicia el proceso de reverso de la compra");
				try{
					reversoProcesar(compraProcesarSolicitud);
				}catch(Exception e){
					logger.info("Error reverso pago: {}", e);
				}
			}else{
				logger.info("Error en la compra.");
				logger.info("CodigoRespuesta: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setIdError(Integer.parseInt(tipoRespuesta.getInfoRespuesta().getCodRespuesta()));
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setFechaTransaccion(UtilsService.getFechaActual());
			}
			
			
//		}catch(AxisFault a){
//			QName error  = a.getFaultCode();
//			String desc  = a.getFaultString();
//			String descDetalle = "";
//			Element[] detalle  = a.getFaultDetails();
//			
//			for(Element det:  detalle){
//				descDetalle += det.getTextContent();
//			}
//			
//			logger.error("Ocurrio un error en el pago:");
//			logger.error("Error code: " + error);
//			logger.error("Error Desc: " + desc);
//			logger.error("Error Detalle: " + descDetalle);
//			
//			compraProRes = new SolicitudCompraRespuesta();
//			
//			compraProRes.setCodRespuesta(error.toString());
//			compraProRes.setEstado(desc);
//			compraProRes.setDescRespuesta(descDetalle);
//			
//			compraProRes.setIdError(Integer.parseInt(error.toString()));
//			compraProRes.setMensajeError(descDetalle);
//			
//			if(desc.contains("TimeoutException")){
//				logger.info("Inicia el proceso de reverso de la compra por TimeoutException.");
//				try{
//					reversoProcesar(compraProcesarSolicitud);
//				}catch(Exception e){
//					logger.error("Error reverso pago: {}", e);
//				}
//			}else{
//				logger.error("Ocurrio un error AxisFault, envio de datos a Redeban: {}", a);
//			}
			
		}catch(Exception e){
			compraProRes = new SolicitudCompraRespuesta();
			compraProRes.setCodRespuesta("99");
			compraProRes.setDescRespuesta("Ocurrio un error General, envio de datos a Redeban: " + e.getMessage());
			
			compraProRes.setIdError(Integer.parseInt(compraProRes.getCodRespuesta()));
			compraProRes.setMensajeError(compraProRes.getDescRespuesta());
			logger.error("Ocurrio un error General, envio de datos a Redeban: {}", e);
		}
		
		return compraProRes;
	}


	public SolicitudCompraRespuesta reversoProcesar(TipoSolicitudCompra compraProcesarSolicitud){
		TipoRespuesta tipoRespuesta = null;
	    SolicitudCompraRespuesta compraProRes = null;
	    
		try{
			
			logger.info("Antes del llamado de reverso..");
			
			tipoRespuesta = multiCompraElectrProxy.multiCompraReversar(compraProcesarSolicitud);
			
			logger.info("Despues del llamado de compra..");
			
			compraProRes = new SolicitudCompraRespuesta();
			
			if(Constantes.CODIGO_APROVADO.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				logger.info("EXITO en el REVERSO compra.");
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("CodigoRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("FechaTransaccion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
//				compraProRes.setNumAprobacion(tipoRespuesta.getInfoCompraResp().getNumAprobacion());
//				compraProRes.setIdTransaccionAutorizador(tipoRespuesta.getIdTransaccionAutorizador());
				
			}else if(Constantes.CODIGO_ERROR.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				
				logger.info("Error en el REVERSO, respuesa incorrecta.");
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				
			}else{
				logger.info("Error en el REVERSO.");
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
			}
		}catch(AxisFault a){
			QName error  = a.getFaultCode();
			String desc  = a.getFaultString();
			String descDetalle = "";
			Element[] detalle  = a.getFaultDetails();
			
			for(Element det:  detalle){
				descDetalle += det.getTextContent();
			}
			
			logger.error("Ocurrio un error en el reveso del pago:");
			logger.error("Error code: " + error);
			logger.error("Error Desc: " + desc);
			logger.error("Error Detalle: " + descDetalle);
			
			compraProRes = new SolicitudCompraRespuesta();
			
			compraProRes.setCodRespuesta(error.toString());
			compraProRes.setEstado(desc);
			compraProRes.setDescRespuesta(descDetalle);
			
		}catch(Exception e){
			compraProRes = new SolicitudCompraRespuesta();
			compraProRes.setCodRespuesta("98");
			compraProRes.setDescRespuesta("Ocurrio un error General, envio de datos a Redeban: " + e.getMessage());
			logger.error("Ocurrio un error General, envio de datos a Redeban: {}", e);
		}
		
		
		
		return compraProRes;
	}
	
}
