package com.addcel.redeban.services;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.addcel.redeban.model.dao.TransaccionDao;
import com.addcel.redeban.model.vo.AbstractVO;
import com.addcel.redeban.model.vo.CompraDirectaVO;
import com.addcel.redeban.model.vo.ExperianUserVO;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.utils.ibatis.service.AbstractService;
import com.addcel.redeban.ws.clientes.datacredit.ServicioValidacionImplProxy;
import com.addcel.redeban.ws.servicios.vo.InfoCompra;
import com.addcel.redeban.ws.servicios.vo.InfoImpuestos;
import com.addcel.redeban.ws.servicios.vo.InfoMedioPago;
import com.addcel.redeban.ws.servicios.vo.InfoPuntoInteraccion;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.AddcelCryptoRSA;
import com.google.gson.Gson;

public class PagoDirectoService  extends AbstractService {
	
	private static final Logger logger = LoggerFactory.getLogger(PagoDirectoService.class);
	
	private Gson gson = new Gson();
	
	private static final SimpleDateFormat formato = new SimpleDateFormat(Constantes.FORMATO_ENCRIPT);

	public String compraDirecta(String json) {
		logger.info(Constantes.LOG_INICIO_COMPRA_DIRECTA);
		CompraDirectaVO compraDirectaVO = null;
		InfoPuntoInteraccion puntoInt = null;
		SolicitudCompra solicitudCompra = null;
		SolicitudCompraRespuesta compraRespuesta = null;
		try{	
			json = AddcelCrypto.decryptSensitive(json);
			logger.info("[COMPRA DIRECTA WS REDEBAN] JSON: ["+json+"]");
			compraDirectaVO = gson.fromJson(json, CompraDirectaVO.class);
			String vigencia = compraDirectaVO.getVigencia();
//			compraDirectaVO.setTarjetaCif(compraDirectaVO.getTarjeta());
//			compraDirectaVO.setTarjeta(AddcelCryptoRSA.encryptPublic(compraDirectaVO.getTarjeta()));
//			compraDirectaVO.setVigencia(AddcelCryptoRSA.encryptPublic(vigencia));
//			insertaTransaccionDirecta(compraDirectaVO);
//			logger.info("[RESPUESTA DE INSERTA TRANSACCION] : "+compraDirectaVO.getPuntoInteraccion().getIdError());
//			if(compraDirectaVO.getPuntoInteraccion().getIdError() != 0){
//				json = gson.toJson(compraDirectaVO.getPuntoInteraccion());
//				json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
//				return json;
//			}
			puntoInt =  enviaTransaccionDirecta(compraDirectaVO);
			logger.info("[RESPUESTA DE ENVIAR TRANSACCION] : "+puntoInt.getIdError());
			if(puntoInt.getIdError() != 0){
				json = gson.toJson(puntoInt);
				json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
				return json;
			}
			
			logger.info("[BANDERA PARA VALIDACION DATACREDIT] : "+puntoInt.getValidaDatacredito());
			if(Constantes.VALIDACION_DATA_CREDIT == puntoInt.getValidaDatacredito()){
				if(!realizaValidacionDataCredit(compraDirectaVO)){
					json = AddcelCrypto.encryptSensitive(formato.format(new Date()), Constantes.ERROR_JSON_TC_NO_VALIDA_DATA_CREDIT);
					return json;
				}
			}
			
			solicitudCompra = getSolicitudCompra(compraDirectaVO, vigencia, puntoInt);
			solicitudCompra.setInfoPuntoInteraccion(puntoInt);
			compraRespuesta = new RedebanCompraService().compraProcesar(solicitudCompra);
			if(compraRespuesta != null){
				compraRespuesta.setIdTransaccion(compraDirectaVO.getPuntoInteraccion().getIdTransaccion());
				actualizaTransaccion(compraRespuesta );
			}
			
			json = gson.toJson(compraRespuesta);	
		}catch(Exception e){
			logger.error("Ocurrio on error al compraProcesar: ", e);
			json = gson.toJson(new AbstractVO(119, "Ocurrio on error al compraProcesar: " + e.getMessage()));
		}finally{
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
		}
		return json;
	}

	private AbstractVO actualizaTransaccion(SolicitudCompraRespuesta compraRespuesta ){
		TransaccionDao dao = null;
		AbstractVO resp = null;
		int status = 0;
		try{
			dao = (TransaccionDao) getBean("TransaccionDao");
			status = dao.actualizarTransaccion(compraRespuesta);
			if(status == 0){
				resp = gson.fromJson(compraRespuesta.getRespuesta(), AbstractVO.class);
				try {
					if(resp.getUrlConfirmacion() != null){
						logger.info("INICIANDO NOTIFICACION DE TRANSACCION - URL "+resp.getUrlConfirmacion());
						RestTemplate restTemplate = new RestTemplate();        	
						HttpHeaders headers = new HttpHeaders();
			        	headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
						MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
						String json = gson.toJson(compraRespuesta);
						json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
						map.add("jsonParams", json);
						HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			        	String respNotificacion = restTemplate.postForObject(resp.getUrlConfirmacion(), request, String.class);
			        	logger.info("FINALIZANDO NOTIFICACION DE TRANSACCION - RESPUESTA: "+respNotificacion);
					}
				} catch (Exception e) {
					logger.error("Ocurrio un erorr al realizar la notificacion: ", e.getCause());
				}
			}else{
				resp = new AbstractVO();
				resp.setIdError(status);
				resp.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			resp = new AbstractVO();
			resp.setIdError(121);
			resp.setMensajeError("Ocurrio un error al insertar la trasaccion en BD: " + e.getMessage());
		}
		return resp;
	}
	
	private boolean realizaValidacionDataCredit(CompraDirectaVO compraDirectaVO) {
		String xmlRequest = null;
		String response = null;
		TransaccionDao dao = null;
		try {
			dao = (TransaccionDao) getBean("TransaccionDao");
			ExperianUserVO user = dao.getDatosExperianUsers(compraDirectaVO.getIdAplicacion());
			user = gson.fromJson(user.getRespuesta(), ExperianUserVO.class);
			ServicioValidacionImplProxy proxyDataCredit = new ServicioValidacionImplProxy();
			xmlRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ "<ValidadorCuenta>"
					+ "<Cuenta tipoIdUsuario=\""+user.getTipoIdUsuario()+"\" "
							+ "idUsuario=\""+user.getIdUsuario()+"\" "
							+ "nitUsuario=\""+user.getNitUsuario()+"\" "
							+ "clave=\""+user.getClave()+"\" "
					+ "tipoIdCliente=\""+user.getTipoIdCliente()+"\" "
					+ "idCliente=\""+compraDirectaVO.getDocumento()+"\" "
					+ "ctaCliente=\""+compraDirectaVO.getTarjetaCif()+"\" "
					+ "primerApellido=\""+compraDirectaVO.getPrimerApellido()+"\"  "
					+ "nombreCliente=\""+compraDirectaVO.getNombres()+"\" /></ValidadorCuenta>";
			logger.info("[DATACREDIT] ["+compraDirectaVO.getNombres()+"] Iniciando comunicacion.");
			logger.info("[DATACREDIT] ["+compraDirectaVO.getNombres()+"] XML: "+xmlRequest);
			response = proxyDataCredit.valCuenta(xmlRequest);			
			Document doc = convertStringToDocument(response, compraDirectaVO);
		    String codigoRespuesta = getCodigoRespuestaDocument(doc, compraDirectaVO);
		    logger.info("[DATACREDIT] ["+compraDirectaVO.getNombres()+"] Respuesta de DataCredit: "+codigoRespuesta);
		    if("00".equals(compraDirectaVO.getCodigoRespuestaDC())){
		    	return true;
		    }
			logger.info("[DATACREDIT] ["+compraDirectaVO.getNombres()+"]- Respuesta de Validacion: "+response);
		} catch (Exception e) {
			logger.error("[DATACREDIT] - ["+compraDirectaVO.getNombres()+"] - Error de comunicacion con DataCredit al consultar la tarjeta "
					+ "["+compraDirectaVO.getTarjeta()+"]");
			e.printStackTrace();
		}
		return false;
	}

	private String getCodigoRespuestaDocument(Document doc, CompraDirectaVO compraDirectaVO) {
		String resp = null;
		if (doc.hasChildNodes()) {
			resp = printNote(doc.getChildNodes(), compraDirectaVO);
		}
		return resp;
	}

	private String printNote(NodeList nodeList, CompraDirectaVO compraDirectaVO) {
		String codResp = null;
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				System.out.println("\nNode Name =" + tempNode.getNodeName()
						+ " [OPEN]");
				System.out.println("Node Value =" + tempNode.getTextContent());
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i = 0; i < nodeMap.getLength(); i++) {
						Node node = nodeMap.item(i);
						System.out.println("attr name : " + node.getNodeName());
						System.out.println("attr value : "
								+ node.getNodeValue());
						if ("codigoRespuesta".equals(node.getNodeName())) {
							codResp = node.getNodeValue();
							compraDirectaVO.setCodigoRespuestaDC(codResp);
							break; 
						}
					}
				}
				if (tempNode.hasChildNodes() && codResp == null) {
					printNote(tempNode.getChildNodes(), compraDirectaVO);
				}
				System.out.println("Node Name =" + tempNode.getNodeName()
						+ " [CLOSE]");
			}
		}
		System.out.println("[DATACREDIT] - [CODIGORESPUESTA] -"+codResp);
		return codResp;

	}
	
	private Document convertStringToDocument(String xmlRequest, CompraDirectaVO compraDirectaVO) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(
					xmlRequest)));
			if (doc.hasChildNodes()) {
				printNote(doc.getChildNodes(), compraDirectaVO);
			}
			logger.info("Document: "+doc);
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private SolicitudCompra getSolicitudCompra(CompraDirectaVO compraDirectaVO, String vigencia, InfoPuntoInteraccion puntoInt) {
		SolicitudCompra solicitudCompra = new SolicitudCompra();
		solicitudCompra.setInfoMedioPago(new InfoMedioPago());
		solicitudCompra.getInfoMedioPago().setNumTarjeta(compraDirectaVO.getTarjetaCif());
		solicitudCompra.getInfoMedioPago().setFechaExpiracion(vigencia);
		solicitudCompra.getInfoMedioPago().setCodVerificacion(String.valueOf(compraDirectaVO.getCvv2()));
		solicitudCompra.getInfoMedioPago().setFranquicia(puntoInt.getFranquicia());
		
		solicitudCompra.setInfoCompra(new InfoCompra());
		solicitudCompra.getInfoCompra().setCantidadCuotas(compraDirectaVO.getInfoCompra().getCantidadCuotas());
		solicitudCompra.getInfoCompra().setMontoTotal(compraDirectaVO.getInfoCompra().getMontoTotal());
		
		InfoImpuestos impuestos = new InfoImpuestos();
		impuestos.setIva(compraDirectaVO.getInfoCompra().getInfoImpuestos().getIvaValor());
		impuestos.setIvaValor(compraDirectaVO.getInfoCompra().getInfoImpuestos().getIvaValor());

		solicitudCompra.getInfoCompra().setInfoImpuestos(impuestos);
		
		return solicitudCompra;
	}

	private InfoPuntoInteraccion enviaTransaccionDirecta(
			CompraDirectaVO compraDirectaVO) {
		InfoPuntoInteraccion puntoInt = null;
		logger.info(Constantes.LOG_INICIA_CONSULTA_BD+compraDirectaVO.getNombres());
		TransaccionDao dao = null;
		try{
			dao = (TransaccionDao) getBean("TransaccionDao");
			dao.enviaTransaccionDirecta(compraDirectaVO);
			puntoInt = gson.fromJson(compraDirectaVO.getRespuesta(), InfoPuntoInteraccion.class);
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			e.printStackTrace();
		}
		return puntoInt;
	}

	private void insertaTransaccionDirecta(CompraDirectaVO compraDirectaVO) {
		logger.info(Constantes.LOG_INICIA_CONSULTA_BD+compraDirectaVO.getNombres());
		TransaccionDao dao = null;
		try{
			dao = (TransaccionDao) getBean("TransaccionDao");
			dao.insertarTransaccionDirecta(compraDirectaVO);
			InfoPuntoInteraccion puntoInt = gson.fromJson(compraDirectaVO.getRespuesta(), InfoPuntoInteraccion.class);
			compraDirectaVO.setPuntoInteraccion(puntoInt);
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			e.printStackTrace();
		}
		return ;
	}

}
