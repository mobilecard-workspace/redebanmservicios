package com.addcel.redeban.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axis.AxisFault;
import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.addcel.redeban.model.dao.BtacoraTarjetaDao;
import com.addcel.redeban.model.dao.TransaccionDao;
import com.addcel.redeban.model.dao.UsuarioDao;
import com.addcel.redeban.model.vo.AbstractVO;
import com.addcel.redeban.model.vo.TransaccionResVO;
import com.addcel.redeban.model.vo.UsuarioVO;
import com.addcel.redeban.utils.AxisSSLSocketFactory;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.utils.UtilsService;
import com.addcel.redeban.utils.ibatis.service.AbstractService;
import com.addcel.redeban.ws.clientes.compra.cancelacion.CompraElectronica_MsgSetPortTypeProxy;
import com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta;
import com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion;
import com.addcel.redeban.ws.servicios.vo.InfoMedioPago;
import com.addcel.redeban.ws.servicios.vo.InfoPuntoInteraccion;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.AddcelCryptoRSA;
import com.google.gson.Gson;

public class RedebanCancelacionService extends AbstractService{
	
	private static final Logger logger = LoggerFactory.getLogger(RedebanCancelacionService.class);

	private Gson gson = new Gson();
	
	private static final String patron = "ddhhmmssSSS";
	
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	private static CompraElectronica_MsgSetPortTypeProxy compraCancelaProxy = null;
	
	public String compraCancelarProcesar(String json) {
	    SolicitudCompraRespuesta compraRespuesta = null;
	    SolicitudCompra solCompra = null;
	    AbstractVO resp = null;
	    TransaccionResVO transVO = null;
	    InfoPuntoInteraccion puntoInt = null;
	    UsuarioVO usuario = null;
		try{
			
			json = AddcelCrypto.decryptSensitive(json);
			solCompra = gson.fromJson(json, SolicitudCompra.class);
			logger.info("SolicitudCompra: "+solCompra);
			logger.debug("Tipo Transaccion: Cancelacion, se utilizan los valores obtenidos del usuario");
			
//			if(solCompra.getPassword()!=null){
//				solCompra.setPassword(AddcelCryptoRSA.encryptPublic(solCompra.getPassword()));
//			}
			
			usuario = getDatosUsuarioVO(solCompra.getIdUsuario() ,solCompra.getLogin(), solCompra.getPassword());
			logger.info("La fecha de vigencia es: "+usuario.getVigencia());
			//Se agregan los datos recuperados de la tarjeta
//			solCompra.setInfoMedioPago(new InfoMedioPago());
//			solCompra.getInfoMedioPago().setNumTarjeta(AddcelCrypto.decryptTarjeta(solCompra.getN) );
//			solCompra.getInfoMedioPago().setFechaExpiracion(usuario.getVigencia());
//			solCompra.getInfoMedioPago().setCodVerificacion(solCompra.getCvv2());
//			solCompra.getInfoMedioPago().setFranquicia(usuario.getFranquiciaDesc());
			
			
			transVO = new TransaccionResVO();
			transVO.setIdTransaccion(solCompra.getIdTransaccion());
			transVO.setIdIntegrador(solCompra.getIdIntegrador());
			
			puntoInt = getDatosIntegradorCancelacion(transVO);
				
			if(puntoInt.getIdError() != 0){
				resp = (AbstractVO) puntoInt;
			}else{
				solCompra.setInfoPuntoInteraccion(puntoInt);
				compraRespuesta = compraCancelarProcesar(solCompra);
				if(compraRespuesta != null){
					compraRespuesta.setIdUsuario(usuario.getIdUsuario());
					compraRespuesta.setIdTransaccion(puntoInt.getIdTransaccion());
					compraRespuesta.setDescRespuesta(solCompra.getRespuesta());
					compraRespuesta.setNumAprobacion(puntoInt.getNumAprobacion());
					compraRespuesta.setCodRespuesta(solCompra.getCodRespuesta());
					actualizaCancelacion(compraRespuesta );
				}
			}

			if( compraRespuesta.getIdError() != 0){
				compraRespuesta = new SolicitudCompraRespuesta();
				compraRespuesta.setIdError(compraRespuesta.getIdError());
				compraRespuesta.setCodRespuesta(String.valueOf(resp.getIdError()));
				compraRespuesta.setMensajeError(compraRespuesta.getMensajeError());
			}
			json = gson.toJson(compraRespuesta);
			
		}catch(Exception e){
			logger.error("Ocurrio on error al compraProcesar: ", e);
			json = gson.toJson(new AbstractVO(119, "Ocurrio on error al compraProcesar: " + e.getMessage()));
		}finally{
			if(json != null){
				json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
			}
		}
		
		return json;
	}
	
	static{
		compraCancelaProxy = new CompraElectronica_MsgSetPortTypeProxy();
		
		AxisSSLSocketFactory.setKeystorePassword("redebankeypass");
		AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/redeban/utils/Redebankey.jks");
		AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.redeban.utils.AxisSSLSocketFactory");
	}
	
	public SolicitudCompraRespuesta compraCancelarProcesar(SolicitudCompra solicitudCompra){
		RedebanCommonService redebanService = new RedebanCommonService();
		TipoRespuesta tipoRespuesta = null;
		TipoSolicitudCancelacion cancelaProcesarSolicitud = null;
	    SolicitudCompraRespuesta compraProRes = null;
	    
		try{
			cancelaProcesarSolicitud = redebanService.getTipoSolicitudCancelacion(solicitudCompra);			
			
			logger.info("Inicio del llamado a Redeban, Cancelacion..");
			
			tipoRespuesta = compraCancelaProxy.compraCancelacionProcesar(cancelaProcesarSolicitud);
			
			compraProRes = new SolicitudCompraRespuesta();
			
			if(Constantes.CODIGO_APROVADO.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				logger.info("Exito en la compra.");
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoCompraResp().getNumAprobacion());
				logger.info("IdTransaccionAutorizador: " + tipoRespuesta.getIdTransaccionAutorizador());
				logger.info("FechaTransaccion: " + tipoRespuesta.getInfoCompraResp().getFechaTransaccion());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				compraProRes.setNumAprobacion(tipoRespuesta.getInfoCompraResp().getNumAprobacion());
				
				compraProRes.setIdTransaccionAutorizador(tipoRespuesta.getIdTransaccionAutorizador());
//				compraProRes.setCostoTransaccion(tipoRespuesta.getInfoCompraResp().getCostoTransaccion().doubleValue());
			}else if(Constantes.CODIGO_ERROR.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				
				logger.info("Error en la compra, proceso reverso.");
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				
				logger.info("Inicia el proceso de reverso de la compra");
				try{
					reversoProcesar(cancelaProcesarSolicitud);
				}catch(Exception e){
					logger.info("Error reverso pago: {}", e);
				}
			}else{
				logger.info("Error en la cancelacin de la compra.");
				logger.info("Datos Compra.");
				logger.info("NumAprobacion: " + cancelaProcesarSolicitud.getInfoRefCancelacion().getNumAprobacion());
				logger.info("IdTransaccionAutorizador: " + cancelaProcesarSolicitud.getInfoRefCancelacion().getIdTransaccionAutorizador());
				
				logger.info("Datos error cancelacion.");
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
			}
			
			
		}catch(AxisFault a){
			QName error  = a.getFaultCode();
			String desc  = a.getFaultString();
			String descDetalle = "";
			Element[] detalle  = a.getFaultDetails();
			
			for(Element det:  detalle){
				descDetalle += det.getTextContent();
			}
			
			logger.error("Ocurrio un error en el pago:");
			logger.error("Error code: " + error);
			logger.error("Error Desc: " + desc);
			logger.error("Error Detalle: " + descDetalle);
			
			compraProRes = new SolicitudCompraRespuesta();
			
			compraProRes.setCodRespuesta(error.toString());
			compraProRes.setEstado(desc);
			compraProRes.setDescRespuesta(descDetalle);
			
			if(desc.contains("TimeoutException")){
				logger.info("Inicia el proceso de reverso de la compra por TimeoutException");
				try{
					reversoProcesar(cancelaProcesarSolicitud);
				}catch(Exception e){
					logger.info("Error reverso pago: {}", e);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		return compraProRes;
	}
	
	public SolicitudCompraRespuesta reversoProcesar(TipoSolicitudCancelacion cancelaProcesarSolicitud){
		TipoRespuesta tipoRespuesta = null;
	    SolicitudCompraRespuesta compraProRes = null;
	    
		try{
			CompraElectronica_MsgSetPortTypeProxy compraElectrProxy = new CompraElectronica_MsgSetPortTypeProxy();
			
			AxisSSLSocketFactory.setKeystorePassword("redebankeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/redeban/utils/Redebankey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.redeban.utils.AxisSSLSocketFactory");
			
			logger.info("Antes del llamado de reverso..");
			
			tipoRespuesta = compraElectrProxy.compraCancelacionReversar(cancelaProcesarSolicitud);
			
			logger.info("Despues del llamado de compra..");
			
			compraProRes = new SolicitudCompraRespuesta();
			
			
			compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
			compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
			
			compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
			compraProRes.setNumAprobacion(tipoRespuesta.getInfoCompraResp().getNumAprobacion());
			
			compraProRes.setIdTransaccionAutorizador(tipoRespuesta.getIdTransaccionAutorizador());
			compraProRes.setCostoTransaccion(tipoRespuesta.getInfoCompraResp().getCostoTransaccion().doubleValue());
//			compraProRes.set
//			compraProRes.set
//			compraProRes.set
		}catch(AxisFault a){
			QName error  = a.getFaultCode();
			String desc  = a.getFaultString();
			String descDetalle = "";
			Element[] detalle  = a.getFaultDetails();
			
			for(Element det:  detalle){
				descDetalle += det.getTextContent();
			}
			
			logger.error("Ocurrio un error en el reveso del pago:");
			logger.error("Error code: " + error);
			logger.error("Error Desc: " + desc);
			logger.error("Error Detalle: " + descDetalle);
			
			compraProRes = new SolicitudCompraRespuesta();
			
			compraProRes.setCodRespuesta(error.toString());
			compraProRes.setEstado(desc);
			compraProRes.setDescRespuesta(descDetalle);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return compraProRes;
	}
		
	private AbstractVO actualizaCancelacion(SolicitudCompraRespuesta compraRespuesta ){
		TransaccionDao dao = null;
		AbstractVO resp = null;
		int status = 0;
		try{
			
			dao = (TransaccionDao) getBean("TransaccionDao");
			
			status = dao.actualizaCancelacion(compraRespuesta);
			if(status == 0){
				resp = gson.fromJson(compraRespuesta.getRespuesta(), AbstractVO.class);
			}else{
				resp = new AbstractVO();
				resp.setIdError(status);
				resp.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			resp = new AbstractVO();
			resp.setIdError(121);
			resp.setMensajeError("Ocurrio un error al insertar la trasaccion en BD: " + e.getMessage());
		}
		return resp;
	}
	
	private InfoPuntoInteraccion getDatosIntegradorCancelacion(TransaccionResVO transVO){
		TransaccionDao dao = null;
		InfoPuntoInteraccion puntoInt = null;
		int status = 0;
		try{
			
			dao = (TransaccionDao) getBean("TransaccionDao");
			status = dao.getDatosIntegradorCancelacion(transVO);
			
			if(status == 0){
				logger.info("Respuesta de Integrador Cancelacion: "+transVO.getRespuesta());
				puntoInt = gson.fromJson(transVO.getRespuesta(), InfoPuntoInteraccion.class);
				
			}else{
				puntoInt = new InfoPuntoInteraccion();
				puntoInt.setIdError(status);
				puntoInt.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			puntoInt = new InfoPuntoInteraccion();
			puntoInt.setIdError(122);
			puntoInt.setMensajeError("Ocurrio un error al insertar la trasaccion en BD: " + e.getMessage());
		}
		return puntoInt;
	}
	
	private UsuarioVO getDatosUsuarioVO(long idUsuario, String login, String password){
		UsuarioDao dao = null;
		UsuarioVO usuario = new UsuarioVO();
		List<UsuarioVO> listaUsuarios = null;
		
		int status = 0;
	    
		try{
			usuario.setIdUsuario(idUsuario);
			usuario.setLogin(login);
			usuario.setPassword(password);
			
			dao = (UsuarioDao) getBean("UsuarioDao");
			
			listaUsuarios = dao.getUsuarios(usuario);
			if(listaUsuarios != null && listaUsuarios.size() > 0){
				usuario = listaUsuarios.get(0);
				
				if(usuario.getIdError() == 0 && usuario.getStatus() != 1){
					usuario.setIdError(usuario.getStatus());
					usuario.setMensajeError(Constantes.ERROR_STATUS);
				}else
				if(usuario.getIdError() == 0){
//					usuario.setVigencia(AddcelCryptoRSA.decryptPrivate(usuario.getVigencia()));
//					usuario.setTarjeta(AddcelCryptoRSA.decryptPrivate(usuario.getTarjeta()));
////					usuario.setPassword("");
				}
			}else{
				usuario.setIdError(status);
				usuario.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio on error al consultar el Usuario: ", e);
			usuario.setIdError(120);
			usuario.setMensajeError("Ocurrio on error al consultar el Usuario: " + e.getMessage());
		}
		
		return usuario;
	}
	
}