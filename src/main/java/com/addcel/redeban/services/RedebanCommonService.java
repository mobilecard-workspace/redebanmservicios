package com.addcel.redeban.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.redeban.utils.UtilsService;
import com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion;
import com.addcel.redeban.ws.clientes.compra.electronica.TipoCabeceraSolicitud;
import com.addcel.redeban.ws.clientes.compra.electronica.TipoInfoCompra;
import com.addcel.redeban.ws.clientes.compra.electronica.TipoInfoMedioPago;
import com.addcel.redeban.ws.clientes.compra.electronica.TipoSolicitudCompra;
import com.addcel.redeban.ws.clientes.compra.vo.TipoFranquicia;
import com.addcel.redeban.ws.clientes.compra.vo.TipoIdPersona;
import com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCredito;
import com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos;
import com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRefCancelacion;
import com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado;
import com.addcel.redeban.ws.clientes.compra.vo.TipoTipoDocumento;
import com.addcel.redeban.ws.clientes.compra.vo.TipoTipoImpuesto;
import com.addcel.redeban.ws.clientes.compra.vo.TipoTipoMontoDetallado;
import com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoCapacidadPIN;
import com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoPuntoInteraccion;
import com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoModoCapturaPAN;
import com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoTipoTerminal;
import com.addcel.redeban.ws.servicios.vo.IdPersona;
import com.addcel.redeban.ws.servicios.vo.InfoCompra;
import com.addcel.redeban.ws.servicios.vo.InfoMedioPago;
import com.addcel.redeban.ws.servicios.vo.InfoPuntoInteraccion;
import com.addcel.redeban.ws.servicios.vo.InfoRefCancelacion;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;

public class RedebanCommonService {
	private static final Logger logger = LoggerFactory.getLogger(RedebanCommonService.class);

	public TipoSolicitudCompra getTipoSolicitudCompra(SolicitudCompra solicitudCompra){
		TipoSolicitudCompra compraProcesarSolicitud = null;
		try{
			compraProcesarSolicitud = new TipoSolicitudCompra();
			compraProcesarSolicitud.setCabeceraSolicitud(getTipoCabeceraSolicitud(solicitudCompra.getInfoPuntoInteraccion()));
			compraProcesarSolicitud.setIdPersona(getTipoIdPersona(solicitudCompra.getIdPersona()));
			compraProcesarSolicitud.setInfoMedioPago(getTipoInfoMedioPago(solicitudCompra.getInfoMedioPago()));
			compraProcesarSolicitud.setInfoCompra(getTipoInfoCompra(solicitudCompra.getInfoCompra(), solicitudCompra.getInfoPuntoInteraccion()));
			if(solicitudCompra.getInfoRefCancelacion() != null){
				getInfoRefCancelacion(solicitudCompra.getInfoRefCancelacion());
			}
		}catch(Exception e){
			logger.error("Ocurio un erro general en el llenado getTipoSolicitudCompra: ", e);
		}
		return compraProcesarSolicitud;
	}
	
	public TipoSolicitudCancelacion getTipoSolicitudCancelacion(SolicitudCompra solicitudCompra){
		TipoSolicitudCancelacion cancelarProcesarSolicitud = null;
		try{
	    	cancelarProcesarSolicitud = new TipoSolicitudCancelacion();
	    	cancelarProcesarSolicitud.setCabeceraSolicitud(getTipoCabeceraSolicitudCancelacion(solicitudCompra.getInfoPuntoInteraccion()));
	    	cancelarProcesarSolicitud.setIdPersona(getTipoIdPersonaCancelacion(solicitudCompra.getIdPersona()));
	    	cancelarProcesarSolicitud.setInfoMedioPago(getTipoInfoMedioPagoCancelacion(solicitudCompra.getInfoPuntoInteraccion()));
	    	cancelarProcesarSolicitud.setInfoCompra(getTipoInfoCompraCancelacion(solicitudCompra.getInfoCompra()));
	    	cancelarProcesarSolicitud.setInfoRefCancelacion(getInfoRefCancelacion(solicitudCompra.getInfoRefCancelacion()));
		}catch(Exception e){
			logger.error("Ocurio un erro general en el llenado getTipoSolicitudCancelacion: ", e);
		}
		return cancelarProcesarSolicitud;
	}
	
	private com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud getTipoCabeceraSolicitudCancelacion(
			InfoPuntoInteraccion puntoInteraccion){
		com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud cabeceraSolicitud = null;
	    TipoInfoPuntoInteraccion infoPuntoInteraccion = null;
	    try{
	    	if(puntoInteraccion == null){
	    		logger.error("No hay informacion InfoPuntoInteraccion.");
	    	}else{
				cabeceraSolicitud = new com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud();
				infoPuntoInteraccion = new TipoInfoPuntoInteraccion();
				
				if(puntoInteraccion.getTipoTerminal() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._WEB)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.WEB);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._OrdenTelefonico)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.OrdenTelefonico);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._Movil)){	
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.Movil);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._SmartPhone)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.SmartPhone);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._Tablet)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.Tablet);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._POS)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.POS);
				}else {
					logger.error("TipoTerminal no soportada.");
				}
					
				infoPuntoInteraccion.setIdTerminal(puntoInteraccion.getIdTerminal());
				infoPuntoInteraccion.setIdAdquiriente(puntoInteraccion.getIdAdquiriente());
				infoPuntoInteraccion.setIdTransaccionTerminal(puntoInteraccion.getIdTransaccionTerminal());
								
				if(puntoInteraccion.getModoCapturaPAN() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value1)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value1);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value2)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value2);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value3)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value3);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value4)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value4);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value5)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value5);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value6)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value6);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value7)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value7);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value8)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value8);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value9)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value9);
				}else{
					logger.error("TipoModoCapturaPAN no soportada.");
				}
				
				if(puntoInteraccion.getCapacidadPIN() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Desconocido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Desconocido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Permitido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Permitido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._NoPermitido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.NoPermitido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._FueraDeServicio)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.FueraDeServicio);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Verificado)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Verificado);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._NoPermitidoBatch)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.NoPermitidoBatch);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Virtual)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Virtual);
				}else{
					logger.error("TipoCapacidadPIN no soportada.");
				}
				
				cabeceraSolicitud.setInfoPuntoInteraccion(infoPuntoInteraccion);
	    	}
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoCabeceraSolicitud: ", e);
	    }
		return cabeceraSolicitud;
	}
	
	private TipoCabeceraSolicitud getTipoCabeceraSolicitud(InfoPuntoInteraccion puntoInteraccion){
		TipoCabeceraSolicitud cabeceraSolicitud = null;
	    TipoInfoPuntoInteraccion infoPuntoInteraccion = null;
	    try{
	    	if(puntoInteraccion == null){
	    		logger.error("No hay informacion InfoPuntoInteraccion.");
	    	}else{
				cabeceraSolicitud = new TipoCabeceraSolicitud();
				infoPuntoInteraccion = new TipoInfoPuntoInteraccion();
				
				if(puntoInteraccion.getTipoTerminal() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._WEB)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.WEB);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._OrdenTelefonico)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.OrdenTelefonico);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._Movil)){	
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.Movil);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._SmartPhone)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.SmartPhone);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._Tablet)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.Tablet);
				}else if(puntoInteraccion.getTipoTerminal().equalsIgnoreCase(TipoTipoTerminal._POS)){
					infoPuntoInteraccion.setTipoTerminal(TipoTipoTerminal.POS);
				}else {
					logger.error("TipoTerminal no soportada.");
				}
					
				infoPuntoInteraccion.setIdTerminal(puntoInteraccion.getIdTerminal());
				infoPuntoInteraccion.setIdAdquiriente(puntoInteraccion.getIdAdquiriente());
				infoPuntoInteraccion.setIdTransaccionTerminal(puntoInteraccion.getIdTransaccionTerminal());
								
				if(puntoInteraccion.getModoCapturaPAN() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value1)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value1);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value2)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value2);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value3)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value3);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value4)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value4);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value5)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value5);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value6)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value6);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value7)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value7);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value8)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value8);
				}else if(puntoInteraccion.getModoCapturaPAN().equalsIgnoreCase(TipoModoCapturaPAN._value9)){
					infoPuntoInteraccion.setModoCapturaPAN(TipoModoCapturaPAN.value9);
				}else{
					logger.error("TipoModoCapturaPAN no soportada.");
				}
				
				if(puntoInteraccion.getCapacidadPIN() == null){
					logger.error("No hay informacion TipoTerminal.");
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Desconocido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Desconocido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Permitido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Permitido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._NoPermitido)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.NoPermitido);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._FueraDeServicio)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.FueraDeServicio);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Verificado)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Verificado);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._NoPermitidoBatch)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.NoPermitidoBatch);
				}else if(puntoInteraccion.getCapacidadPIN().equalsIgnoreCase(TipoCapacidadPIN._Virtual)){
					infoPuntoInteraccion.setCapacidadPIN(TipoCapacidadPIN.Virtual);
				}else{
					logger.error("TipoCapacidadPIN no soportada.");
				}
				
				cabeceraSolicitud.setInfoPuntoInteraccion(infoPuntoInteraccion);
	    	}
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoCabeceraSolicitud: ", e);
	    }
	    
		return cabeceraSolicitud;
	}
	
	
	private TipoIdPersona getTipoIdPersona(IdPersona idPersona){
		TipoIdPersona tipoIdPersona = null;
		try{
			if(idPersona != null){
				tipoIdPersona = new TipoIdPersona();
				
				if(idPersona.getTipoDocumento() == null){
					logger.error("No hay informacion TipoDocumento.");
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._CC)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CC);
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._CE)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CE);
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._TI)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.TI);
				}else{
					logger.error("TipoDocumento no soportada.");
				}
				tipoIdPersona.setNumDocumento(idPersona.getNumDocumento());
			}
		}catch(Exception e){
			logger.error("Ocurio un erro general en el llenado getTipoIdPersona: ", e);
		}
		return tipoIdPersona;
	}
	
	
	private TipoIdPersona getTipoIdPersonaCancelacion(IdPersona idPersona){
		TipoIdPersona tipoIdPersona = null;
		try{
			
			if(idPersona != null){
				tipoIdPersona = new TipoIdPersona();
				
				if(idPersona.getTipoDocumento() == null){
					logger.error("No hay informacion TipoDocumento.");
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._CC)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CC);
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._CE)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.CE);
				}else if(idPersona.getTipoDocumento().equals(TipoTipoDocumento._TI)){
					tipoIdPersona.setTipoDocumento(TipoTipoDocumento.TI);
				}else{
					logger.error("TipoDocumento no soportada.");
				}
				
				tipoIdPersona.setNumDocumento(idPersona.getNumDocumento());
			}
		}catch(Exception e){
			logger.error("Ocurio un erro general en el llenado getTipoIdPersona: ", e);
		}
		
		
		return tipoIdPersona;
	}
	
	private com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago getTipoInfoMedioPagoCancelacion(InfoPuntoInteraccion puntoInteraccion){
		com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago tipoInfoMedioPago = null;
	    TipoIdTarjetaCredito idTarjetaCredito = null;
	    
	    try{
	    	tipoInfoMedioPago = new com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago();
	    	idTarjetaCredito = new TipoIdTarjetaCredito();
	    	
	    	if(puntoInteraccion.getFranquicia() == null){
	    		logger.error("No hay informacion de Franquicia.");
	    	}else if(puntoInteraccion.getFranquicia().equals(TipoFranquicia._VISA)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.VISA);
	    	}else if(puntoInteraccion.getFranquicia().equals(TipoFranquicia._AmEx)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.AmEx);
	    	}else if(puntoInteraccion.getFranquicia().equals(TipoFranquicia._MasterCard)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.MasterCard);
	    	}else if(puntoInteraccion.getFranquicia().equals(TipoFranquicia._DinersClub)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.DinersClub);
	    	}else if(puntoInteraccion.getFranquicia().equals(TipoFranquicia._UnionPay)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.UnionPay);
	    	}else{
	    		logger.error("Franquicia no soportada.");
	    	}
	    	
	    	idTarjetaCredito.setNumTarjeta(puntoInteraccion.getNumTarjeta());
	    	idTarjetaCredito.setFechaExpiracion(UtilsService.getFechaExpDate(puntoInteraccion.getFechaVigencia()));
	    	idTarjetaCredito.setCodVerificacion(puntoInteraccion.getCodigoSeguridad());
	    	tipoInfoMedioPago.setIdTarjetaCredito(idTarjetaCredito);
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoInfoMedioPago: ", e);
	    }
	    
		return tipoInfoMedioPago;
	}
	
	private TipoInfoMedioPago getTipoInfoMedioPago(InfoMedioPago infoMedioPago){
		TipoInfoMedioPago tipoInfoMedioPago = null;
	    TipoIdTarjetaCredito idTarjetaCredito = null;
	    
	    try{
	    	tipoInfoMedioPago = new TipoInfoMedioPago();
	    	idTarjetaCredito = new TipoIdTarjetaCredito();
	    	
	    	if(infoMedioPago.getFranquicia() == null){
	    		logger.error("No hay informacion de Franquicia.");
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._VISA)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.VISA);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._AmEx)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.AmEx);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._MasterCard)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.MasterCard);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._DinersClub)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.DinersClub);
	    	}else if(infoMedioPago.getFranquicia().equals(TipoFranquicia._UnionPay)){
	    		idTarjetaCredito.setFranquicia(TipoFranquicia.UnionPay);
	    	}else{
	    		logger.error("Franquicia no soportada.");
	    	}
	    	
	    	idTarjetaCredito.setNumTarjeta(infoMedioPago.getNumTarjeta());
	    	idTarjetaCredito.setFechaExpiracion(UtilsService.getFechaExpDate(infoMedioPago.getFechaExpiracion()));
	    	idTarjetaCredito.setCodVerificacion(infoMedioPago.getCodVerificacion());
	    	tipoInfoMedioPago.setIdTarjetaCredito(idTarjetaCredito);
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoInfoMedioPago: ", e);
	    }
	    
		return tipoInfoMedioPago;
	}

	private com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra getTipoInfoCompraCancelacion(InfoCompra infoCompra){
		com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra tipoInfoCompra = null;
		TipoInfoImpuestos[] infoImpuestos = null;
		TipoMontoDetallado[] montoDetallado = null;

	    try{
	    	tipoInfoCompra = new com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra();
	    	tipoInfoCompra.setMontoTotal(new BigDecimal(String.valueOf(infoCompra.getMontoTotal())));
	    	tipoInfoCompra.setCantidadCuotas(infoCompra.getCantidadCuotas() == 0? 1: infoCompra.getCantidadCuotas());
//	    	if(!UtilsService.isEmpty(infoCompra.getReferencia())){
//	    		tipoInfoCompra.setReferencia(infoCompra.getReferencia());
//	    	}

	    	infoImpuestos = new TipoInfoImpuestos[1];
	    	infoImpuestos[0] = new TipoInfoImpuestos();
	    	infoImpuestos[0].setTipoImpuesto(TipoTipoImpuesto.IVA);
	    	infoImpuestos[0].setMonto(new BigDecimal(String.valueOf(infoCompra.getInfoImpuestos().getIvaValor())));
	    	tipoInfoCompra.setInfoImpuestos(infoImpuestos);
	    	
	    	if(infoCompra.getInfoImpuestos().getDevolucionIVA() > 0){
	    		montoDetallado = new TipoMontoDetallado[1];
	    		montoDetallado[0] = new TipoMontoDetallado();
	    		montoDetallado[0].setTipoMontoDetallado(TipoTipoMontoDetallado.BaseDevolucionIVA);
	    		montoDetallado[0].setMonto(new BigDecimal(infoCompra.getInfoImpuestos().getDevolucionIVA()));
		    	tipoInfoCompra.setMontoDetallado(montoDetallado);
	    	}
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoInfoCompra: ", e);
	    }
	    
		return tipoInfoCompra;
	}
	
	private TipoInfoCompra getTipoInfoCompra(InfoCompra infoCompra, InfoPuntoInteraccion infoPuntoInteraccion){
		TipoInfoCompra tipoInfoCompra = null;
		TipoInfoImpuestos[] infoImpuestos = null;
		TipoMontoDetallado[] montoDetallado = null;

	    try{
	    	tipoInfoCompra = new TipoInfoCompra();
	    	tipoInfoCompra.setMontoTotal(new BigDecimal(String.valueOf(infoCompra.getMontoTotal())));
	    	tipoInfoCompra.setCantidadCuotas(infoCompra.getCantidadCuotas() == 0? 1: infoCompra.getCantidadCuotas());
//	    	if(!UtilsService.isEmpty(infoCompra.getReferencia())){
//	    		tipoInfoCompra.setReferencia(infoCompra.getReferencia());
//	    	}

	    	if(infoCompra.getInfoImpuestos().getIvaValor() > 0){
	    		infoImpuestos = new TipoInfoImpuestos[1];
		    	infoImpuestos[0] = new TipoInfoImpuestos();
		    	infoImpuestos[0].setTipoImpuesto(TipoTipoImpuesto.IVA);
		    	infoImpuestos[0].setMonto(new BigDecimal(String.valueOf(infoCompra.getInfoImpuestos().getIvaValor())));
		    	infoImpuestos[0].setBaseImpuesto(new BigDecimal(infoPuntoInteraccion.getBaseImpuesto()));
//		    	infoImpuestos[0].setBaseImpuesto(new BigDecimal(16));
		    	
		    	tipoInfoCompra.setInfoImpuestos(infoImpuestos);
		    	
		    	if(infoCompra.getInfoImpuestos().getDevolucionIVA() > 0){
		    		montoDetallado = new TipoMontoDetallado[1];
		    		montoDetallado[0] = new TipoMontoDetallado();
		    		montoDetallado[0].setTipoMontoDetallado(TipoTipoMontoDetallado.BaseDevolucionIVA);
		    		montoDetallado[0].setMonto(new BigDecimal(infoCompra.getInfoImpuestos().getDevolucionIVA()));
			    	tipoInfoCompra.setMontoDetallado(montoDetallado);
		    	}
	    	}
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getTipoInfoCompra: ", e);
	    }
	    
		return tipoInfoCompra;
	}
	
	private TipoInfoRefCancelacion getInfoRefCancelacion(InfoRefCancelacion infoRefCancelacion){
		TipoInfoRefCancelacion itipoRefCancelacion = null;

	    try{
	    	if(infoRefCancelacion == null){
	    		logger.error("No hay informacion de InfoRefCancelacion.");
	    	}else{
	    		itipoRefCancelacion = new TipoInfoRefCancelacion();
	    		itipoRefCancelacion.setNumAprobacion(infoRefCancelacion.getNumAprobacion());
		    	itipoRefCancelacion.setIdTransaccionAutorizador(infoRefCancelacion.getIdTransaccionAutorizador());
	    	}
	    	
	    }catch(Exception e){
	    	logger.error("Ocurio un erro general en el llenado getInfoRefCancelacion: ", e);
	    }
	    
		return itipoRefCancelacion;
	}

	public static void main(String[] args) {
		try {
			Calendar c = Calendar.getInstance();
//			c.setTime(new Date());
//			c.set(Calendar.MILLISECOND, 0);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm.ss");
//			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//			System.out.println(c.getTime());
//			System.out.println(sdf.format(c.getTime()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			System.out.println(sdf.getCalendar());
			System.out.println(UtilsService.getFechaLarga2(c));
			System.out.println(UtilsService.getFechaLarga2(c));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public TipoConsultaPSP getTipoConsulta(SolicitudCompra solicitudCompra) {
//		TipoConsultaPSP consulPSP = new TipoConsultaPSP();
//		TipoSolicitudConsultaPSP consultaPSPSolicitud = new TipoSolicitudConsultaPSP();
//		try{
//			consultaPSPSolicitud.setCabeceraSolicitud(getTipoCabeceraSolicitudPSP(solicitudCompra.getInfoPuntoInteraccion()));
//			consultaPSPSolicitud.setInfoMedioPago(getTipoInfoMedioPagoPSP(solicitudCompra.getInfoMedioPago()));
//			consultaPSPSolicitud.setInfoTransaccion(getInfoTransaccion(solicitudCompra.getInfoPuntoInteraccion()));
//			consulPSP.setConsultaPSPSolicitud(consultaPSPSolicitud);
//		}catch(Exception e){
//			logger.error("Ocurio un erro general en el llenado getTipoSolicitudCompra: ", e);
//		}
//		return consulPSP;
//	}
//	public TipoPagoPSP getTipoPago(SolicitudCompra solicitudCompra) {
//		TipoPagoPSP pagoPSP = new TipoPagoPSP();
//		TipoSolicitudPagoPSP pagarPSPSolicitud = new TipoSolicitudPagoPSP();
//		
//		pagarPSPSolicitud.setCabeceraSolicitud(getTipoCabeceraSolicitud(solicitudCompra.getInfoPuntoInteraccion()));
//		pagarPSPSolicitud.setIdPersona(getTipoIdPersona(solicitudCompra.getIdPersona()));
//		pagarPSPSolicitud.setInfoMedioPago(getTipoInfoMedioPago(solicitudCompra.getInfoMedioPago()));
//		pagarPSPSolicitud.setInfoPagoServicio(getInfoEmpresaServicioArray(solicitudCompra.getInfoPuntoInteraccion()));
//		pagarPSPSolicitud.setInfoTransaccion(getInfoTransaccion(solicitudCompra.getInfoPuntoInteraccion()));
//		
//		pagoPSP.setPagarPSPSolicitud(pagarPSPSolicitud);
//		return null;
//	}
//	public TipoNotificacionPSP getTipoNotificacion(SolicitudCompra solicitudCompra) {
//		TipoNotificacionPSP tipoNotificacionPSP = new TipoNotificacionPSP();
//		TipoSolicitud notificacionPSPSolicitud  = new TipoSolicitud();
//		
//		notificacionPSPSolicitud.setCabeceraSolicitud(getTipoCabeceraSolicitud(solicitudCompra.getInfoPuntoInteraccion()));
//		notificacionPSPSolicitud.setIdPersona(getTipoIdPersona(solicitudCompra.getIdPersona()));
//		notificacionPSPSolicitud.setInfoMedioPago(getTipoInfoMedioPago(solicitudCompra.getInfoMedioPago()));
//		notificacionPSPSolicitud.setInfoNotificacion(getInfoNotificacion());
//		notificacionPSPSolicitud.setInfoNotificacionPSP(getInfoNotificacionPSP());
//		notificacionPSPSolicitud.setInfoPagoServicio(getInfoEmpresaServicioArray(solicitudCompra.getInfoPuntoInteraccion()));
//		notificacionPSPSolicitud.setInfoTransaccion(getInfoTransaccion(solicitudCompra.getInfoPuntoInteraccion()));
//		
//		tipoNotificacionPSP.setNotificacionPSPSolicitud(notificacionPSPSolicitud);
//		return tipoNotificacionPSP;
//	}

//	private TipoInfoNotificacionPSP getInfoNotificacionPSP() {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	private TipoInfoNotificacion getInfoNotificacion() {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
