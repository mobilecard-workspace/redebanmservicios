package com.addcel.redeban.services;

import javax.xml.namespace.QName;

import org.apache.axis.AxisFault;
import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.addcel.redeban.utils.AxisSSLSocketFactory;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.utils.UtilsService;
import com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortTypeProxy;
import com.addcel.redeban.ws.clientes.compra.electronica.TipoRespuesta;
import com.addcel.redeban.ws.clientes.compra.electronica.TipoSolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;

public class RedebanCompraService {
	private static final Logger logger = LoggerFactory.getLogger(RedebanCompraService.class);
	
	private static CompraElectronica_MsgSetPortTypeProxy compraElectrProxy = null;
	
	static{
		compraElectrProxy = new CompraElectronica_MsgSetPortTypeProxy();
		
		AxisSSLSocketFactory.setKeystorePassword("redebankeypass");
		AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/redeban/utils/Redebankey2018Dev.jks");
		AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.redeban.utils.AxisSSLSocketFactory");
	}

	public SolicitudCompraRespuesta compraProcesar(SolicitudCompra solicitudCompra){
		long idTransaccion = solicitudCompra.getInfoPuntoInteraccion().getIdTransaccion();
		logger.info(Constantes.LOG_INICIO_COMUNICACION_REDEBAN + idTransaccion);
		RedebanCommonService redebanService = new RedebanCommonService();
		TipoRespuesta tipoRespuesta = null;
		TipoSolicitudCompra compraProcesarSolicitud = null;
	    SolicitudCompraRespuesta compraProRes = null;
	    double total = 0;
		try{
			total = solicitudCompra.getInfoCompra().getMontoTotal();
			solicitudCompra.getInfoCompra().setSubtotal(total);
			solicitudCompra.getInfoCompra().setMontoTotal(total+solicitudCompra.getInfoCompra().getComision());
			compraProcesarSolicitud = redebanService.getTipoSolicitudCompra(solicitudCompra);
			
			tipoRespuesta = compraElectrProxy.compraProcesar(compraProcesarSolicitud);			
			compraProRes = new SolicitudCompraRespuesta();
			
			if(Constantes.CODIGO_APROVADO.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				logger.info(Constantes.LOG_COMPRA_EXITOSA, idTransaccion );
				logger.info(Constantes.LOG_COMPRA_EXITOSA_DESC_RESP + idTransaccion +" - "+tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info(Constantes.LOG_COMPRA_EXITOSA_NUM_APROB + idTransaccion+" - "+tipoRespuesta.getInfoCompraResp().getNumAprobacion());
				logger.info(Constantes.LOG_COMPRA_EXITOSA_ID_TRANS_AUTORIZA + idTransaccion+" - "+tipoRespuesta.getIdTransaccionAutorizador());
				logger.info(Constantes.LOG_COMPRA_EXITOSA_FECHA_REDEBAN + idTransaccion+" - "+tipoRespuesta.getInfoCompraResp().getFechaTransaccion());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				compraProRes.setNumAprobacion(tipoRespuesta.getInfoCompraResp().getNumAprobacion());
				compraProRes.setFechaTransaccion(UtilsService.getFechaLarga(tipoRespuesta.getInfoCompraResp().getFechaTransaccion()));
				compraProRes.setIdTransaccionAutorizador(tipoRespuesta.getIdTransaccionAutorizador());
				compraProRes.setMontoTotal(total);
				compraProRes.setComision(solicitudCompra.getInfoCompra().getComision());
				compraProRes.setIdIntegrador(solicitudCompra.getInfoPuntoInteraccion().getIdIntegrador());
			}else if(Constantes.CODIGO_ERROR.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				
				logger.info(Constantes.LOG_ERROR_COMPRA_ELECTRONICA + idTransaccion);
				logger.info(Constantes.LOG_ERROR_RESP_CODIGO_RESP + idTransaccion+" - "+tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info(Constantes.LOG_ERROR_RESP_DESC_RESP + idTransaccion+" - "+tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info(Constantes.LOG_ERROR_RESP_ESTADO_RESP + idTransaccion+" - "+tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				compraProRes.setFechaTransaccion(UtilsService.getFechaActual());
				compraProRes.setIdError(Integer.parseInt(tipoRespuesta.getInfoRespuesta().getCodRespuesta()));
				compraProRes.setMensajeError(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setMontoTotal(total);
				compraProRes.setComision(solicitudCompra.getInfoCompra().getComision());
				logger.info("Inicia el proceso de reverso de la compra");
				try{
					reversoProcesar(compraProcesarSolicitud);
				}catch(Exception e){
					logger.info("Error reverso pago: {}", e);
				}
			}else{
				logger.info(Constantes.LOG_ERROR_COMPRA_ELECTRONICA + idTransaccion);
				logger.info(Constantes.LOG_ERROR_RESP_CODIGO_RESP + idTransaccion+" - "+tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info(Constantes.LOG_ERROR_RESP_DESC_RESP + idTransaccion+" - "+tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info(Constantes.LOG_ERROR_RESP_ESTADO_RESP + idTransaccion+" - "+tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setIdError(Integer.parseInt(tipoRespuesta.getInfoRespuesta().getCodRespuesta()));
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				compraProRes.setMontoTotal(total);
				compraProRes.setComision(solicitudCompra.getInfoCompra().getComision());
				compraProRes.setFechaTransaccion(UtilsService.getFechaActual());
			}
			
			
		}catch(AxisFault a){
			QName error  = a.getFaultCode();
			String desc  = a.getFaultString();
			String descDetalle = "";
			Element[] detalle  = a.getFaultDetails();
			
			for(Element det:  detalle){
				descDetalle += det.getTextContent();
			}
			
			logger.error("Ocurrio un error en el pago:");
			logger.error("Error code: " + error);
			logger.error("Error Desc: " + desc);
			logger.error("Error Detalle: " + descDetalle);
			
			compraProRes = new SolicitudCompraRespuesta();
			
			compraProRes.setCodRespuesta(error.toString());
			compraProRes.setEstado(desc);
			compraProRes.setDescRespuesta(descDetalle);
			
			compraProRes.setIdError(Integer.parseInt(error.toString()));
			compraProRes.setMensajeError(descDetalle);
			
			if(desc.contains("TimeoutException")){
				logger.info("Inicia el proceso de reverso de la compra por TimeoutException.");
				try{
					reversoProcesar(compraProcesarSolicitud);
				}catch(Exception e){
					logger.error("Error reverso pago: {}", e);
				}
			}else{
				logger.error("Ocurrio un error AxisFault, envio de datos a Redeban: {}", a);
			}
			
		}catch(Exception e){
			compraProRes = new SolicitudCompraRespuesta();
			compraProRes.setCodRespuesta("99");
			compraProRes.setDescRespuesta("Ocurrio un error General, envio de datos a Redeban: " + e.getMessage());
			
			compraProRes.setIdError(Integer.parseInt(compraProRes.getCodRespuesta()));
			compraProRes.setMensajeError(compraProRes.getDescRespuesta());
			logger.error("Ocurrio un error General, envio de datos a Redeban: {}", e);
		}
		logger.info(Constantes.LOG_FIN_COMUNICACION_REDEBAN + idTransaccion);
		return compraProRes;
	}
	
	public SolicitudCompraRespuesta reversoProcesar(TipoSolicitudCompra compraProcesarSolicitud){
		TipoRespuesta tipoRespuesta = null;
	    SolicitudCompraRespuesta compraProRes = null;
	    
		try{
			
			logger.info("Antes del llamado de reverso..");
			
			tipoRespuesta = compraElectrProxy.compraReversar(compraProcesarSolicitud);
			
			logger.info("Despues del llamado de compra..");
			
			compraProRes = new SolicitudCompraRespuesta();
			
			
			if(Constantes.CODIGO_APROVADO.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				logger.info("EXITO en el REVERSO compra.");
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoCompraResp().getNumAprobacion());
				logger.info("IdTransaccionAutorizador: " + tipoRespuesta.getIdTransaccionAutorizador());
				logger.info("FechaTransaccion: " + tipoRespuesta.getInfoCompraResp().getFechaTransaccion());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				compraProRes.setNumAprobacion(tipoRespuesta.getInfoCompraResp().getNumAprobacion());
				
				compraProRes.setIdTransaccionAutorizador(tipoRespuesta.getIdTransaccionAutorizador());
			}else if(Constantes.CODIGO_ERROR.containsKey(tipoRespuesta.getInfoRespuesta().getCodRespuesta())){
				
				logger.info("Error en el REVERSO, respuesa incorrecta.");
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
				
			}else{
				logger.info("Error en el REVERSO.");
				logger.info("NumAprobacion: " + tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				logger.info("DescRespuesta: " + tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				logger.info("Estado: " + tipoRespuesta.getInfoRespuesta().getEstado());
				
				compraProRes.setCodRespuesta(tipoRespuesta.getInfoRespuesta().getCodRespuesta());
				compraProRes.setDescRespuesta(tipoRespuesta.getInfoRespuesta().getDescRespuesta());
				compraProRes.setEstado(tipoRespuesta.getInfoRespuesta().getEstado());
			}
		}catch(AxisFault a){
			QName error  = a.getFaultCode();
			String desc  = a.getFaultString();
			String descDetalle = "";
			Element[] detalle  = a.getFaultDetails();
			
			for(Element det:  detalle){
				descDetalle += det.getTextContent();
			}
			
			logger.error("Ocurrio un error en el reveso del pago:");
			logger.error("Error code: " + error);
			logger.error("Error Desc: " + desc);
			logger.error("Error Detalle: " + descDetalle);
			
			compraProRes = new SolicitudCompraRespuesta();
			
			compraProRes.setCodRespuesta(error.toString());
			compraProRes.setEstado(desc);
			compraProRes.setDescRespuesta(descDetalle);
			
		}catch(Exception e){
			compraProRes = new SolicitudCompraRespuesta();
			compraProRes.setCodRespuesta("98");
			compraProRes.setDescRespuesta("Ocurrio un error General, envio de datos a Redeban: " + e.getMessage());
			logger.error("Ocurrio un error General, envio de datos a Redeban: {}", e);
		}
		
		
		
		return compraProRes;
	}
	
	
}
