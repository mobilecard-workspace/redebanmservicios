package com.addcel.redeban.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.redeban.model.dao.BtacoraTarjetaDao;
import com.addcel.redeban.model.dao.TransaccionDao;
import com.addcel.redeban.model.dao.UsuarioDao;
import com.addcel.redeban.model.vo.AbstractVO;
import com.addcel.redeban.model.vo.TransaccionResVO;
import com.addcel.redeban.model.vo.UsuarioVO;
import com.addcel.redeban.utils.Constantes;
import com.addcel.redeban.utils.UtilsService;
import com.addcel.redeban.utils.ibatis.service.AbstractService;
import com.addcel.redeban.ws.servicios.vo.InfoMedioPago;
import com.addcel.redeban.ws.servicios.vo.InfoPuntoInteraccion;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.AddcelCryptoRSA;
import com.google.gson.Gson;

public class RedebanService  extends AbstractService{
	private static final Logger logger = LoggerFactory.getLogger(RedebanService.class);
	
	private Gson gson = new Gson();
	
	private static final String patron = "ddhhmmssSSS";
	
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	public String compraProcesar(String json){
		SolicitudCompra solicitudCompra = null;
	    SolicitudCompraRespuesta compraRespuesta = null;
	    UsuarioVO usuario = null;
	    AbstractVO resp = null;
	    TransaccionResVO transVO = null;
	    InfoPuntoInteraccion puntoInt = null;
	    
		try{
			
			json = AddcelCrypto.decryptSensitive(json);
			solicitudCompra = gson.fromJson(json, SolicitudCompra.class);
			
//			if(solicitudCompra.getPassword()!=null && solicitudCompra.getIdUsuarioComercio() == 0){
//				solicitudCompra.setPassword(AddcelCryptoRSA.encryptPublic(solicitudCompra.getPassword()));
//			}
			usuario = getDatosUsuarioVO(solicitudCompra.getIdUsuario() ,solicitudCompra.getLogin(), solicitudCompra.getPassword());
//			if(usuario.getIdError() != 0){
//				resp = (AbstractVO) usuario;
//			} else if(usuario.getIdUsuario() == 0){
//				resp = new AbstractVO(-1, "Usuario no existe o esta inactivo.");
//			} else{
//				logger.debug("Tipo transaccion: {}", solicitudCompra.getInfoCompra().getTipo());
//				if((solicitudCompra.getInfoCompra().getTipo() % 2) == 0){
//					logger.debug("Tipo Transaccion: Recurrencia, se utilizan los valores obtenidos del usuario");
//					solicitudCompra.setLogin(usuario.getLogin());
//					solicitudCompra.setPassword(usuario.getPassword());
//				}
//				//Se agregan los datos recuperados de la tarjeta
//				solicitudCompra.setInfoMedioPago(new InfoMedioPago());
//				solicitudCompra.getInfoMedioPago().setNumTarjeta(usuario.getTarjeta());
//				solicitudCompra.getInfoMedioPago().setFechaExpiracion(usuario.getVigencia());
//				solicitudCompra.getInfoMedioPago().setCodVerificacion(solicitudCompra.getCvv2());
//				solicitudCompra.getInfoMedioPago().setFranquicia(usuario.getFranquiciaDesc());
//				transVO = guardaTransaccion(solicitudCompra);
//				if(transVO.getIdError() != 0){
//					resp = (AbstractVO) transVO;
//				}else{
					transVO = new TransaccionResVO();
					transVO.setIdIntegrador(solicitudCompra.getTrans().getIdIntegrador());
					transVO.setIdTransaccion(solicitudCompra.getTrans().getIdTransaccion());
					puntoInt = getDatosIntegrador(transVO);
					
					if(puntoInt.getIdError() != 0){
						resp = (AbstractVO) puntoInt;
					}else{
						solicitudCompra.setInfoPuntoInteraccion(puntoInt);
						
						getComisiones(solicitudCompra);
						
						compraRespuesta =  enviarXTipoTransaccion(solicitudCompra, transVO.getIdTransaccion());
						
						if(Constantes.CODIGO_APROVADO.containsKey(compraRespuesta.getCodRespuesta())){
							new UtilsService().sendMail(usuario, solicitudCompra, compraRespuesta);
						}
					}
//				}
				
//			}
			
//			insertaBitacoraTarjeta(transVO, solicitudCompra, usuario);
			
			if(resp != null && resp.getIdError() != 0 && compraRespuesta == null){
				compraRespuesta = new SolicitudCompraRespuesta();
				compraRespuesta.setIdError(resp.getIdError());
				compraRespuesta.setCodRespuesta(String.valueOf(resp.getIdError()));
				compraRespuesta.setMensajeError(resp.getMensajeError());
			}
			// CODIGO PARA EN DURO PARA SIMULAR UNA COMPRA SATISFACTORIA 
//			compraRespuesta = new SolicitudCompraRespuesta();
//			compraRespuesta.setIdError(0);
//			compraRespuesta.setCodRespuesta("00");
//			compraRespuesta.setMensajeError("Aprobado");
			json = gson.toJson(compraRespuesta);
			
		}catch(Exception e){
			logger.error("Ocurrio on error al compraProcesar: ", e);
			e.printStackTrace();
			json = gson.toJson(new AbstractVO(119, "Ocurrio on error al compraProcesar: " + e.getMessage()));
		}finally{
			logger.info("RESPUESTA DE REDEBAN WS: "+json);
			if(json != null){
				json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
			}
		}
		
		return json;
	}

	private void getComisiones(SolicitudCompra solicitudCompra) {
		TransaccionDao dao = null;
		double comision = 0; 
		try{
			dao = (TransaccionDao) getBean("TransaccionDao");
			comision = dao.getComisionXProducto(solicitudCompra.getIdAplicacion(), 
					solicitudCompra.getInfoCompra().getIdProducto(),
					solicitudCompra.getInfoCompra().getMontoTotal());
			solicitudCompra.getInfoCompra().setComision(comision);
		}catch(Exception e){
			logger.error("Ocurrio un error al obtener las comisiones en BD: ", e);
		}
		return ;
	}

	private void insertaBitacoraTarjeta(TransaccionResVO transVO,
			SolicitudCompra solicitudCompra, UsuarioVO usuario) {
		BtacoraTarjetaDao dao = null;
		try {
			int status = 0;
			dao = (BtacoraTarjetaDao) getBean("BitacoraTDao");		
			status = dao.insertBtacoraTarjeta(transVO, solicitudCompra, usuario);
			logger.debug("Inserccion en Bitacora tarjetas satisfactorio: "+status);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Ocurrio un error al insertar en bitacora tarjetas" +e.getMessage());
		}
		
	}

	private InfoPuntoInteraccion consultaServicio(SolicitudCompra solicitudCompra) {
		TransaccionDao dao = null;
		InfoPuntoInteraccion puntoInt = null;
		try{
			dao = (TransaccionDao) getBean("TransaccionDao");
			dao.getConsultaServicio(solicitudCompra);
			puntoInt = gson.fromJson(solicitudCompra.getRespuesta(), InfoPuntoInteraccion.class);
		}catch(Exception e){
			logger.error("Ocurrio un error al obtener las comisiones en BD: ", e);
		}
		return puntoInt;
	}
	
	private SolicitudCompraRespuesta enviarXTipoTransaccion(SolicitudCompra solicitudCompra, long idTransaccion){
		SolicitudCompraRespuesta compraRespuesta = null;
		switch (solicitudCompra.getInfoPuntoInteraccion().getTipoTransaccion()){
			case 1:
				compraRespuesta = new RedebanCompraService().compraProcesar(solicitudCompra);
				break;
			case 2:
				compraRespuesta = new RedebanCompraService().compraProcesar(solicitudCompra);
				break;
			case 3:
				compraRespuesta = new RedebanCompraService().compraProcesar(solicitudCompra);
				break;
			case 4:
				compraRespuesta = new RedebanCompraService().compraProcesar(solicitudCompra);
				break;
			case 5:
				compraRespuesta = new MultiCompraService().compraProcesar(solicitudCompra);
				break;
			case 6:
				compraRespuesta = new MultiCompraService().compraProcesar(solicitudCompra);
				break;
			default:
		}
		if(compraRespuesta != null){
			compraRespuesta.setIdTransaccion(idTransaccion);
			actualizaTransaccion(compraRespuesta );
		}
		return compraRespuesta;
	}
	
	
	private UsuarioVO getDatosUsuarioVO(long idUsuario, String login, String password){
		UsuarioDao dao = null;
		UsuarioVO usuario = new UsuarioVO();
		List<UsuarioVO> listaUsuarios = null;
		
		int status = 0;
	    
		try{
			usuario.setIdUsuario(idUsuario);
			usuario.setLogin(login);
			usuario.setPassword(password);
			
			dao = (UsuarioDao) getBean("UsuarioDao");
			
			listaUsuarios = dao.getUsuarios(usuario);
			if(listaUsuarios != null && listaUsuarios.size() > 0){
				usuario = listaUsuarios.get(0);
				
				if(usuario.getIdError() == 0 && usuario.getStatus() != 1){
					usuario.setIdError(usuario.getStatus());
					usuario.setMensajeError(Constantes.ERROR_STATUS);
				}else
				if(usuario.getIdError() == 0){
//					usuario.setVigencia(AddcelCryptoRSA.decryptPrivate(usuario.getVigencia()));
//					usuario.setTarjeta(AddcelCryptoRSA.decryptPrivate(usuario.getTarjeta()));
//					usuario.setPassword("");
				}
			}else{
				usuario.setIdError(status);
				usuario.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio on error al consultar el Usuario: ", e);
			usuario.setIdError(120);
			usuario.setMensajeError("Ocurrio on error al consultar el Usuario: " + e.getMessage());
		}
		
		return usuario;
	}
	
	
	private TransaccionResVO guardaTransaccion(SolicitudCompra solicitudCompra){
		TransaccionDao dao = null;
		TransaccionResVO transVO = null;
		int status = 0;
		try{
			
			dao = (TransaccionDao) getBean("TransaccionDao");
			
			status = dao.insertarTransaccion(solicitudCompra);
			if(status == 0){
				logger.info("Respuesta de insertaTransaccion: "+solicitudCompra.getRespuesta());
				transVO = gson.fromJson(solicitudCompra.getRespuesta(), TransaccionResVO.class);
			
			}else{
				transVO = new TransaccionResVO();
				transVO.setIdError(status);
				transVO.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			transVO = new TransaccionResVO();
			transVO.setIdError(121);
			transVO.setMensajeError("Ocurrio un error al insertar la trasaccion en BD: " + e.getMessage());
		}
		return transVO;
	}
	
	private AbstractVO actualizaTransaccion(SolicitudCompraRespuesta compraRespuesta ){
		TransaccionDao dao = null;
		AbstractVO resp = null;
		int status = 0;
		try{
			
			dao = (TransaccionDao) getBean("TransaccionDao");
			
			status = dao.actualizarTransaccion(compraRespuesta);
			if(status == 0){
				resp = gson.fromJson(compraRespuesta.getRespuesta(), AbstractVO.class);
				if(!StringUtils.isEmpty(resp.getUrlConfirmacion())){
					logger.info("INICIANDO NOTIFICACION DE TRANSACCION - URL "+resp.getUrlConfirmacion());
					RestTemplate restTemplate = new RestTemplate();        	
					HttpHeaders headers = new HttpHeaders();
		        	headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
					MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
					String json = gson.toJson(compraRespuesta);
					json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
					map.add("jsonParams", json);
					HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		        	String respNotificacion = restTemplate.postForObject(resp.getUrlConfirmacion(), request, String.class);
		        	logger.info("FINALIZANDO NOTIFICACION A COLMEDICA - RESPUESTA: "+respNotificacion);
				}
			}else{
				resp = new AbstractVO();
				resp.setIdError(status);
				resp.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			resp = new AbstractVO();
			resp.setIdError(121);
			resp.setMensajeError("Ocurrio un error al insertar la trasaccion en BD: " + e.getMessage());
		}
		return resp;
	}
	
	private InfoPuntoInteraccion getDatosIntegrador(TransaccionResVO transVO){
		TransaccionDao dao = null;
		InfoPuntoInteraccion puntoInt = null;
		int status = 0;
		try{
			
			dao = (TransaccionDao) getBean("TransaccionDao");
			status = dao.getDatosIntegrador(transVO);
			
			if(status == 0){
				puntoInt = gson.fromJson(transVO.getRespuesta(), InfoPuntoInteraccion.class);
			
			}else{
				puntoInt = new InfoPuntoInteraccion();
				puntoInt.setIdError(status);
				puntoInt.setMensajeError(Constantes.ERROR_GENERICO_DB);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al insertar la trasaccion en BD: ", e);
			puntoInt = new InfoPuntoInteraccion();
			puntoInt.setIdError(122);
			puntoInt.setMensajeError("Ocurrio un error al insertar la trasaccion en BD: " + e.getMessage());
		}
		return puntoInt;
	}

}
