package com.addcel.redeban.utils;

import java.util.HashMap;


public class Constantes {
	
	public static HashMap<String, String> CODIGO_APROVADO = new HashMap<String, String>();
	
	public static HashMap<String, String> CODIGO_ERROR = new HashMap<String, String>();
	
	public static final String ERROR_GENERICO_DB = "Ocurrio un error en la BD.";
	
	public static final String ERROR_STATUS = "El usuario no esta Activo.";

	public static final String LOG_INICIA_COMPRA_DIRECTA = "[**** INICIANDO PAGO DIRECTO CON REDEBAN ****]";

	public static final String LOG_FIN_COMPRA_DIRECTA = "[**** FINALIZANDO PAGO DIRECTO CON REDEBAN ****]";
	
	public static final String FORMATO_ENCRIPT = "ddhhmmssSSS";

	public static final String LOG_INICIA_CONSULTA_BD = "[INICIANDO TRANSACCION] USUARIO: ";

	public static final int VALIDACION_DATA_CREDIT = 1;

	public static final String ERROR_JSON_TC_NO_VALIDA_DATA_CREDIT = "{\"idError\": -1000, \"mensajeError\": \"Su tarjeta no ha podido ser validada, intente nuevamente.\"}";

	public static final String ERROR_JSON_INSERTAR_TRANSACCION = "{\"idError\": -1000, \"mensajeError\": \"El pago no ha podido procesarse, intentelo nuevamente.\"}";

	public static final Object LOG_INICIO_CONSULTA_TARJETA_BD = "[BD - EJECUTANDO CONSULTA  TARJETA DEL USUARIO] : ";

	public static final Object LOG_RESPUESTA_CONSULTA_TARJETA_BD = "[BD - RESPUESTA CONSULTA TARJETA DEL USUARIO] : ";

	public static final Object LOG_FIN_GUARDANDO_BITACORA_TARJETA_BD = "[BD - INICIO BITACORA TARJETA ] USUARIO : ";

	public static final String LOG_INICIO_GUARDANDO_BITACORA_TARJETA_BD = "[BD - FIN BITACORA TARJETA ] TRANSACCION : {}";

	public static final String LOG_INICIO_CONSULTA_DATOS_USUARIO = "[BD - INICIO CONSULTA DATOS USUARIO ] LOGIN : ";
	
	public static final String LOG_FIN_CONSULTA_DATOS_USUARIO = "[BD - FIN CONSULTA DATOS USUARIO ] LOGIN : ";

	public static final String LOG_ERROR_CONSULTA_DATOS_USUARIO = "[BD - ERROR CONSULTA DATOS USUARIO ] LOGIN : ";
	
	public static final String LOG_RESP_CONSULTA_DATOS_USUARIO = "[BD - RESPUESTA CONSULTA DATOS USUARIO ] : ";

	public static final String LOG_INICIO_BUSQ_USUARIO = "[BD - INICIO BUSQUEDA USUARIO] : ";
	
	public static final String LOG_FIN_BUSQ_USUARIO = "[BD - FIN BUSQUEDA USUARIO] : ";
	
	public static final String LOG_ERROR_BUSQ_USUARIO = "[BD - ERROR BUSQUEDA USUARIO] : ";
	
	public static final String LOG_RESP_BUSQ_USUARIO = "[BD - RESPUESTA BUSQUEDA USUARIO] TOTAL DE REGISTROS ENCONTRADOS: ";

	public static final String LOG_INICIO_CONSULTA_DATOS_COMERCIO = "[BD - INICIO CONSULTA DATOS COMERCIO] ID APLICACION:  ";
	
	public static final String LOG_FIN_CONSULTA_DATOS_COMERCIO = "[BD - FIN CONSULTA DATOS COMERCIO] ID APLICACION: ";
	
	public static final String LOG_ERROR_CONSULTA_DATOS_COMERCIO = "[BD - ERROR CONSULTA DATOS COMERCIO] ID APLICACION: ";
	
	public static final String LOG_RESP_CONSULTA_DATOS_COMERCIO = "[BD - RESPUESTA CONSULTA DATOS COMERCIO] : ";

	public static final String LOG_INICIO_INSERTA_TRANSACCION = "[BD - INICIO INSERTA TRANSACCION] USUARIO : ";
	
	public static final String LOG_FIN_INSERTA_TRANSACCION = "[BD - FIN INSERTA TRANSACCION] USUARIO : ";
	
	public static final String LOG_ERROR_INSERTA_TRANSACCION = "[BD - ERROR INSERTA TRANSACCION] USUARIO : ";
	
	public static final String LOG_RESP_INSERTA_TRANSACCION = "[BD - RESPUESTA INSERTA TRANSACCION] USUARIO : ";

	public static final String LOG_INSERTA_TRANSACCION_DATOS = "[BD - INSERTA TRANSACCION DATOS] ";

	public static final String LOG_INICIO_ACTUALIZA_TRANSACCION = "[BD - INICIO ACTUALIZA TRANSACCION] ID TRANSACCION : ";
	
	public static final String LOG_FIN_ACTUALIZA_TRANSACCION = "[BD - FIN ACTUALIZA TRANSACCION] ID TRANSACCION : ";
	
	public static final String LOG_ERROR_ACTUALIZA_TRANSACCION = "[BD - ERROR ACTUALIZA TRANSACCION] ID TRANSACCION : ";
	
	public static final String LOG_RESP_ACTUALIZA_TRANSACCION = "[BD - RESPUESTA ACTUALIZA TRANSACCION] ID TRANSACCION : ";

	public static final String LOG_INICIO_ACTUALIZA_CANCELACION = "[BD - INICIO ACTUALIZA CANCELACION] ID TRANSACCION : ";
	
	public static final String LOG_FIN_ACTUALIZA_CANCELACION = "[BD - FIN ACTUALIZA CANCELACION] ID TRANSACCION : ";
	
	public static final String LOG_ERROR_ACTUALIZA_CANCELACION = "[BD - ERROR ACTUALIZA CANCELACION] ID TRANSACCION : ";
	
	public static final String LOG_RESP_ACTUALIZA_CANCELACION = "[BD - ERROR ACTUALIZA CANCELACION] ID TRANSACCION : ";

	public static final String LOG_INICIO_CONSULTA_DATOS_INTEGRADOR = "[BD - INICIO CONSULTA DATOS INTEGRADOR] ID TRANSACCION : ";
	
	public static final String LOG_FIN_CONSULTA_DATOS_INTEGRADOR = "[BD - FIN CONSULTA DATOS INTEGRADOR] ID TRANSACCION : ";
	
	public static final String LOG_RESP_CONSULTA_DATOS_INTEGRADOR = "[BD - RESPUESTA CONSULTA DATOS INTEGRADOR] ID TRANSACCION : ";
	
	public static final String LOG_ERROR_CONSULTA_DATOS_INTEGRADOR = "[BD - ERROR CONSULTA DATOS INTEGRADOR] ID TRANSACCION : ";

	public static final String LOG_INICIO_CONSULTA_DATOS_INTEGRADOR_CANCELACION = "[BD - INICIO CONSULTA DATOS INTEGRADOR CANCELACION] ID TRANSACCION : ";
	
	public static final String LOG_FIN_CONSULTA_DATOS_INTEGRADOR_CANCELACION = "[BD - FIN CONSULTA DATOS INTEGRADOR CANCELACION] ID TRANSACCION : ";
	
	public static final String LOG_ERROR_CONSULTA_DATOS_INTEGRADOR_CANCELACION = "[BD - ERROR CONSULTA DATOS INTEGRADOR CANCELACION] ID TRANSACCION : ";
	
	public static final String LOG_RESP_CONSULTA_DATOS_INTEGRADOR_CANCELACION = "[BD - RESPUESTA CONSULTA DATOS INTEGRADOR CANCELACION] : ";

	public static final String LOG_INICIO_CONSULTA_COMISION = "[BD - INICIO CONSULTA COMISIONES] ID APLICACION: ";
	
	public static final String LOG_FIN_CONSULTA_COMISION = "[BD - FIN CONSULTA COMISIONES] ID APLICACION: ";
	
	public static final String LOG_ERROR_CONSULTA_COMISION = "[BD - ERROR CONSULTA COMISIONES] ID APLICACION: ";
	
	public static final String LOG_RESP_CONSULTA_COMISION = "[BD - RESPUESTA CONSULTA COMISIONES] : ";

	public static final String LOG_INICIO_CONSULTA_SERVICIO = "[BD - INICIO CONSULTA SERVICIO] ID TRANSACCION: ";
	
	public static final String LOG_FIN_CONSULTA_SERVICIO = "[BD - FIN CONSULTA SERVICIO] ID TRANSACCION: ";
	
	public static final String LOG_RESP_CONSULTA_SERVICIO = "[BD - RESPUESTA CONSULTA SERVICIO] ID TRANSACCION: ";
	
	public static final String LOG_ERROR_CONSULTA_SERVICIO = "[BD - ERROR CONSULTA SERVICIO] ID TRANSACCION: ";

	public static final String LOG_CONSULTA_SERVICIO_DATOS = "[BD - CONSULTA SERVICIO DATOS] ";

	public static final String LOG_INICIO_TRANSACCION_DIRECTA = "[BD - INICIO INSERTA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";
	
	public static final String LOG_FIN_TRANSACCION_DIRECTA = "[BD - FIN INSERTA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";
	
	public static final String LOG_RESP_TRANSACCION_DIRECTA = "[BD - RESPUESTA INSERTA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";
	
	public static final String LOG_ERROR_TRANSACCION_DIRECTA = "[BD - ERROR INSERTA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";

	public static final String LOG_TRANSACCION_DIRECTA_DATOS = "[BD - INSERTA TRANSACCION DIRECTA DATOS] ";

	public static final String LOG_INICIO_ENVIA_TRANSACCION_DIRECTA = "[BD - INICIO ENVIA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";
	
	public static final String LOG_FIN_ENVIA_TRANSACCION_DIRECTA = "[BD - FIN ENVIA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";
	
	public static final String LOG_RESP_ENVIA_TRANSACCION_DIRECTA = "[BD - RESPUESTA ENVIA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";
	
	public static final String LOG_ERROR_ENVIA_TRANSACCION_DIRECTA = "[BD - ERROR ENVIA TRANSACCION DIRECTA] ID USUARIO COMERCIO: ";

	public static final String LOG_ENVIA_TRANSACCION_DIRECTA_DATOS = "[BD - ENVIA TRANSACCION DIRECTA DATOS] ";

	public static final String LOG_INICIO_CONSULTA_DATOS_EXPERIAN = "[BD - INICIO CONSULTA DATOS EXPERIAN] ID APLICACION: ";
	
	public static final String LOG_FIN_CONSULTA_DATOS_EXPERIAN = "[BD - FIN CONSULTA DATOS EXPERIAN] ID APLICACION: ";
	
	public static final String LOG_RESP_CONSULTA_DATOS_EXPERIAN = "[BD - RESPUESTA CONSULTA DATOS EXPERIAN] ID APLICACION: ";
	
	public static final String LOG_ERROR_CONSULTA_DATOS_EXPERIAN = "[BD - ERROR CONSULTA DATOS EXPERIAN] ID APLICACION: ";

	public static final String LOG_INICIO_COMPRA_DIRECTA = "[REDEBANWS] - [INICIANDO COMPRA DIRECTA ] ";

	public static final String PROCESO_VALIDACION_ACCESO_WS = "[REDEBANWS] - [AUTENTICANDO CREDENCIALES VIA WSSECURITY] ";

	public static final String LOG_USUARIO_AUTENTICADO_EXITO =  "[REDEBANWS] - [USUARIO AUTENTICADO EXITOSAMENTE] : ";
	
	public static final String LOG_USUARIO_AUTENTICADO_ERROR = "[REDEBANWS] - [USUARIO NO AUTENTICADO ] : ";
	
	public static final String LOG_ASTERISCO = "********************************************";
	
	public static final String LOG_USER_LABEL = "[USUARIO COMERCIO]  : ";
	
	public static final String LOG_USER_PASSWORD_LABEL = "[PASSWORD COMERCIO] : ";
	
	public static final String LOG_PASS_MASK = "******";

	public static final String LOG_ERROR_COMPRA_ELECTRONICA = "[ERROR] [COMPRA ELECTRONICA] - ID TRANSACCION : ";

	public static final String LOG_ERROR_RESP_CODIGO_RESP = "[ERROR] [COMPRA ELECTRONICA] - ID TRANSACCION : ";
	
	public static final String   LOG_ERROR_RESP_DESC_RESP = "[ERROR] [COMPRA ELECTRONICA] - ID TRANSACCION : ";
	
	public static final String LOG_ERROR_RESP_ESTADO_RESP = "[ERROR] [COMPRA ELECTRONICA] - ID TRANSACCION : ";

	public static final String LOG_INICIO_COMUNICACION_REDEBAN = "[REDEBANWS] - [INICIANDO COMUNICACION CON COMPRA ELECTRONICA] : ID TRANSACCION ";

	public static final String LOG_COMPRA_EXITOSA = "[REDEBANWS] - [COMPRA EXITOSA PARA LA TRANSACCION] : ";

	public static final String LOG_COMPRA_EXITOSA_DESC_RESP = "[REDEBANWS] - [COMPRA EXITOSA RESPUESTA] - [ID TRANSACCION - DESCRIPCION RESPUESTA]      : ";

	public static final String LOG_COMPRA_EXITOSA_NUM_APROB = "[REDEBANWS] - [COMPRA EXITOSA RESPUESTA] - [ID TRANSACCION - NO APROBACION]              : ";

	public static final String LOG_COMPRA_EXITOSA_ID_TRANS_AUTORIZA = "[REDEBANWS] - [COMPRA EXITOSA RESPUESTA] - [ID TRANSACCION - ID TRANSACCION AUTORIZADOR] : ";

	public static final String LOG_COMPRA_EXITOSA_FECHA_REDEBAN = "[REDEBANWS] - [COMPRA EXITOSA RESPUESTA] - [ID TRANSACCION - FECHA REDEBAN]              : ";

	public static final String LOG_COMPRA_EXITOSA_FECHA_PROCESADA = "[REDEBANWS] - [COMPRA EXITOSA RESPUESTA] - [ID TRANSACCION - FECHA PROCESADA]             : ";

	public static final String LOG_FIN_COMUNICACION_REDEBAN = "[REDEBANWS] - [FINALIZANDO COMUNICACION CON COMPRA ELECTRONICA] : ID TRANSACCION {}";
	
	static{
		CODIGO_APROVADO.put("00", "00");
		CODIGO_APROVADO.put("08", "08");
		CODIGO_ERROR.put("9001", "9001");
		CODIGO_ERROR.put("9002", "9002");
		CODIGO_ERROR.put("9004", "9004");
	}
}
