/**
 * 
 */
package com.addcel.redeban.utils.ibatis.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author JOCAMPO
 * 
 */

public class SessionApplicationContextBuilderImpl {
	private static Logger log = LoggerFactory.getLogger(SessionApplicationContextBuilderImpl.class);

	private static final String resource = "com/addcel/redeban/model/ibatis/spring-context.xml";
	private static ClassPathXmlApplicationContext ctxt;

	public static ClassPathXmlApplicationContext getApplicationContexInstance() {

		try {
			ctxt = new ClassPathXmlApplicationContext(resource);
		} catch (Exception e) {
			log.error(
					"Error al tratar de crear la instancia SqlSessionFActoryImpl.",
					e);
		}

		return ctxt;
	}

	public static ClassPathXmlApplicationContext reloadApplicationContex() {
		try {
			ctxt = new ClassPathXmlApplicationContext(resource);
		} catch (Exception e) {
			log.error(
					"Error al tratar de crear la instancia SqlSessionFActoryImpl.",
					e);
		}

		return ctxt;
	}

}
