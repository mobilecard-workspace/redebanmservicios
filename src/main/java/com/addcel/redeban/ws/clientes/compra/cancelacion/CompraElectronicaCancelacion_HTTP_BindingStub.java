/**
 * CompraElectronicaCancelacion_HTTP_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.cancelacion;

public class CompraElectronicaCancelacion_HTTP_BindingStub extends org.apache.axis.client.Stub implements com.addcel.redeban.ws.clientes.compra.cancelacion.CompraElectronica_MsgSetPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[2];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("compraCancelacionProcesar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "compraCancelacionProcesarSolicitud"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoSolicitudCancelacion"), com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoRespuesta"));
        oper.setReturnClass(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "compraCancelacionProcesarRespuesta"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("compraCancelacionReversar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "compraCancelacionReversarSolicitud"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoSolicitudCancelacion"), com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoRespuesta"));
        oper.setReturnClass(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "compraCancelacionReversarRespuesta"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

    }

    public CompraElectronicaCancelacion_HTTP_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public CompraElectronicaCancelacion_HTTP_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public CompraElectronicaCancelacion_HTTP_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoCabeceraSolicitud");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoCantidadCuotas");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompra");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompraResp");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompraResp.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoMedioPago");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoReferencia");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoRespuesta");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoSolicitudCancelacion");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoCapacidadPIN");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoCapacidadPIN.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoIdAdquiriente");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoIdTerminal");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoIdTransaccionTerminal");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoPuntoInteraccion");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoPuntoInteraccion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoTerminal");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoTerminal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoModoCapturaPAN");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoModoCapturaPAN.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoTipoTerminal");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoTipoTerminal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoClave");
            cachedSerQNames.add(qName);
            cls = byte[].class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(arraysf);
            cachedDeserFactories.add(arraydf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoCodBanco");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoCodRespuesta");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoCodSeguridadEMV");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoCodVerficacion");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoDataEMV");
            cachedSerQNames.add(qName);
            cls = byte[].class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(arraysf);
            cachedDeserFactories.add(arraydf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoDescripcion");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoEstado");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoFormatoClave");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoFranquicia");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoFranquicia.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdCuenta");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoIdCuenta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdPersona");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoIdPersona.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaCredito");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCredito.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaCreditoPrivada");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCreditoPrivada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaDebitoPrivada");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaDebitoPrivada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTrack");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoIdTrack.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTransaccionAutorizador");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoAdicional");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoAutenticacion");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAutenticacion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoCHIP");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoCHIP.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoImpuestos");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRefCancelacion");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRefCancelacion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRespuesta");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRespuesta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoUbicacion");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoInfoUbicacion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoMonto");
            cachedSerQNames.add(qName);
            cls = java.math.BigDecimal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoMontoDetallado");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoNumAprobacion");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoNumCuenta");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoNumDocumento");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoNumTarjeta");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoBolsillo");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoTipoBolsillo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoCuenta");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoTipoCuenta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoDocumento");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoTipoDocumento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoImpuesto");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoTipoImpuesto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoInfo");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoMontoDetallado");
            cachedSerQNames.add(qName);
            cls = com.addcel.redeban.ws.clientes.compra.vo.TipoTipoMontoDetallado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTrack");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta compraCancelacionProcesar(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion compraCancelacionProcesarSolicitud) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "compraCancelacionProcesar"));
        _call.setTimeout(50000);
        _call.addHeader(getSOAPHeaderElement());

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {compraCancelacionProcesarSolicitud});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta compraCancelacionReversar(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion compraCancelacionReversarSolicitud) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "compraCancelacionReversar"));
        _call.setTimeout(50000);
        _call.addHeader(getSOAPHeaderElement());

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {compraCancelacionReversarSolicitud});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }
    
    private static final String user = "0013813340";
    private static final String pass = "AddcelColombia78/";

    private org.apache.axis.message.SOAPHeaderElement getSOAPHeaderElement() throws org.apache.axis.AxisFault{
    	org.apache.axis.message.SOAPHeaderElement wsseSecurity = null;
    	try{
    		wsseSecurity =
	    			new org.apache.axis.message.SOAPHeaderElement("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd","Security");
	    	org.apache.axis.message.MessageElement usernameToken = 
	    			new org.apache.axis.message.MessageElement("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "UsernameToken");
	    	
	    	org.apache.axis.message.MessageElement username = 
	    			new org.apache.axis.message.MessageElement( "Username", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
	    	
	    	org.apache.axis.message.MessageElement password = 
	    			new org.apache.axis.message.MessageElement( "Password", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
	    	
	    	wsseSecurity.setPrefix("wsse");
	    	wsseSecurity.setActor(null);
	    	wsseSecurity.setMustUnderstand(true);
	    	password.addAttribute("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Type", "PasswordText");
	    	
	    	username.setObjectValue(user);
	    	password.setObjectValue(pass);
	    	usernameToken.addChild(username);
	    	usernameToken.addChild(password);
	    	wsseSecurity.addChild(usernameToken);
	    	
	    	System.out.println("heaer: " + wsseSecurity.toString());
    	}catch(javax.xml.soap.SOAPException se){
    		throw new org.apache.axis.AxisFault("Failure trying to get the Security UsernameToken", se);
    	}
    	return wsseSecurity;
    }    
    
}
