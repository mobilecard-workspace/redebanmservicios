/**
 * CompraElectronica_MsgSetPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.cancelacion;

public interface CompraElectronica_MsgSetPortType extends java.rmi.Remote {
    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta compraCancelacionProcesar(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion compraCancelacionProcesarSolicitud) throws java.rmi.RemoteException;
    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoRespuesta compraCancelacionReversar(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoSolicitudCancelacion compraCancelacionReversarSolicitud) throws java.rmi.RemoteException;
}
