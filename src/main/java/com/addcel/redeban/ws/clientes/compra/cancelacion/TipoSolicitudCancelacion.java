/**
 * TipoSolicitudCancelacion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.cancelacion;

public class TipoSolicitudCancelacion  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud cabeceraSolicitud;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoIdPersona idPersona;

    private com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago infoMedioPago;

    private com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra infoCompra;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRefCancelacion infoRefCancelacion;

    public TipoSolicitudCancelacion() {
    }

    public TipoSolicitudCancelacion(
           com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud cabeceraSolicitud,
           com.addcel.redeban.ws.clientes.compra.vo.TipoIdPersona idPersona,
           com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago infoMedioPago,
           com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra infoCompra,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRefCancelacion infoRefCancelacion) {
           this.cabeceraSolicitud = cabeceraSolicitud;
           this.idPersona = idPersona;
           this.infoMedioPago = infoMedioPago;
           this.infoCompra = infoCompra;
           this.infoRefCancelacion = infoRefCancelacion;
    }


    /**
     * Gets the cabeceraSolicitud value for this TipoSolicitudCancelacion.
     * 
     * @return cabeceraSolicitud
     */
    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud getCabeceraSolicitud() {
        return cabeceraSolicitud;
    }


    /**
     * Sets the cabeceraSolicitud value for this TipoSolicitudCancelacion.
     * 
     * @param cabeceraSolicitud
     */
    public void setCabeceraSolicitud(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoCabeceraSolicitud cabeceraSolicitud) {
        this.cabeceraSolicitud = cabeceraSolicitud;
    }


    /**
     * Gets the idPersona value for this TipoSolicitudCancelacion.
     * 
     * @return idPersona
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoIdPersona getIdPersona() {
        return idPersona;
    }


    /**
     * Sets the idPersona value for this TipoSolicitudCancelacion.
     * 
     * @param idPersona
     */
    public void setIdPersona(com.addcel.redeban.ws.clientes.compra.vo.TipoIdPersona idPersona) {
        this.idPersona = idPersona;
    }


    /**
     * Gets the infoMedioPago value for this TipoSolicitudCancelacion.
     * 
     * @return infoMedioPago
     */
    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago getInfoMedioPago() {
        return infoMedioPago;
    }


    /**
     * Sets the infoMedioPago value for this TipoSolicitudCancelacion.
     * 
     * @param infoMedioPago
     */
    public void setInfoMedioPago(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoMedioPago infoMedioPago) {
        this.infoMedioPago = infoMedioPago;
    }


    /**
     * Gets the infoCompra value for this TipoSolicitudCancelacion.
     * 
     * @return infoCompra
     */
    public com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra getInfoCompra() {
        return infoCompra;
    }


    /**
     * Sets the infoCompra value for this TipoSolicitudCancelacion.
     * 
     * @param infoCompra
     */
    public void setInfoCompra(com.addcel.redeban.ws.clientes.compra.cancelacion.TipoInfoCompra infoCompra) {
        this.infoCompra = infoCompra;
    }


    /**
     * Gets the infoRefCancelacion value for this TipoSolicitudCancelacion.
     * 
     * @return infoRefCancelacion
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRefCancelacion getInfoRefCancelacion() {
        return infoRefCancelacion;
    }


    /**
     * Sets the infoRefCancelacion value for this TipoSolicitudCancelacion.
     * 
     * @param infoRefCancelacion
     */
    public void setInfoRefCancelacion(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRefCancelacion infoRefCancelacion) {
        this.infoRefCancelacion = infoRefCancelacion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoSolicitudCancelacion)) return false;
        TipoSolicitudCancelacion other = (TipoSolicitudCancelacion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cabeceraSolicitud==null && other.getCabeceraSolicitud()==null) || 
             (this.cabeceraSolicitud!=null &&
              this.cabeceraSolicitud.equals(other.getCabeceraSolicitud()))) &&
            ((this.idPersona==null && other.getIdPersona()==null) || 
             (this.idPersona!=null &&
              this.idPersona.equals(other.getIdPersona()))) &&
            ((this.infoMedioPago==null && other.getInfoMedioPago()==null) || 
             (this.infoMedioPago!=null &&
              this.infoMedioPago.equals(other.getInfoMedioPago()))) &&
            ((this.infoCompra==null && other.getInfoCompra()==null) || 
             (this.infoCompra!=null &&
              this.infoCompra.equals(other.getInfoCompra()))) &&
            ((this.infoRefCancelacion==null && other.getInfoRefCancelacion()==null) || 
             (this.infoRefCancelacion!=null &&
              this.infoRefCancelacion.equals(other.getInfoRefCancelacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCabeceraSolicitud() != null) {
            _hashCode += getCabeceraSolicitud().hashCode();
        }
        if (getIdPersona() != null) {
            _hashCode += getIdPersona().hashCode();
        }
        if (getInfoMedioPago() != null) {
            _hashCode += getInfoMedioPago().hashCode();
        }
        if (getInfoCompra() != null) {
            _hashCode += getInfoCompra().hashCode();
        }
        if (getInfoRefCancelacion() != null) {
            _hashCode += getInfoRefCancelacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoSolicitudCancelacion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoSolicitudCancelacion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabeceraSolicitud");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "cabeceraSolicitud"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoCabeceraSolicitud"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPersona");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idPersona"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdPersona"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoMedioPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoMedioPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoMedioPago"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoCompra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoCompra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompra"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoRefCancelacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoRefCancelacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRefCancelacion"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
