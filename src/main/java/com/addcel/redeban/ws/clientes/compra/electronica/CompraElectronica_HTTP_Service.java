/**
 * CompraElectronica_HTTP_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public interface CompraElectronica_HTTP_Service extends javax.xml.rpc.Service {
    public java.lang.String getCompraElectronica_HTTP_PortAddress();

    public com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType getCompraElectronica_HTTP_Port() throws javax.xml.rpc.ServiceException;

    public com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType getCompraElectronica_HTTP_Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
