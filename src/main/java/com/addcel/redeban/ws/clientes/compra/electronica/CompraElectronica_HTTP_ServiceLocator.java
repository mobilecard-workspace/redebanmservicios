/**
 * CompraElectronica_HTTP_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public class CompraElectronica_HTTP_ServiceLocator extends org.apache.axis.client.Service implements com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_HTTP_Service {

    public CompraElectronica_HTTP_ServiceLocator() {
    }


    public CompraElectronica_HTTP_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CompraElectronica_HTTP_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CompraElectronica_HTTP_Port
    private java.lang.String CompraElectronica_HTTP_Port_address = "https://www.txsprodrbm.com/ESBCompraHB/CompraElectronica";

    public java.lang.String getCompraElectronica_HTTP_PortAddress() {
        return CompraElectronica_HTTP_Port_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CompraElectronica_HTTP_PortWSDDServiceName = "CompraElectronica_HTTP_Port";

    public java.lang.String getCompraElectronica_HTTP_PortWSDDServiceName() {
        return CompraElectronica_HTTP_PortWSDDServiceName;
    }

    public void setCompraElectronica_HTTP_PortWSDDServiceName(java.lang.String name) {
        CompraElectronica_HTTP_PortWSDDServiceName = name;
    }

    public com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType getCompraElectronica_HTTP_Port() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CompraElectronica_HTTP_Port_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCompraElectronica_HTTP_Port(endpoint);
    }

    public com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType getCompraElectronica_HTTP_Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_HTTP_BindingStub _stub = new com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_HTTP_BindingStub(portAddress, this);
            _stub.setPortName(getCompraElectronica_HTTP_PortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCompraElectronica_HTTP_PortEndpointAddress(java.lang.String address) {
        CompraElectronica_HTTP_Port_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_HTTP_BindingStub _stub = new com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_HTTP_BindingStub(new java.net.URL(CompraElectronica_HTTP_Port_address), this);
                _stub.setPortName(getCompraElectronica_HTTP_PortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CompraElectronica_HTTP_Port".equals(inputPortName)) {
            return getCompraElectronica_HTTP_Port();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "CompraElectronica_HTTP_Service");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "CompraElectronica_HTTP_Port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CompraElectronica_HTTP_Port".equals(portName)) {
            setCompraElectronica_HTTP_PortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
