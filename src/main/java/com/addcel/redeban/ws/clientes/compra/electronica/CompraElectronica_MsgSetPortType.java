/**
 * CompraElectronica_MsgSetPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public interface CompraElectronica_MsgSetPortType extends java.rmi.Remote {
    public com.addcel.redeban.ws.clientes.compra.electronica.TipoRespuesta compraProcesar(com.addcel.redeban.ws.clientes.compra.electronica.TipoSolicitudCompra compraProcesarSolicitud) throws java.rmi.RemoteException;
    public com.addcel.redeban.ws.clientes.compra.electronica.TipoRespuesta compraReversar(com.addcel.redeban.ws.clientes.compra.electronica.TipoSolicitudCompra compraReversarSolicitud) throws java.rmi.RemoteException;
}
