package com.addcel.redeban.ws.clientes.compra.electronica;

public class CompraElectronica_MsgSetPortTypeProxy implements com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType {
  private String _endpoint = null;
  private com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType compraElectronica_MsgSetPortType = null;
  
  public CompraElectronica_MsgSetPortTypeProxy() {
    _initCompraElectronica_MsgSetPortTypeProxy();
  }
  
  public CompraElectronica_MsgSetPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initCompraElectronica_MsgSetPortTypeProxy();
  }
  
  private void _initCompraElectronica_MsgSetPortTypeProxy() {
    try {
      compraElectronica_MsgSetPortType = (new com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_HTTP_ServiceLocator()).getCompraElectronica_HTTP_Port();
      if (compraElectronica_MsgSetPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)compraElectronica_MsgSetPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)compraElectronica_MsgSetPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (compraElectronica_MsgSetPortType != null)
      ((javax.xml.rpc.Stub)compraElectronica_MsgSetPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.redeban.ws.clientes.compra.electronica.CompraElectronica_MsgSetPortType getCompraElectronica_MsgSetPortType() {
    if (compraElectronica_MsgSetPortType == null)
      _initCompraElectronica_MsgSetPortTypeProxy();
    return compraElectronica_MsgSetPortType;
  }
  
  public com.addcel.redeban.ws.clientes.compra.electronica.TipoRespuesta compraProcesar(com.addcel.redeban.ws.clientes.compra.electronica.TipoSolicitudCompra compraProcesarSolicitud) throws java.rmi.RemoteException{
    if (compraElectronica_MsgSetPortType == null)
      _initCompraElectronica_MsgSetPortTypeProxy();
    return compraElectronica_MsgSetPortType.compraProcesar(compraProcesarSolicitud);
  }
  
  public com.addcel.redeban.ws.clientes.compra.electronica.TipoRespuesta compraReversar(com.addcel.redeban.ws.clientes.compra.electronica.TipoSolicitudCompra compraReversarSolicitud) throws java.rmi.RemoteException{
    if (compraElectronica_MsgSetPortType == null)
      _initCompraElectronica_MsgSetPortTypeProxy();
    return compraElectronica_MsgSetPortType.compraReversar(compraReversarSolicitud);
  }
  
  
}