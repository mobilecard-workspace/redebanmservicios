/**
 * TipoInfoCompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public class TipoInfoCompra  implements java.io.Serializable {
    private java.math.BigDecimal montoTotal;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos[] infoImpuestos;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado[] montoDetallado;

    private java.lang.String referencia;

    private java.lang.Integer cantidadCuotas;

    public TipoInfoCompra() {
    }

    public TipoInfoCompra(
           java.math.BigDecimal montoTotal,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos[] infoImpuestos,
           com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado[] montoDetallado,
           java.lang.String referencia,
           java.lang.Integer cantidadCuotas) {
           this.montoTotal = montoTotal;
           this.infoImpuestos = infoImpuestos;
           this.montoDetallado = montoDetallado;
           this.referencia = referencia;
           this.cantidadCuotas = cantidadCuotas;
    }


    /**
     * Gets the montoTotal value for this TipoInfoCompra.
     * 
     * @return montoTotal
     */
    public java.math.BigDecimal getMontoTotal() {
        return montoTotal;
    }


    /**
     * Sets the montoTotal value for this TipoInfoCompra.
     * 
     * @param montoTotal
     */
    public void setMontoTotal(java.math.BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }


    /**
     * Gets the infoImpuestos value for this TipoInfoCompra.
     * 
     * @return infoImpuestos
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos[] getInfoImpuestos() {
        return infoImpuestos;
    }


    /**
     * Sets the infoImpuestos value for this TipoInfoCompra.
     * 
     * @param infoImpuestos
     */
    public void setInfoImpuestos(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos[] infoImpuestos) {
        this.infoImpuestos = infoImpuestos;
    }

    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos getInfoImpuestos(int i) {
        return this.infoImpuestos[i];
    }

    public void setInfoImpuestos(int i, com.addcel.redeban.ws.clientes.compra.vo.TipoInfoImpuestos _value) {
        this.infoImpuestos[i] = _value;
    }


    /**
     * Gets the montoDetallado value for this TipoInfoCompra.
     * 
     * @return montoDetallado
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado[] getMontoDetallado() {
        return montoDetallado;
    }


    /**
     * Sets the montoDetallado value for this TipoInfoCompra.
     * 
     * @param montoDetallado
     */
    public void setMontoDetallado(com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado[] montoDetallado) {
        this.montoDetallado = montoDetallado;
    }

    public com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado getMontoDetallado(int i) {
        return this.montoDetallado[i];
    }

    public void setMontoDetallado(int i, com.addcel.redeban.ws.clientes.compra.vo.TipoMontoDetallado _value) {
        this.montoDetallado[i] = _value;
    }


    /**
     * Gets the referencia value for this TipoInfoCompra.
     * 
     * @return referencia
     */
    public java.lang.String getReferencia() {
        return referencia;
    }


    /**
     * Sets the referencia value for this TipoInfoCompra.
     * 
     * @param referencia
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }


    /**
     * Gets the cantidadCuotas value for this TipoInfoCompra.
     * 
     * @return cantidadCuotas
     */
    public java.lang.Integer getCantidadCuotas() {
        return cantidadCuotas;
    }


    /**
     * Sets the cantidadCuotas value for this TipoInfoCompra.
     * 
     * @param cantidadCuotas
     */
    public void setCantidadCuotas(java.lang.Integer cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoCompra)) return false;
        TipoInfoCompra other = (TipoInfoCompra) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.montoTotal==null && other.getMontoTotal()==null) || 
             (this.montoTotal!=null &&
              this.montoTotal.equals(other.getMontoTotal()))) &&
            ((this.infoImpuestos==null && other.getInfoImpuestos()==null) || 
             (this.infoImpuestos!=null &&
              java.util.Arrays.equals(this.infoImpuestos, other.getInfoImpuestos()))) &&
            ((this.montoDetallado==null && other.getMontoDetallado()==null) || 
             (this.montoDetallado!=null &&
              java.util.Arrays.equals(this.montoDetallado, other.getMontoDetallado()))) &&
            ((this.referencia==null && other.getReferencia()==null) || 
             (this.referencia!=null &&
              this.referencia.equals(other.getReferencia()))) &&
            ((this.cantidadCuotas==null && other.getCantidadCuotas()==null) || 
             (this.cantidadCuotas!=null &&
              this.cantidadCuotas.equals(other.getCantidadCuotas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMontoTotal() != null) {
            _hashCode += getMontoTotal().hashCode();
        }
        if (getInfoImpuestos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInfoImpuestos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInfoImpuestos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMontoDetallado() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMontoDetallado());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMontoDetallado(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReferencia() != null) {
            _hashCode += getReferencia().hashCode();
        }
        if (getCantidadCuotas() != null) {
            _hashCode += getCantidadCuotas().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoCompra.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompra"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montoTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "montoTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoImpuestos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoImpuestos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoImpuestos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montoDetallado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "montoDetallado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoMontoDetallado"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantidadCuotas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "cantidadCuotas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
