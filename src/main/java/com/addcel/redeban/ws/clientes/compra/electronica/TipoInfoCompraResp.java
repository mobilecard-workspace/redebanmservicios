/**
 * TipoInfoCompraResp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public class TipoInfoCompraResp  implements java.io.Serializable {
    private java.util.Calendar fechaTransaccion;

    private java.util.Date fechaPosteo;

    private java.lang.String numAprobacion;

    private java.math.BigDecimal costoTransaccion;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional[] infoAdicional;

    public TipoInfoCompraResp() {
    }

    public TipoInfoCompraResp(
           java.util.Calendar fechaTransaccion,
           java.util.Date fechaPosteo,
           java.lang.String numAprobacion,
           java.math.BigDecimal costoTransaccion,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional[] infoAdicional) {
           this.fechaTransaccion = fechaTransaccion;
           this.fechaPosteo = fechaPosteo;
           this.numAprobacion = numAprobacion;
           this.costoTransaccion = costoTransaccion;
           this.infoAdicional = infoAdicional;
    }


    /**
     * Gets the fechaTransaccion value for this TipoInfoCompraResp.
     * 
     * @return fechaTransaccion
     */
    public java.util.Calendar getFechaTransaccion() {
        return fechaTransaccion;
    }


    /**
     * Sets the fechaTransaccion value for this TipoInfoCompraResp.
     * 
     * @param fechaTransaccion
     */
    public void setFechaTransaccion(java.util.Calendar fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }


    /**
     * Gets the fechaPosteo value for this TipoInfoCompraResp.
     * 
     * @return fechaPosteo
     */
    public java.util.Date getFechaPosteo() {
        return fechaPosteo;
    }


    /**
     * Sets the fechaPosteo value for this TipoInfoCompraResp.
     * 
     * @param fechaPosteo
     */
    public void setFechaPosteo(java.util.Date fechaPosteo) {
        this.fechaPosteo = fechaPosteo;
    }


    /**
     * Gets the numAprobacion value for this TipoInfoCompraResp.
     * 
     * @return numAprobacion
     */
    public java.lang.String getNumAprobacion() {
        return numAprobacion;
    }


    /**
     * Sets the numAprobacion value for this TipoInfoCompraResp.
     * 
     * @param numAprobacion
     */
    public void setNumAprobacion(java.lang.String numAprobacion) {
        this.numAprobacion = numAprobacion;
    }


    /**
     * Gets the costoTransaccion value for this TipoInfoCompraResp.
     * 
     * @return costoTransaccion
     */
    public java.math.BigDecimal getCostoTransaccion() {
        return costoTransaccion;
    }


    /**
     * Sets the costoTransaccion value for this TipoInfoCompraResp.
     * 
     * @param costoTransaccion
     */
    public void setCostoTransaccion(java.math.BigDecimal costoTransaccion) {
        this.costoTransaccion = costoTransaccion;
    }


    /**
     * Gets the infoAdicional value for this TipoInfoCompraResp.
     * 
     * @return infoAdicional
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional[] getInfoAdicional() {
        return infoAdicional;
    }


    /**
     * Sets the infoAdicional value for this TipoInfoCompraResp.
     * 
     * @param infoAdicional
     */
    public void setInfoAdicional(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional[] infoAdicional) {
        this.infoAdicional = infoAdicional;
    }

    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional getInfoAdicional(int i) {
        return this.infoAdicional[i];
    }

    public void setInfoAdicional(int i, com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAdicional _value) {
        this.infoAdicional[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoCompraResp)) return false;
        TipoInfoCompraResp other = (TipoInfoCompraResp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fechaTransaccion==null && other.getFechaTransaccion()==null) || 
             (this.fechaTransaccion!=null &&
              this.fechaTransaccion.equals(other.getFechaTransaccion()))) &&
            ((this.fechaPosteo==null && other.getFechaPosteo()==null) || 
             (this.fechaPosteo!=null &&
              this.fechaPosteo.equals(other.getFechaPosteo()))) &&
            ((this.numAprobacion==null && other.getNumAprobacion()==null) || 
             (this.numAprobacion!=null &&
              this.numAprobacion.equals(other.getNumAprobacion()))) &&
            ((this.costoTransaccion==null && other.getCostoTransaccion()==null) || 
             (this.costoTransaccion!=null &&
              this.costoTransaccion.equals(other.getCostoTransaccion()))) &&
            ((this.infoAdicional==null && other.getInfoAdicional()==null) || 
             (this.infoAdicional!=null &&
              java.util.Arrays.equals(this.infoAdicional, other.getInfoAdicional())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFechaTransaccion() != null) {
            _hashCode += getFechaTransaccion().hashCode();
        }
        if (getFechaPosteo() != null) {
            _hashCode += getFechaPosteo().hashCode();
        }
        if (getNumAprobacion() != null) {
            _hashCode += getNumAprobacion().hashCode();
        }
        if (getCostoTransaccion() != null) {
            _hashCode += getCostoTransaccion().hashCode();
        }
        if (getInfoAdicional() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInfoAdicional());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInfoAdicional(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoCompraResp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompraResp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaTransaccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "fechaTransaccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaPosteo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "fechaPosteo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAprobacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "numAprobacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("costoTransaccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "costoTransaccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoAdicional");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoAdicional"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoAdicional"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
