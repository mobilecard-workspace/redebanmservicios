/**
 * TipoInfoMedioPago.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public class TipoInfoMedioPago  implements java.io.Serializable {
    private com.addcel.redeban.ws.clientes.compra.vo.TipoIdTrack idTrack;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaDebitoPrivada idTarjetaDebitoPrivada;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCreditoPrivada idTarjetaCreditoPrivada;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCredito idTarjetaCredito;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoIdCuenta idCuenta;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoCHIP infoCHIP;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAutenticacion infoAutenticacion;

    public TipoInfoMedioPago() {
    }

    public TipoInfoMedioPago(
           com.addcel.redeban.ws.clientes.compra.vo.TipoIdTrack idTrack,
           com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaDebitoPrivada idTarjetaDebitoPrivada,
           com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCreditoPrivada idTarjetaCreditoPrivada,
           com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCredito idTarjetaCredito,
           com.addcel.redeban.ws.clientes.compra.vo.TipoIdCuenta idCuenta,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoCHIP infoCHIP,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAutenticacion infoAutenticacion) {
           this.idTrack = idTrack;
           this.idTarjetaDebitoPrivada = idTarjetaDebitoPrivada;
           this.idTarjetaCreditoPrivada = idTarjetaCreditoPrivada;
           this.idTarjetaCredito = idTarjetaCredito;
           this.idCuenta = idCuenta;
           this.infoCHIP = infoCHIP;
           this.infoAutenticacion = infoAutenticacion;
    }


    /**
     * Gets the idTrack value for this TipoInfoMedioPago.
     * 
     * @return idTrack
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoIdTrack getIdTrack() {
        return idTrack;
    }


    /**
     * Sets the idTrack value for this TipoInfoMedioPago.
     * 
     * @param idTrack
     */
    public void setIdTrack(com.addcel.redeban.ws.clientes.compra.vo.TipoIdTrack idTrack) {
        this.idTrack = idTrack;
    }


    /**
     * Gets the idTarjetaDebitoPrivada value for this TipoInfoMedioPago.
     * 
     * @return idTarjetaDebitoPrivada
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaDebitoPrivada getIdTarjetaDebitoPrivada() {
        return idTarjetaDebitoPrivada;
    }


    /**
     * Sets the idTarjetaDebitoPrivada value for this TipoInfoMedioPago.
     * 
     * @param idTarjetaDebitoPrivada
     */
    public void setIdTarjetaDebitoPrivada(com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaDebitoPrivada idTarjetaDebitoPrivada) {
        this.idTarjetaDebitoPrivada = idTarjetaDebitoPrivada;
    }


    /**
     * Gets the idTarjetaCreditoPrivada value for this TipoInfoMedioPago.
     * 
     * @return idTarjetaCreditoPrivada
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCreditoPrivada getIdTarjetaCreditoPrivada() {
        return idTarjetaCreditoPrivada;
    }


    /**
     * Sets the idTarjetaCreditoPrivada value for this TipoInfoMedioPago.
     * 
     * @param idTarjetaCreditoPrivada
     */
    public void setIdTarjetaCreditoPrivada(com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCreditoPrivada idTarjetaCreditoPrivada) {
        this.idTarjetaCreditoPrivada = idTarjetaCreditoPrivada;
    }


    /**
     * Gets the idTarjetaCredito value for this TipoInfoMedioPago.
     * 
     * @return idTarjetaCredito
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCredito getIdTarjetaCredito() {
        return idTarjetaCredito;
    }


    /**
     * Sets the idTarjetaCredito value for this TipoInfoMedioPago.
     * 
     * @param idTarjetaCredito
     */
    public void setIdTarjetaCredito(com.addcel.redeban.ws.clientes.compra.vo.TipoIdTarjetaCredito idTarjetaCredito) {
        this.idTarjetaCredito = idTarjetaCredito;
    }


    /**
     * Gets the idCuenta value for this TipoInfoMedioPago.
     * 
     * @return idCuenta
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoIdCuenta getIdCuenta() {
        return idCuenta;
    }


    /**
     * Sets the idCuenta value for this TipoInfoMedioPago.
     * 
     * @param idCuenta
     */
    public void setIdCuenta(com.addcel.redeban.ws.clientes.compra.vo.TipoIdCuenta idCuenta) {
        this.idCuenta = idCuenta;
    }


    /**
     * Gets the infoCHIP value for this TipoInfoMedioPago.
     * 
     * @return infoCHIP
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoCHIP getInfoCHIP() {
        return infoCHIP;
    }


    /**
     * Sets the infoCHIP value for this TipoInfoMedioPago.
     * 
     * @param infoCHIP
     */
    public void setInfoCHIP(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoCHIP infoCHIP) {
        this.infoCHIP = infoCHIP;
    }


    /**
     * Gets the infoAutenticacion value for this TipoInfoMedioPago.
     * 
     * @return infoAutenticacion
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAutenticacion getInfoAutenticacion() {
        return infoAutenticacion;
    }


    /**
     * Sets the infoAutenticacion value for this TipoInfoMedioPago.
     * 
     * @param infoAutenticacion
     */
    public void setInfoAutenticacion(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoAutenticacion infoAutenticacion) {
        this.infoAutenticacion = infoAutenticacion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoMedioPago)) return false;
        TipoInfoMedioPago other = (TipoInfoMedioPago) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idTrack==null && other.getIdTrack()==null) || 
             (this.idTrack!=null &&
              this.idTrack.equals(other.getIdTrack()))) &&
            ((this.idTarjetaDebitoPrivada==null && other.getIdTarjetaDebitoPrivada()==null) || 
             (this.idTarjetaDebitoPrivada!=null &&
              this.idTarjetaDebitoPrivada.equals(other.getIdTarjetaDebitoPrivada()))) &&
            ((this.idTarjetaCreditoPrivada==null && other.getIdTarjetaCreditoPrivada()==null) || 
             (this.idTarjetaCreditoPrivada!=null &&
              this.idTarjetaCreditoPrivada.equals(other.getIdTarjetaCreditoPrivada()))) &&
            ((this.idTarjetaCredito==null && other.getIdTarjetaCredito()==null) || 
             (this.idTarjetaCredito!=null &&
              this.idTarjetaCredito.equals(other.getIdTarjetaCredito()))) &&
            ((this.idCuenta==null && other.getIdCuenta()==null) || 
             (this.idCuenta!=null &&
              this.idCuenta.equals(other.getIdCuenta()))) &&
            ((this.infoCHIP==null && other.getInfoCHIP()==null) || 
             (this.infoCHIP!=null &&
              this.infoCHIP.equals(other.getInfoCHIP()))) &&
            ((this.infoAutenticacion==null && other.getInfoAutenticacion()==null) || 
             (this.infoAutenticacion!=null &&
              this.infoAutenticacion.equals(other.getInfoAutenticacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdTrack() != null) {
            _hashCode += getIdTrack().hashCode();
        }
        if (getIdTarjetaDebitoPrivada() != null) {
            _hashCode += getIdTarjetaDebitoPrivada().hashCode();
        }
        if (getIdTarjetaCreditoPrivada() != null) {
            _hashCode += getIdTarjetaCreditoPrivada().hashCode();
        }
        if (getIdTarjetaCredito() != null) {
            _hashCode += getIdTarjetaCredito().hashCode();
        }
        if (getIdCuenta() != null) {
            _hashCode += getIdCuenta().hashCode();
        }
        if (getInfoCHIP() != null) {
            _hashCode += getInfoCHIP().hashCode();
        }
        if (getInfoAutenticacion() != null) {
            _hashCode += getInfoAutenticacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoMedioPago.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoMedioPago"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTrack");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idTrack"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTrack"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTarjetaDebitoPrivada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idTarjetaDebitoPrivada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaDebitoPrivada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTarjetaCreditoPrivada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idTarjetaCreditoPrivada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaCreditoPrivada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTarjetaCredito");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idTarjetaCredito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaCredito"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdCuenta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoCHIP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoCHIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoCHIP"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoAutenticacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoAutenticacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoAutenticacion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
