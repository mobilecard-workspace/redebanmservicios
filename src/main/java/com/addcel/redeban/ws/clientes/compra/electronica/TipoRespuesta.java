/**
 * TipoRespuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.electronica;

public class TipoRespuesta  implements java.io.Serializable {
    private com.addcel.redeban.ws.clientes.compra.electronica.TipoCabeceraSolicitud cabeceraRespuesta;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRespuesta infoRespuesta;

    private com.addcel.redeban.ws.clientes.compra.electronica.TipoInfoCompraResp infoCompraResp;

    private java.lang.Long idTransaccionAutorizador;

    private com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoTerminal infoTerminal;

    public TipoRespuesta() {
    }

    public TipoRespuesta(
           com.addcel.redeban.ws.clientes.compra.electronica.TipoCabeceraSolicitud cabeceraRespuesta,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRespuesta infoRespuesta,
           com.addcel.redeban.ws.clientes.compra.electronica.TipoInfoCompraResp infoCompraResp,
           java.lang.Long idTransaccionAutorizador,
           com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoTerminal infoTerminal) {
           this.cabeceraRespuesta = cabeceraRespuesta;
           this.infoRespuesta = infoRespuesta;
           this.infoCompraResp = infoCompraResp;
           this.idTransaccionAutorizador = idTransaccionAutorizador;
           this.infoTerminal = infoTerminal;
    }


    /**
     * Gets the cabeceraRespuesta value for this TipoRespuesta.
     * 
     * @return cabeceraRespuesta
     */
    public com.addcel.redeban.ws.clientes.compra.electronica.TipoCabeceraSolicitud getCabeceraRespuesta() {
        return cabeceraRespuesta;
    }


    /**
     * Sets the cabeceraRespuesta value for this TipoRespuesta.
     * 
     * @param cabeceraRespuesta
     */
    public void setCabeceraRespuesta(com.addcel.redeban.ws.clientes.compra.electronica.TipoCabeceraSolicitud cabeceraRespuesta) {
        this.cabeceraRespuesta = cabeceraRespuesta;
    }


    /**
     * Gets the infoRespuesta value for this TipoRespuesta.
     * 
     * @return infoRespuesta
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRespuesta getInfoRespuesta() {
        return infoRespuesta;
    }


    /**
     * Sets the infoRespuesta value for this TipoRespuesta.
     * 
     * @param infoRespuesta
     */
    public void setInfoRespuesta(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoRespuesta infoRespuesta) {
        this.infoRespuesta = infoRespuesta;
    }


    /**
     * Gets the infoCompraResp value for this TipoRespuesta.
     * 
     * @return infoCompraResp
     */
    public com.addcel.redeban.ws.clientes.compra.electronica.TipoInfoCompraResp getInfoCompraResp() {
        return infoCompraResp;
    }


    /**
     * Sets the infoCompraResp value for this TipoRespuesta.
     * 
     * @param infoCompraResp
     */
    public void setInfoCompraResp(com.addcel.redeban.ws.clientes.compra.electronica.TipoInfoCompraResp infoCompraResp) {
        this.infoCompraResp = infoCompraResp;
    }


    /**
     * Gets the idTransaccionAutorizador value for this TipoRespuesta.
     * 
     * @return idTransaccionAutorizador
     */
    public java.lang.Long getIdTransaccionAutorizador() {
        return idTransaccionAutorizador;
    }


    /**
     * Sets the idTransaccionAutorizador value for this TipoRespuesta.
     * 
     * @param idTransaccionAutorizador
     */
    public void setIdTransaccionAutorizador(java.lang.Long idTransaccionAutorizador) {
        this.idTransaccionAutorizador = idTransaccionAutorizador;
    }


    /**
     * Gets the infoTerminal value for this TipoRespuesta.
     * 
     * @return infoTerminal
     */
    public com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoTerminal getInfoTerminal() {
        return infoTerminal;
    }


    /**
     * Sets the infoTerminal value for this TipoRespuesta.
     * 
     * @param infoTerminal
     */
    public void setInfoTerminal(com.addcel.redeban.ws.clientes.compra.vo.comercio.TipoInfoTerminal infoTerminal) {
        this.infoTerminal = infoTerminal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoRespuesta)) return false;
        TipoRespuesta other = (TipoRespuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cabeceraRespuesta==null && other.getCabeceraRespuesta()==null) || 
             (this.cabeceraRespuesta!=null &&
              this.cabeceraRespuesta.equals(other.getCabeceraRespuesta()))) &&
            ((this.infoRespuesta==null && other.getInfoRespuesta()==null) || 
             (this.infoRespuesta!=null &&
              this.infoRespuesta.equals(other.getInfoRespuesta()))) &&
            ((this.infoCompraResp==null && other.getInfoCompraResp()==null) || 
             (this.infoCompraResp!=null &&
              this.infoCompraResp.equals(other.getInfoCompraResp()))) &&
            ((this.idTransaccionAutorizador==null && other.getIdTransaccionAutorizador()==null) || 
             (this.idTransaccionAutorizador!=null &&
              this.idTransaccionAutorizador.equals(other.getIdTransaccionAutorizador()))) &&
            ((this.infoTerminal==null && other.getInfoTerminal()==null) || 
             (this.infoTerminal!=null &&
              this.infoTerminal.equals(other.getInfoTerminal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCabeceraRespuesta() != null) {
            _hashCode += getCabeceraRespuesta().hashCode();
        }
        if (getInfoRespuesta() != null) {
            _hashCode += getInfoRespuesta().hashCode();
        }
        if (getInfoCompraResp() != null) {
            _hashCode += getInfoCompraResp().hashCode();
        }
        if (getIdTransaccionAutorizador() != null) {
            _hashCode += getIdTransaccionAutorizador().hashCode();
        }
        if (getInfoTerminal() != null) {
            _hashCode += getInfoTerminal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoRespuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoRespuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabeceraRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "cabeceraRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoCabeceraSolicitud"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRespuesta"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoCompraResp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoCompraResp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompraResp"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTransaccionAutorizador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "idTransaccionAutorizador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoTerminal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoTerminal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoTerminal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
