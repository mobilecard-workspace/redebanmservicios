/**
 * TipoIdPersona.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.vo;

public class TipoIdPersona  implements java.io.Serializable {
    private com.addcel.redeban.ws.clientes.compra.vo.TipoTipoDocumento tipoDocumento;

    private long numDocumento;

    public TipoIdPersona() {
    }

    public TipoIdPersona(
           com.addcel.redeban.ws.clientes.compra.vo.TipoTipoDocumento tipoDocumento,
           long numDocumento) {
           this.tipoDocumento = tipoDocumento;
           this.numDocumento = numDocumento;
    }


    /**
     * Gets the tipoDocumento value for this TipoIdPersona.
     * 
     * @return tipoDocumento
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoTipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }


    /**
     * Sets the tipoDocumento value for this TipoIdPersona.
     * 
     * @param tipoDocumento
     */
    public void setTipoDocumento(com.addcel.redeban.ws.clientes.compra.vo.TipoTipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }


    /**
     * Gets the numDocumento value for this TipoIdPersona.
     * 
     * @return numDocumento
     */
    public long getNumDocumento() {
        return numDocumento;
    }


    /**
     * Sets the numDocumento value for this TipoIdPersona.
     * 
     * @param numDocumento
     */
    public void setNumDocumento(long numDocumento) {
        this.numDocumento = numDocumento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoIdPersona)) return false;
        TipoIdPersona other = (TipoIdPersona) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipoDocumento==null && other.getTipoDocumento()==null) || 
             (this.tipoDocumento!=null &&
              this.tipoDocumento.equals(other.getTipoDocumento()))) &&
            this.numDocumento == other.getNumDocumento();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipoDocumento() != null) {
            _hashCode += getTipoDocumento().hashCode();
        }
        _hashCode += new Long(getNumDocumento()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoIdPersona.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdPersona"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoDocumento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "tipoDocumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoDocumento"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDocumento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "numDocumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
