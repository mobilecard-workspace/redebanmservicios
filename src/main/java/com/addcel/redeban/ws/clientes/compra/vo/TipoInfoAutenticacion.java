/**
 * TipoInfoAutenticacion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.vo;

public class TipoInfoAutenticacion  implements java.io.Serializable {
    private byte[] clave;

    private java.lang.String formatoClave;

    public TipoInfoAutenticacion() {
    }

    public TipoInfoAutenticacion(
           byte[] clave,
           java.lang.String formatoClave) {
           this.clave = clave;
           this.formatoClave = formatoClave;
    }


    /**
     * Gets the clave value for this TipoInfoAutenticacion.
     * 
     * @return clave
     */
    public byte[] getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this TipoInfoAutenticacion.
     * 
     * @param clave
     */
    public void setClave(byte[] clave) {
        this.clave = clave;
    }


    /**
     * Gets the formatoClave value for this TipoInfoAutenticacion.
     * 
     * @return formatoClave
     */
    public java.lang.String getFormatoClave() {
        return formatoClave;
    }


    /**
     * Sets the formatoClave value for this TipoInfoAutenticacion.
     * 
     * @param formatoClave
     */
    public void setFormatoClave(java.lang.String formatoClave) {
        this.formatoClave = formatoClave;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoAutenticacion)) return false;
        TipoInfoAutenticacion other = (TipoInfoAutenticacion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clave==null && other.getClave()==null) || 
             (this.clave!=null &&
              java.util.Arrays.equals(this.clave, other.getClave()))) &&
            ((this.formatoClave==null && other.getFormatoClave()==null) || 
             (this.formatoClave!=null &&
              this.formatoClave.equals(other.getFormatoClave())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClave() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClave());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClave(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFormatoClave() != null) {
            _hashCode += getFormatoClave().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoAutenticacion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoAutenticacion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clave");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "hexBinary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formatoClave");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "formatoClave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
