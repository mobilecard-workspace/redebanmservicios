/**
 * TipoInfoCHIP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.vo;

public class TipoInfoCHIP  implements java.io.Serializable {
    private java.lang.String codSeguridadEMV;

    private byte[] dataEMV;

    public TipoInfoCHIP() {
    }

    public TipoInfoCHIP(
           java.lang.String codSeguridadEMV,
           byte[] dataEMV) {
           this.codSeguridadEMV = codSeguridadEMV;
           this.dataEMV = dataEMV;
    }


    /**
     * Gets the codSeguridadEMV value for this TipoInfoCHIP.
     * 
     * @return codSeguridadEMV
     */
    public java.lang.String getCodSeguridadEMV() {
        return codSeguridadEMV;
    }


    /**
     * Sets the codSeguridadEMV value for this TipoInfoCHIP.
     * 
     * @param codSeguridadEMV
     */
    public void setCodSeguridadEMV(java.lang.String codSeguridadEMV) {
        this.codSeguridadEMV = codSeguridadEMV;
    }


    /**
     * Gets the dataEMV value for this TipoInfoCHIP.
     * 
     * @return dataEMV
     */
    public byte[] getDataEMV() {
        return dataEMV;
    }


    /**
     * Sets the dataEMV value for this TipoInfoCHIP.
     * 
     * @param dataEMV
     */
    public void setDataEMV(byte[] dataEMV) {
        this.dataEMV = dataEMV;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoCHIP)) return false;
        TipoInfoCHIP other = (TipoInfoCHIP) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codSeguridadEMV==null && other.getCodSeguridadEMV()==null) || 
             (this.codSeguridadEMV!=null &&
              this.codSeguridadEMV.equals(other.getCodSeguridadEMV()))) &&
            ((this.dataEMV==null && other.getDataEMV()==null) || 
             (this.dataEMV!=null &&
              java.util.Arrays.equals(this.dataEMV, other.getDataEMV())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodSeguridadEMV() != null) {
            _hashCode += getCodSeguridadEMV().hashCode();
        }
        if (getDataEMV() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDataEMV());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDataEMV(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoCHIP.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoCHIP"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSeguridadEMV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "codSeguridadEMV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEMV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "dataEMV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "hexBinary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
