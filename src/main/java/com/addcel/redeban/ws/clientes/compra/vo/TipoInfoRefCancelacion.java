/**
 * TipoInfoRefCancelacion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.vo;

public class TipoInfoRefCancelacion  implements java.io.Serializable {
    private java.lang.String numAprobacion;

    private long idTransaccionAutorizador;

    public TipoInfoRefCancelacion() {
    }

    public TipoInfoRefCancelacion(
           java.lang.String numAprobacion,
           long idTransaccionAutorizador) {
           this.numAprobacion = numAprobacion;
           this.idTransaccionAutorizador = idTransaccionAutorizador;
    }


    /**
     * Gets the numAprobacion value for this TipoInfoRefCancelacion.
     * 
     * @return numAprobacion
     */
    public java.lang.String getNumAprobacion() {
        return numAprobacion;
    }


    /**
     * Sets the numAprobacion value for this TipoInfoRefCancelacion.
     * 
     * @param numAprobacion
     */
    public void setNumAprobacion(java.lang.String numAprobacion) {
        this.numAprobacion = numAprobacion;
    }


    /**
     * Gets the idTransaccionAutorizador value for this TipoInfoRefCancelacion.
     * 
     * @return idTransaccionAutorizador
     */
    public long getIdTransaccionAutorizador() {
        return idTransaccionAutorizador;
    }


    /**
     * Sets the idTransaccionAutorizador value for this TipoInfoRefCancelacion.
     * 
     * @param idTransaccionAutorizador
     */
    public void setIdTransaccionAutorizador(long idTransaccionAutorizador) {
        this.idTransaccionAutorizador = idTransaccionAutorizador;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoRefCancelacion)) return false;
        TipoInfoRefCancelacion other = (TipoInfoRefCancelacion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.numAprobacion==null && other.getNumAprobacion()==null) || 
             (this.numAprobacion!=null &&
              this.numAprobacion.equals(other.getNumAprobacion()))) &&
            this.idTransaccionAutorizador == other.getIdTransaccionAutorizador();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNumAprobacion() != null) {
            _hashCode += getNumAprobacion().hashCode();
        }
        _hashCode += new Long(getIdTransaccionAutorizador()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoRefCancelacion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRefCancelacion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAprobacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "numAprobacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTransaccionAutorizador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "idTransaccionAutorizador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
