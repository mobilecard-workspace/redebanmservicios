/**
 * TipoTipoBolsillo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.vo;

public class TipoTipoBolsillo implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoTipoBolsillo(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Lealtad = "Lealtad";
    public static final java.lang.String _CuotaMonetaria = "CuotaMonetaria";
    public static final java.lang.String _CupoCredito = "CupoCredito";
    public static final java.lang.String _BonoEfectivo = "BonoEfectivo";
    public static final java.lang.String _CreditoRotativoBolsillo = "CreditoRotativoBolsillo";
    public static final java.lang.String _BonoRegalo = "BonoRegalo";
    public static final java.lang.String _BonoDescuento = "BonoDescuento";
    public static final java.lang.String _BolsilloCredito = "BolsilloCredito";
    public static final java.lang.String _BolsilloDebito = "BolsilloDebito";
    public static final java.lang.String _Ahorros = "Ahorros";
    public static final java.lang.String _Corriente = "Corriente";
    public static final java.lang.String _Credito = "Credito";
    public static final java.lang.String _Desconocida = "Desconocida";
    public static final java.lang.String _CreditoRotativo = "CreditoRotativo";
    public static final TipoTipoBolsillo Lealtad = new TipoTipoBolsillo(_Lealtad);
    public static final TipoTipoBolsillo CuotaMonetaria = new TipoTipoBolsillo(_CuotaMonetaria);
    public static final TipoTipoBolsillo CupoCredito = new TipoTipoBolsillo(_CupoCredito);
    public static final TipoTipoBolsillo BonoEfectivo = new TipoTipoBolsillo(_BonoEfectivo);
    public static final TipoTipoBolsillo CreditoRotativoBolsillo = new TipoTipoBolsillo(_CreditoRotativoBolsillo);
    public static final TipoTipoBolsillo BonoRegalo = new TipoTipoBolsillo(_BonoRegalo);
    public static final TipoTipoBolsillo BonoDescuento = new TipoTipoBolsillo(_BonoDescuento);
    public static final TipoTipoBolsillo BolsilloCredito = new TipoTipoBolsillo(_BolsilloCredito);
    public static final TipoTipoBolsillo BolsilloDebito = new TipoTipoBolsillo(_BolsilloDebito);
    public static final TipoTipoBolsillo Ahorros = new TipoTipoBolsillo(_Ahorros);
    public static final TipoTipoBolsillo Corriente = new TipoTipoBolsillo(_Corriente);
    public static final TipoTipoBolsillo Credito = new TipoTipoBolsillo(_Credito);
    public static final TipoTipoBolsillo Desconocida = new TipoTipoBolsillo(_Desconocida);
    public static final TipoTipoBolsillo CreditoRotativo = new TipoTipoBolsillo(_CreditoRotativo);
    public java.lang.String getValue() { return _value_;}
    public static TipoTipoBolsillo fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoTipoBolsillo enumeration = (TipoTipoBolsillo)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoTipoBolsillo fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoTipoBolsillo.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoBolsillo"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
