/**
 * TipoInfoTerminal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.compra.vo.comercio;

public class TipoInfoTerminal  implements java.io.Serializable {
    private java.lang.String nombreAdquiriente;

    private com.addcel.redeban.ws.clientes.compra.vo.TipoInfoUbicacion infoUbicacion;

    public TipoInfoTerminal() {
    }

    public TipoInfoTerminal(
           java.lang.String nombreAdquiriente,
           com.addcel.redeban.ws.clientes.compra.vo.TipoInfoUbicacion infoUbicacion) {
           this.nombreAdquiriente = nombreAdquiriente;
           this.infoUbicacion = infoUbicacion;
    }


    /**
     * Gets the nombreAdquiriente value for this TipoInfoTerminal.
     * 
     * @return nombreAdquiriente
     */
    public java.lang.String getNombreAdquiriente() {
        return nombreAdquiriente;
    }


    /**
     * Sets the nombreAdquiriente value for this TipoInfoTerminal.
     * 
     * @param nombreAdquiriente
     */
    public void setNombreAdquiriente(java.lang.String nombreAdquiriente) {
        this.nombreAdquiriente = nombreAdquiriente;
    }


    /**
     * Gets the infoUbicacion value for this TipoInfoTerminal.
     * 
     * @return infoUbicacion
     */
    public com.addcel.redeban.ws.clientes.compra.vo.TipoInfoUbicacion getInfoUbicacion() {
        return infoUbicacion;
    }


    /**
     * Sets the infoUbicacion value for this TipoInfoTerminal.
     * 
     * @param infoUbicacion
     */
    public void setInfoUbicacion(com.addcel.redeban.ws.clientes.compra.vo.TipoInfoUbicacion infoUbicacion) {
        this.infoUbicacion = infoUbicacion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoTerminal)) return false;
        TipoInfoTerminal other = (TipoInfoTerminal) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nombreAdquiriente==null && other.getNombreAdquiriente()==null) || 
             (this.nombreAdquiriente!=null &&
              this.nombreAdquiriente.equals(other.getNombreAdquiriente()))) &&
            ((this.infoUbicacion==null && other.getInfoUbicacion()==null) || 
             (this.infoUbicacion!=null &&
              this.infoUbicacion.equals(other.getInfoUbicacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNombreAdquiriente() != null) {
            _hashCode += getNombreAdquiriente().hashCode();
        }
        if (getInfoUbicacion() != null) {
            _hashCode += getInfoUbicacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoTerminal.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoTerminal"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreAdquiriente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "nombreAdquiriente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoUbicacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "infoUbicacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoUbicacion"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
