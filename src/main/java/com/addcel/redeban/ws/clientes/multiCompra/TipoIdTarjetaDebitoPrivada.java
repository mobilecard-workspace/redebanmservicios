/**
 * TipoIdTarjetaDebitoPrivada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra;

public class TipoIdTarjetaDebitoPrivada  implements java.io.Serializable {
    private java.lang.String franquicia;

    private java.lang.String numTarjeta;

    private com.addcel.redeban.ws.clientes.multiCompra.TipoTipoBolsillo tipoBolsillo;

    public TipoIdTarjetaDebitoPrivada() {
    }

    public TipoIdTarjetaDebitoPrivada(
           java.lang.String franquicia,
           java.lang.String numTarjeta,
           com.addcel.redeban.ws.clientes.multiCompra.TipoTipoBolsillo tipoBolsillo) {
           this.franquicia = franquicia;
           this.numTarjeta = numTarjeta;
           this.tipoBolsillo = tipoBolsillo;
    }


    /**
     * Gets the franquicia value for this TipoIdTarjetaDebitoPrivada.
     * 
     * @return franquicia
     */
    public java.lang.String getFranquicia() {
        return franquicia;
    }


    /**
     * Sets the franquicia value for this TipoIdTarjetaDebitoPrivada.
     * 
     * @param franquicia
     */
    public void setFranquicia(java.lang.String franquicia) {
        this.franquicia = franquicia;
    }


    /**
     * Gets the numTarjeta value for this TipoIdTarjetaDebitoPrivada.
     * 
     * @return numTarjeta
     */
    public java.lang.String getNumTarjeta() {
        return numTarjeta;
    }


    /**
     * Sets the numTarjeta value for this TipoIdTarjetaDebitoPrivada.
     * 
     * @param numTarjeta
     */
    public void setNumTarjeta(java.lang.String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }


    /**
     * Gets the tipoBolsillo value for this TipoIdTarjetaDebitoPrivada.
     * 
     * @return tipoBolsillo
     */
    public com.addcel.redeban.ws.clientes.multiCompra.TipoTipoBolsillo getTipoBolsillo() {
        return tipoBolsillo;
    }


    /**
     * Sets the tipoBolsillo value for this TipoIdTarjetaDebitoPrivada.
     * 
     * @param tipoBolsillo
     */
    public void setTipoBolsillo(com.addcel.redeban.ws.clientes.multiCompra.TipoTipoBolsillo tipoBolsillo) {
        this.tipoBolsillo = tipoBolsillo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoIdTarjetaDebitoPrivada)) return false;
        TipoIdTarjetaDebitoPrivada other = (TipoIdTarjetaDebitoPrivada) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.franquicia==null && other.getFranquicia()==null) || 
             (this.franquicia!=null &&
              this.franquicia.equals(other.getFranquicia()))) &&
            ((this.numTarjeta==null && other.getNumTarjeta()==null) || 
             (this.numTarjeta!=null &&
              this.numTarjeta.equals(other.getNumTarjeta()))) &&
            ((this.tipoBolsillo==null && other.getTipoBolsillo()==null) || 
             (this.tipoBolsillo!=null &&
              this.tipoBolsillo.equals(other.getTipoBolsillo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFranquicia() != null) {
            _hashCode += getFranquicia().hashCode();
        }
        if (getNumTarjeta() != null) {
            _hashCode += getNumTarjeta().hashCode();
        }
        if (getTipoBolsillo() != null) {
            _hashCode += getTipoBolsillo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoIdTarjetaDebitoPrivada.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoIdTarjetaDebitoPrivada"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("franquicia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "franquicia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "numTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoBolsillo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "tipoBolsillo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoBolsillo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
