/**
 * TipoInfoImpuestos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra;

public class TipoInfoImpuestos  implements java.io.Serializable {
    private com.addcel.redeban.ws.clientes.multiCompra.TipoTipoImpuesto tipoImpuesto;

    private java.math.BigDecimal monto;

    private java.math.BigDecimal baseImpuesto;

    public TipoInfoImpuestos() {
    }

    public TipoInfoImpuestos(
           com.addcel.redeban.ws.clientes.multiCompra.TipoTipoImpuesto tipoImpuesto,
           java.math.BigDecimal monto,
           java.math.BigDecimal baseImpuesto) {
           this.tipoImpuesto = tipoImpuesto;
           this.monto = monto;
           this.baseImpuesto = baseImpuesto;
    }


    /**
     * Gets the tipoImpuesto value for this TipoInfoImpuestos.
     * 
     * @return tipoImpuesto
     */
    public com.addcel.redeban.ws.clientes.multiCompra.TipoTipoImpuesto getTipoImpuesto() {
        return tipoImpuesto;
    }


    /**
     * Sets the tipoImpuesto value for this TipoInfoImpuestos.
     * 
     * @param tipoImpuesto
     */
    public void setTipoImpuesto(com.addcel.redeban.ws.clientes.multiCompra.TipoTipoImpuesto tipoImpuesto) {
        this.tipoImpuesto = tipoImpuesto;
    }


    /**
     * Gets the monto value for this TipoInfoImpuestos.
     * 
     * @return monto
     */
    public java.math.BigDecimal getMonto() {
        return monto;
    }


    /**
     * Sets the monto value for this TipoInfoImpuestos.
     * 
     * @param monto
     */
    public void setMonto(java.math.BigDecimal monto) {
        this.monto = monto;
    }


    /**
     * Gets the baseImpuesto value for this TipoInfoImpuestos.
     * 
     * @return baseImpuesto
     */
    public java.math.BigDecimal getBaseImpuesto() {
        return baseImpuesto;
    }


    /**
     * Sets the baseImpuesto value for this TipoInfoImpuestos.
     * 
     * @param baseImpuesto
     */
    public void setBaseImpuesto(java.math.BigDecimal baseImpuesto) {
        this.baseImpuesto = baseImpuesto;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoImpuestos)) return false;
        TipoInfoImpuestos other = (TipoInfoImpuestos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipoImpuesto==null && other.getTipoImpuesto()==null) || 
             (this.tipoImpuesto!=null &&
              this.tipoImpuesto.equals(other.getTipoImpuesto()))) &&
            ((this.monto==null && other.getMonto()==null) || 
             (this.monto!=null &&
              this.monto.equals(other.getMonto()))) &&
            ((this.baseImpuesto==null && other.getBaseImpuesto()==null) || 
             (this.baseImpuesto!=null &&
              this.baseImpuesto.equals(other.getBaseImpuesto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipoImpuesto() != null) {
            _hashCode += getTipoImpuesto().hashCode();
        }
        if (getMonto() != null) {
            _hashCode += getMonto().hashCode();
        }
        if (getBaseImpuesto() != null) {
            _hashCode += getBaseImpuesto().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoImpuestos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoImpuestos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoImpuesto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "tipoImpuesto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoTipoImpuesto"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "monto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseImpuesto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "baseImpuesto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
