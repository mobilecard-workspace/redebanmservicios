/**
 * TipoInfoRespuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra;

public class TipoInfoRespuesta  implements java.io.Serializable {
    private java.lang.String codRespuesta;

    private java.lang.String descRespuesta;

    private java.lang.String estado;

    public TipoInfoRespuesta() {
    }

    public TipoInfoRespuesta(
           java.lang.String codRespuesta,
           java.lang.String descRespuesta,
           java.lang.String estado) {
           this.codRespuesta = codRespuesta;
           this.descRespuesta = descRespuesta;
           this.estado = estado;
    }


    /**
     * Gets the codRespuesta value for this TipoInfoRespuesta.
     * 
     * @return codRespuesta
     */
    public java.lang.String getCodRespuesta() {
        return codRespuesta;
    }


    /**
     * Sets the codRespuesta value for this TipoInfoRespuesta.
     * 
     * @param codRespuesta
     */
    public void setCodRespuesta(java.lang.String codRespuesta) {
        this.codRespuesta = codRespuesta;
    }


    /**
     * Gets the descRespuesta value for this TipoInfoRespuesta.
     * 
     * @return descRespuesta
     */
    public java.lang.String getDescRespuesta() {
        return descRespuesta;
    }


    /**
     * Sets the descRespuesta value for this TipoInfoRespuesta.
     * 
     * @param descRespuesta
     */
    public void setDescRespuesta(java.lang.String descRespuesta) {
        this.descRespuesta = descRespuesta;
    }


    /**
     * Gets the estado value for this TipoInfoRespuesta.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this TipoInfoRespuesta.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoRespuesta)) return false;
        TipoInfoRespuesta other = (TipoInfoRespuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codRespuesta==null && other.getCodRespuesta()==null) || 
             (this.codRespuesta!=null &&
              this.codRespuesta.equals(other.getCodRespuesta()))) &&
            ((this.descRespuesta==null && other.getDescRespuesta()==null) || 
             (this.descRespuesta!=null &&
              this.descRespuesta.equals(other.getDescRespuesta()))) &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodRespuesta() != null) {
            _hashCode += getCodRespuesta().hashCode();
        }
        if (getDescRespuesta() != null) {
            _hashCode += getDescRespuesta().hashCode();
        }
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoRespuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRespuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "codRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "descRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
