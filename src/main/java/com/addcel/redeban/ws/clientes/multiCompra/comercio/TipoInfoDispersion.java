/**
 * TipoInfoDispersion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra.comercio;

public class TipoInfoDispersion  implements java.io.Serializable {
    private java.lang.String idTerminal;

    private java.lang.String idAdquiriente;

    private com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompra infoCompra;

    public TipoInfoDispersion() {
    }

    public TipoInfoDispersion(
           java.lang.String idTerminal,
           java.lang.String idAdquiriente,
           com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompra infoCompra) {
           this.idTerminal = idTerminal;
           this.idAdquiriente = idAdquiriente;
           this.infoCompra = infoCompra;
    }


    /**
     * Gets the idTerminal value for this TipoInfoDispersion.
     * 
     * @return idTerminal
     */
    public java.lang.String getIdTerminal() {
        return idTerminal;
    }


    /**
     * Sets the idTerminal value for this TipoInfoDispersion.
     * 
     * @param idTerminal
     */
    public void setIdTerminal(java.lang.String idTerminal) {
        this.idTerminal = idTerminal;
    }


    /**
     * Gets the idAdquiriente value for this TipoInfoDispersion.
     * 
     * @return idAdquiriente
     */
    public java.lang.String getIdAdquiriente() {
        return idAdquiriente;
    }


    /**
     * Sets the idAdquiriente value for this TipoInfoDispersion.
     * 
     * @param idAdquiriente
     */
    public void setIdAdquiriente(java.lang.String idAdquiriente) {
        this.idAdquiriente = idAdquiriente;
    }


    /**
     * Gets the infoCompra value for this TipoInfoDispersion.
     * 
     * @return infoCompra
     */
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompra getInfoCompra() {
        return infoCompra;
    }


    /**
     * Sets the infoCompra value for this TipoInfoDispersion.
     * 
     * @param infoCompra
     */
    public void setInfoCompra(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompra infoCompra) {
        this.infoCompra = infoCompra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoDispersion)) return false;
        TipoInfoDispersion other = (TipoInfoDispersion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idTerminal==null && other.getIdTerminal()==null) || 
             (this.idTerminal!=null &&
              this.idTerminal.equals(other.getIdTerminal()))) &&
            ((this.idAdquiriente==null && other.getIdAdquiriente()==null) || 
             (this.idAdquiriente!=null &&
              this.idAdquiriente.equals(other.getIdAdquiriente()))) &&
            ((this.infoCompra==null && other.getInfoCompra()==null) || 
             (this.infoCompra!=null &&
              this.infoCompra.equals(other.getInfoCompra())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdTerminal() != null) {
            _hashCode += getIdTerminal().hashCode();
        }
        if (getIdAdquiriente() != null) {
            _hashCode += getIdAdquiriente().hashCode();
        }
        if (getInfoCompra() != null) {
            _hashCode += getInfoCompra().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoDispersion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoDispersion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTerminal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "idTerminal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idAdquiriente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "idAdquiriente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoCompra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "infoCompra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompra"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
