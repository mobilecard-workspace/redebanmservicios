/**
 * TipoInfoDispersionResp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra.comercio;

public class TipoInfoDispersionResp  implements java.io.Serializable {
    private java.lang.String idTerminal;

    private java.lang.String idAdquiriente;

    private com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompraResp infoCompraResp;

    private long idTransaccionAutorizador;

    private com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoTerminal infoTerminal;

    public TipoInfoDispersionResp() {
    }

    public TipoInfoDispersionResp(
           java.lang.String idTerminal,
           java.lang.String idAdquiriente,
           com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompraResp infoCompraResp,
           long idTransaccionAutorizador,
           com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoTerminal infoTerminal) {
           this.idTerminal = idTerminal;
           this.idAdquiriente = idAdquiriente;
           this.infoCompraResp = infoCompraResp;
           this.idTransaccionAutorizador = idTransaccionAutorizador;
           this.infoTerminal = infoTerminal;
    }


    /**
     * Gets the idTerminal value for this TipoInfoDispersionResp.
     * 
     * @return idTerminal
     */
    public java.lang.String getIdTerminal() {
        return idTerminal;
    }


    /**
     * Sets the idTerminal value for this TipoInfoDispersionResp.
     * 
     * @param idTerminal
     */
    public void setIdTerminal(java.lang.String idTerminal) {
        this.idTerminal = idTerminal;
    }


    /**
     * Gets the idAdquiriente value for this TipoInfoDispersionResp.
     * 
     * @return idAdquiriente
     */
    public java.lang.String getIdAdquiriente() {
        return idAdquiriente;
    }


    /**
     * Sets the idAdquiriente value for this TipoInfoDispersionResp.
     * 
     * @param idAdquiriente
     */
    public void setIdAdquiriente(java.lang.String idAdquiriente) {
        this.idAdquiriente = idAdquiriente;
    }


    /**
     * Gets the infoCompraResp value for this TipoInfoDispersionResp.
     * 
     * @return infoCompraResp
     */
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompraResp getInfoCompraResp() {
        return infoCompraResp;
    }


    /**
     * Sets the infoCompraResp value for this TipoInfoDispersionResp.
     * 
     * @param infoCompraResp
     */
    public void setInfoCompraResp(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoInfoCompraResp infoCompraResp) {
        this.infoCompraResp = infoCompraResp;
    }


    /**
     * Gets the idTransaccionAutorizador value for this TipoInfoDispersionResp.
     * 
     * @return idTransaccionAutorizador
     */
    public long getIdTransaccionAutorizador() {
        return idTransaccionAutorizador;
    }


    /**
     * Sets the idTransaccionAutorizador value for this TipoInfoDispersionResp.
     * 
     * @param idTransaccionAutorizador
     */
    public void setIdTransaccionAutorizador(long idTransaccionAutorizador) {
        this.idTransaccionAutorizador = idTransaccionAutorizador;
    }


    /**
     * Gets the infoTerminal value for this TipoInfoDispersionResp.
     * 
     * @return infoTerminal
     */
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoTerminal getInfoTerminal() {
        return infoTerminal;
    }


    /**
     * Sets the infoTerminal value for this TipoInfoDispersionResp.
     * 
     * @param infoTerminal
     */
    public void setInfoTerminal(com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoTerminal infoTerminal) {
        this.infoTerminal = infoTerminal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoInfoDispersionResp)) return false;
        TipoInfoDispersionResp other = (TipoInfoDispersionResp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idTerminal==null && other.getIdTerminal()==null) || 
             (this.idTerminal!=null &&
              this.idTerminal.equals(other.getIdTerminal()))) &&
            ((this.idAdquiriente==null && other.getIdAdquiriente()==null) || 
             (this.idAdquiriente!=null &&
              this.idAdquiriente.equals(other.getIdAdquiriente()))) &&
            ((this.infoCompraResp==null && other.getInfoCompraResp()==null) || 
             (this.infoCompraResp!=null &&
              this.infoCompraResp.equals(other.getInfoCompraResp()))) &&
            this.idTransaccionAutorizador == other.getIdTransaccionAutorizador() &&
            ((this.infoTerminal==null && other.getInfoTerminal()==null) || 
             (this.infoTerminal!=null &&
              this.infoTerminal.equals(other.getInfoTerminal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdTerminal() != null) {
            _hashCode += getIdTerminal().hashCode();
        }
        if (getIdAdquiriente() != null) {
            _hashCode += getIdAdquiriente().hashCode();
        }
        if (getInfoCompraResp() != null) {
            _hashCode += getInfoCompraResp().hashCode();
        }
        _hashCode += new Long(getIdTransaccionAutorizador()).hashCode();
        if (getInfoTerminal() != null) {
            _hashCode += getInfoTerminal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoInfoDispersionResp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoDispersionResp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTerminal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "idTerminal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idAdquiriente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "idAdquiriente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoCompraResp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "infoCompraResp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoInfoCompraResp"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTransaccionAutorizador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "idTransaccionAutorizador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoTerminal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "infoTerminal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoTerminal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
