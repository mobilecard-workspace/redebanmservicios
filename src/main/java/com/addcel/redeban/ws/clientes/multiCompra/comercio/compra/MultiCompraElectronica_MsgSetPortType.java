/**
 * MultiCompraElectronica_MsgSetPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra.comercio.compra;

public interface MultiCompraElectronica_MsgSetPortType extends java.rmi.Remote {
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoRespuesta multiCompraReversar(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoSolicitudCompra multiCompraReversarSolicitud) throws java.rmi.RemoteException;
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoRespuesta multiCompraProcesar(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoSolicitudCompra multiCompraProcesarSolicitud) throws java.rmi.RemoteException;
}
