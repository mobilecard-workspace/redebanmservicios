package com.addcel.redeban.ws.clientes.multiCompra.comercio.compra;

public class MultiCompraElectronica_MsgSetPortTypeProxy implements com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.MultiCompraElectronica_MsgSetPortType {
  private String _endpoint = null;
  private com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.MultiCompraElectronica_MsgSetPortType multiCompraElectronica_MsgSetPortType = null;
  
  public MultiCompraElectronica_MsgSetPortTypeProxy() {
    _initMultiCompraElectronica_MsgSetPortTypeProxy();
  }
  
  public MultiCompraElectronica_MsgSetPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initMultiCompraElectronica_MsgSetPortTypeProxy();
  }
  
  private void _initMultiCompraElectronica_MsgSetPortTypeProxy() {
    try {
      multiCompraElectronica_MsgSetPortType = (new com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.MultiCompraElectronica_HTTP_ServiceLocator()).getMultiCompraElectronica_HTTP_Port();
      if (multiCompraElectronica_MsgSetPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)multiCompraElectronica_MsgSetPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)multiCompraElectronica_MsgSetPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (multiCompraElectronica_MsgSetPortType != null)
      ((javax.xml.rpc.Stub)multiCompraElectronica_MsgSetPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.MultiCompraElectronica_MsgSetPortType getMultiCompraElectronica_MsgSetPortType() {
    if (multiCompraElectronica_MsgSetPortType == null)
      _initMultiCompraElectronica_MsgSetPortTypeProxy();
    return multiCompraElectronica_MsgSetPortType;
  }
  
  public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoRespuesta multiCompraReversar(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoSolicitudCompra multiCompraReversarSolicitud) throws java.rmi.RemoteException{
    if (multiCompraElectronica_MsgSetPortType == null)
      _initMultiCompraElectronica_MsgSetPortTypeProxy();
    return multiCompraElectronica_MsgSetPortType.multiCompraReversar(multiCompraReversarSolicitud);
  }
  
  public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoRespuesta multiCompraProcesar(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoSolicitudCompra multiCompraProcesarSolicitud) throws java.rmi.RemoteException{
    if (multiCompraElectronica_MsgSetPortType == null)
      _initMultiCompraElectronica_MsgSetPortTypeProxy();
    return multiCompraElectronica_MsgSetPortType.multiCompraProcesar(multiCompraProcesarSolicitud);
  }
  
  
}