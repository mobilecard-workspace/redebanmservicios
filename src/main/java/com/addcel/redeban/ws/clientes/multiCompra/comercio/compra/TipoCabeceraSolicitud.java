/**
 * TipoCabeceraSolicitud.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra.comercio.compra;

public class TipoCabeceraSolicitud  implements java.io.Serializable {
    private com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoPuntoInteraccion infoPuntoInteraccion;

    public TipoCabeceraSolicitud() {
    }

    public TipoCabeceraSolicitud(
           com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoPuntoInteraccion infoPuntoInteraccion) {
           this.infoPuntoInteraccion = infoPuntoInteraccion;
    }


    /**
     * Gets the infoPuntoInteraccion value for this TipoCabeceraSolicitud.
     * 
     * @return infoPuntoInteraccion
     */
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoPuntoInteraccion getInfoPuntoInteraccion() {
        return infoPuntoInteraccion;
    }


    /**
     * Sets the infoPuntoInteraccion value for this TipoCabeceraSolicitud.
     * 
     * @param infoPuntoInteraccion
     */
    public void setInfoPuntoInteraccion(com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoPuntoInteraccion infoPuntoInteraccion) {
        this.infoPuntoInteraccion = infoPuntoInteraccion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoCabeceraSolicitud)) return false;
        TipoCabeceraSolicitud other = (TipoCabeceraSolicitud) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.infoPuntoInteraccion==null && other.getInfoPuntoInteraccion()==null) || 
             (this.infoPuntoInteraccion!=null &&
              this.infoPuntoInteraccion.equals(other.getInfoPuntoInteraccion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInfoPuntoInteraccion() != null) {
            _hashCode += getInfoPuntoInteraccion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoCabeceraSolicitud.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoCabeceraSolicitud"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoPuntoInteraccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoPuntoInteraccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoPuntoInteraccion"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
