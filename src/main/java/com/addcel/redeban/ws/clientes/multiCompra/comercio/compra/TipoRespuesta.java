/**
 * TipoRespuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.redeban.ws.clientes.multiCompra.comercio.compra;

public class TipoRespuesta  implements java.io.Serializable {
    private com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoCabeceraSolicitud cabeceraRespuesta;

    private com.addcel.redeban.ws.clientes.multiCompra.TipoInfoRespuesta infoRespuesta;

    private com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp[] infoDispersionResp;

    public TipoRespuesta() {
    }

    public TipoRespuesta(
           com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoCabeceraSolicitud cabeceraRespuesta,
           com.addcel.redeban.ws.clientes.multiCompra.TipoInfoRespuesta infoRespuesta,
           com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp[] infoDispersionResp) {
           this.cabeceraRespuesta = cabeceraRespuesta;
           this.infoRespuesta = infoRespuesta;
           this.infoDispersionResp = infoDispersionResp;
    }


    /**
     * Gets the cabeceraRespuesta value for this TipoRespuesta.
     * 
     * @return cabeceraRespuesta
     */
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoCabeceraSolicitud getCabeceraRespuesta() {
        return cabeceraRespuesta;
    }


    /**
     * Sets the cabeceraRespuesta value for this TipoRespuesta.
     * 
     * @param cabeceraRespuesta
     */
    public void setCabeceraRespuesta(com.addcel.redeban.ws.clientes.multiCompra.comercio.compra.TipoCabeceraSolicitud cabeceraRespuesta) {
        this.cabeceraRespuesta = cabeceraRespuesta;
    }


    /**
     * Gets the infoRespuesta value for this TipoRespuesta.
     * 
     * @return infoRespuesta
     */
    public com.addcel.redeban.ws.clientes.multiCompra.TipoInfoRespuesta getInfoRespuesta() {
        return infoRespuesta;
    }


    /**
     * Sets the infoRespuesta value for this TipoRespuesta.
     * 
     * @param infoRespuesta
     */
    public void setInfoRespuesta(com.addcel.redeban.ws.clientes.multiCompra.TipoInfoRespuesta infoRespuesta) {
        this.infoRespuesta = infoRespuesta;
    }


    /**
     * Gets the infoDispersionResp value for this TipoRespuesta.
     * 
     * @return infoDispersionResp
     */
    public com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp[] getInfoDispersionResp() {
        return infoDispersionResp;
    }


    /**
     * Sets the infoDispersionResp value for this TipoRespuesta.
     * 
     * @param infoDispersionResp
     */
    public void setInfoDispersionResp(com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp[] infoDispersionResp) {
        this.infoDispersionResp = infoDispersionResp;
    }

    public com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp getInfoDispersionResp(int i) {
        return this.infoDispersionResp[i];
    }

    public void setInfoDispersionResp(int i, com.addcel.redeban.ws.clientes.multiCompra.comercio.TipoInfoDispersionResp _value) {
        this.infoDispersionResp[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoRespuesta)) return false;
        TipoRespuesta other = (TipoRespuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cabeceraRespuesta==null && other.getCabeceraRespuesta()==null) || 
             (this.cabeceraRespuesta!=null &&
              this.cabeceraRespuesta.equals(other.getCabeceraRespuesta()))) &&
            ((this.infoRespuesta==null && other.getInfoRespuesta()==null) || 
             (this.infoRespuesta!=null &&
              this.infoRespuesta.equals(other.getInfoRespuesta()))) &&
            ((this.infoDispersionResp==null && other.getInfoDispersionResp()==null) || 
             (this.infoDispersionResp!=null &&
              java.util.Arrays.equals(this.infoDispersionResp, other.getInfoDispersionResp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCabeceraRespuesta() != null) {
            _hashCode += getCabeceraRespuesta().hashCode();
        }
        if (getInfoRespuesta() != null) {
            _hashCode += getInfoRespuesta().hashCode();
        }
        if (getInfoDispersionResp() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInfoDispersionResp());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInfoDispersionResp(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoRespuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoRespuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabeceraRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "cabeceraRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "TipoCabeceraSolicitud"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/", "TipoInfoRespuesta"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoDispersionResp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/compra/", "infoDispersionResp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.rbm.com.co/esb/comercio/", "TipoInfoDispersionResp"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
