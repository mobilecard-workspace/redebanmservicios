package com.addcel.redeban.ws.servicios;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.redeban.services.RedebanCancelacionService;
import com.addcel.redeban.services.RedebanCompraService;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompra;
import com.addcel.redeban.ws.servicios.vo.SolicitudCompraRespuesta;
import com.google.gson.Gson;

public class RedebanMulticolo {
	private static final Logger logger = LoggerFactory.getLogger(RedebanMulticolo.class);

	public SolicitudCompraRespuesta compraProcesar(SolicitudCompra solicitudCompra){
		SolicitudCompraRespuesta compraProRes = null;
		RedebanCompraService redebanMCService = null;
		
		Gson gson = new Gson();
		try{
			logger.info("Dentro del servicio compraProcesar...");
			redebanMCService = new RedebanCompraService();
			System.out.println("**********************************************************");
			System.out.println("Gson: " + gson.toJson(solicitudCompra));
			System.out.println("**********************************************************");
			
			compraProRes = redebanMCService.compraProcesar(solicitudCompra);
		}catch(Exception e){
			e.printStackTrace();
		}
	    
		return compraProRes;
	}
	
//	public CompraProcesarRespuesta compraReversar(SolicitudCompra solicitudCompra){
//		CompraProcesarRespuesta compraProRes = null;
//		RedebanCompraService redebanMCService = null;
//		try{
//			logger.info("Dentro del servicio compraReversar...");
//			redebanMCService = new RedebanCompraService();
////			compraProRes = redebanMCService.compraProcesar(solicitudCompra);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	    
//		return compraProRes;
//	}
	
	public SolicitudCompraRespuesta compraCancelar(SolicitudCompra solicitudCompra){
		SolicitudCompraRespuesta compraProRes = null;
		RedebanCancelacionService redebanService = null;
		Gson gson = new Gson();
		try{
			logger.info("Dentro del servicio compraCancelar...");
			redebanService = new RedebanCancelacionService();
			
			System.out.println("**********************************************************");
			System.out.println("Gson: " + gson.toJson(solicitudCompra));
			System.out.println("**********************************************************");
			
			compraProRes = redebanService.compraCancelarProcesar(solicitudCompra);
		}catch(Exception e){
			e.printStackTrace();
		}
	    
		return compraProRes;
	}
}
