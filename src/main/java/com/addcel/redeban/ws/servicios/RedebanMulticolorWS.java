package com.addcel.redeban.ws.servicios;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.redeban.services.PagoDirectoService;
import com.addcel.redeban.services.RedebanCancelacionService;
import com.addcel.redeban.services.RedebanService;
import com.addcel.redeban.utils.Constantes;

public class RedebanMulticolorWS {
	
	private static final Logger logger = LoggerFactory.getLogger(RedebanMulticolorWS.class);

	public String compraProcesar(String parameter){
		RedebanService service = null;
		try{
			logger.info("Dentro del servicio /RedebanMulticolorWS/compraProcesar");
			service = new RedebanService();
			parameter = service.compraProcesar(parameter);
		}catch(Exception e){
			e.printStackTrace();
		}
	    
		return parameter;
	}
	
	
	public String compraCancelar(String parameter){
		RedebanCancelacionService redebanService = null;
		try{
			logger.info("Dentro del servicio compraCancelar...");
			redebanService = new RedebanCancelacionService();
			parameter = redebanService.compraCancelarProcesar(parameter);
		}catch(Exception e){
			e.printStackTrace();
		}
		return parameter;
	}
	
	public String compraDirecta(String parameter){
		logger.info(Constantes.LOG_INICIA_COMPRA_DIRECTA);
		PagoDirectoService service = null;
		try{
			service = new PagoDirectoService();
			parameter = service.compraDirecta(parameter);
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.info(Constantes.LOG_FIN_COMPRA_DIRECTA);
		return parameter;
	}
}
