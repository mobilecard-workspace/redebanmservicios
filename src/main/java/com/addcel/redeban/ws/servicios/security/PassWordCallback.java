package com.addcel.redeban.ws.servicios.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.redeban.utils.Constantes;

public class PassWordCallback implements CallbackHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(PassWordCallback.class);

	@SuppressWarnings("unused")
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		logger.info(Constantes.PROCESO_VALIDACION_ACCESO_WS); 
		for (int i = 0; i < callbacks.length; i++) {  
            WSPasswordCallback pwcb = (WSPasswordCallback)callbacks[i]; 
            if(logger.isDebugEnabled()){
            	logger.debug(Constantes.LOG_ASTERISCO);
        		logger.debug(Constantes.LOG_USER_LABEL + pwcb.getIdentifier());
        		logger.debug(Constantes.LOG_USER_PASSWORD_LABEL + Constantes.LOG_PASS_MASK);
        		logger.debug(Constantes.LOG_ASTERISCO);
            }
            if(pwcb.getIdentifier().equals("testRedebanMServicios") && pwcb.getPassword().equals("testRedebanMServicios2014")) {  
            	logger.info(Constantes.LOG_USUARIO_AUTENTICADO_EXITO+"["+pwcb.getIdentifier()+"]");
                return;  
            } else {
            	logger.error(Constantes.LOG_USUARIO_AUTENTICADO_ERROR+"["+pwcb.getIdentifier()+"]"); 
                throw new UnsupportedCallbackException(callbacks[i], "fallo de autenticación");  
            }  
        }  
		
	}
}