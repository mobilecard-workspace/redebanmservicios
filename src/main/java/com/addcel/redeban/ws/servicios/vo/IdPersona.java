package com.addcel.redeban.ws.servicios.vo;

public class IdPersona {
 
	private String tipoDocumento;

    private long numDocumento;

    public IdPersona() {
    }

    public IdPersona(
           String tipoDocumento,
           long numDocumento) {
           this.tipoDocumento = tipoDocumento;
           this.numDocumento = numDocumento;
    }


    /**
     * Gets the tipoDocumento value for this TipoIdPersona.
     * 
     * @return tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }


    /**
     * Sets the tipoDocumento value for this TipoIdPersona.
     * 
     * @param tipoDocumento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }


    /**
     * Gets the numDocumento value for this TipoIdPersona.
     * 
     * @return numDocumento
     */
    public long getNumDocumento() {
        return numDocumento;
    }


    /**
     * Sets the numDocumento value for this TipoIdPersona.
     * 
     * @param numDocumento
     */
    public void setNumDocumento(long numDocumento) {
        this.numDocumento = numDocumento;
    }

}
