package com.addcel.redeban.ws.servicios.vo;

public class IdTarjeta {
 
	private String franquicia;

    private java.lang.String numTarjeta;

    private String fechaExpiracion;

    private java.lang.String codVerificacion;

    public IdTarjeta() {
    }

    /**
     * Gets the franquicia value for this TipoIdTarjetaCredito.
     * 
     * @return franquicia
     */
    public String getFranquicia() {
        return franquicia;
    }


    /**
     * Sets the franquicia value for this TipoIdTarjetaCredito.
     * 
     * @param franquicia
     */
    public void setFranquicia(String franquicia) {
        this.franquicia = franquicia;
    }


    /**
     * Gets the numTarjeta value for this TipoIdTarjetaCredito.
     * 
     * @return numTarjeta
     */
    public java.lang.String getNumTarjeta() {
        return numTarjeta;
    }


    /**
     * Sets the numTarjeta value for this TipoIdTarjetaCredito.
     * 
     * @param numTarjeta
     */
    public void setNumTarjeta(java.lang.String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }


    /**
     * Gets the fechaExpiracion value for this TipoIdTarjetaCredito.
     * 
     * @return fechaExpiracion
     */
    public String getFechaExpiracion() {
        return fechaExpiracion;
    }


    /**
     * Sets the fechaExpiracion value for this TipoIdTarjetaCredito.
     * 
     * @param fechaExpiracion
     */
    public void setFechaExpiracion(String fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }


    /**
     * Gets the codVerificacion value for this TipoIdTarjetaCredito.
     * 
     * @return codVerificacion
     */
    public java.lang.String getCodVerificacion() {
        return codVerificacion;
    }


    /**
     * Sets the codVerificacion value for this TipoIdTarjetaCredito.
     * 
     * @param codVerificacion
     */
    public void setCodVerificacion(java.lang.String codVerificacion) {
        this.codVerificacion = codVerificacion;
    }

}
