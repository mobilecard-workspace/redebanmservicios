package com.addcel.redeban.ws.servicios.vo;

public class InfoAutenticacion {
    private byte[] clave;

    private String formatoClave;

    public InfoAutenticacion() {
    }

    public InfoAutenticacion(
           byte[] clave,
           String formatoClave) {
           this.clave = clave;
           this.formatoClave = formatoClave;
    }


    /**
     * Gets the clave value for this TipoInfoAutenticacion.
     * 
     * @return clave
     */
    public byte[] getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this TipoInfoAutenticacion.
     * 
     * @param clave
     */
    public void setClave(byte[] clave) {
        this.clave = clave;
    }


    /**
     * Gets the formatoClave value for this TipoInfoAutenticacion.
     * 
     * @return formatoClave
     */
    public String getFormatoClave() {
        return formatoClave;
    }


    /**
     * Sets the formatoClave value for this TipoInfoAutenticacion.
     * 
     * @param formatoClave
     */
    public void setFormatoClave(String formatoClave) {
        this.formatoClave = formatoClave;
    }

    
}
