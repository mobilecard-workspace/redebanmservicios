package com.addcel.redeban.ws.servicios.vo;

public class InfoCompra {
	
	private int idProducto;
	
	private int idServicio;
	
	private String concepto;

	private double montoTotal;
	
	private double comicion;
	
	private double subtotal;

    private InfoImpuestos infoImpuestos;
    
    private String referencia;
    
    private String moneda;

    private int cantidadCuotas;
    
    private int tipo;
    
    private double comision;

	public double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public InfoImpuestos getInfoImpuestos() {
		return infoImpuestos;
	}

	public void setInfoImpuestos(InfoImpuestos infoImpuestos) {
		this.infoImpuestos = infoImpuestos;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public int getCantidadCuotas() {
		return cantidadCuotas;
	}

	public void setCantidadCuotas(int cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the comicion
	 */
	public double getComicion() {
		return comicion;
	}

	/**
	 * @param comicion the comicion to set
	 */
	public void setComicion(double comicion) {
		this.comicion = comicion;
	}

	/**
	 * @return the subtotal
	 */
	public double getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

}
