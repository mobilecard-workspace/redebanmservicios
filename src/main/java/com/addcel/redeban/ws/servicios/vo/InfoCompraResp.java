package com.addcel.redeban.ws.servicios.vo;

public class InfoCompraResp {

	private java.util.Calendar fechaTransaccion;

    private java.util.Date fechaPosteo;

    private int numAprobacion;

    private double costoTransaccion;

    public InfoCompraResp() {
    }


	public java.util.Calendar getFechaTransaccion() {
		return fechaTransaccion;
	}


	public void setFechaTransaccion(java.util.Calendar fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}


	public java.util.Date getFechaPosteo() {
		return fechaPosteo;
	}


	public void setFechaPosteo(java.util.Date fechaPosteo) {
		this.fechaPosteo = fechaPosteo;
	}


	public int getNumAprobacion() {
		return numAprobacion;
	}


	public void setNumAprobacion(int numAprobacion) {
		this.numAprobacion = numAprobacion;
	}


	public double getCostoTransaccion() {
		return costoTransaccion;
	}


	public void setCostoTransaccion(double costoTransaccion) {
		this.costoTransaccion = costoTransaccion;
	}

}
