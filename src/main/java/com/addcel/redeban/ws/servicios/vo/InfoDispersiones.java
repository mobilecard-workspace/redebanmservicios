package com.addcel.redeban.ws.servicios.vo;

public class InfoDispersiones {

	private int idComercio;
	private int idDispersion;
	private String idAdquiriente;
	private String idTerminal;
	private double montoTotal;
	private double montoImpuesto;
	private String tipoImpuesto;
	/**
	 * @return the idComercio
	 */
	public int getIdComercio() {
		return idComercio;
	}
	/**
	 * @param idComercio the idComercio to set
	 */
	public void setIdComercio(int idComercio) {
		this.idComercio = idComercio;
	}
	/**
	 * @return the idDispersion
	 */
	public int getIdDispersion() {
		return idDispersion;
	}
	/**
	 * @param idDispersion the idDispersion to set
	 */
	public void setIdDispersion(int idDispersion) {
		this.idDispersion = idDispersion;
	}
	/**
	 * @return the idAdquiriente
	 */
	public String getIdAdquiriente() {
		return idAdquiriente;
	}
	/**
	 * @param idAdquiriente the idAdquiriente to set
	 */
	public void setIdAdquiriente(String idAdquiriente) {
		this.idAdquiriente = idAdquiriente;
	}
	/**
	 * @return the idTerminal
	 */
	public String getIdTerminal() {
		return idTerminal;
	}
	/**
	 * @param idTerminal the idTerminal to set
	 */
	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}
	/**
	 * @return the montoTotal
	 */
	public double getMontoTotal() {
		return montoTotal;
	}
	/**
	 * @param montoTotal the montoTotal to set
	 */
	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}
	/**
	 * @return the montoImpuesto
	 */
	public double getMontoImpuesto() {
		return montoImpuesto;
	}
	/**
	 * @param montoImpuesto the montoImpuesto to set
	 */
	public void setMontoImpuesto(double montoImpuesto) {
		this.montoImpuesto = montoImpuesto;
	}
	/**
	 * @return the tipoImpuesto
	 */
	public String getTipoImpuesto() {
		return tipoImpuesto;
	}
	/**
	 * @param tipoImpuesto the tipoImpuesto to set
	 */
	public void setTipoImpuesto(String tipoImpuesto) {
		this.tipoImpuesto = tipoImpuesto;
	}
    
    
	
}
