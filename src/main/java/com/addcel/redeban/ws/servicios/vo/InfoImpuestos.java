package com.addcel.redeban.ws.servicios.vo;

public class InfoImpuestos {

	private double iva;
	private double ivaValor;
    private double devolucionIVA;
    private double montoAdicional;
    private double descuento;
    private double consumo;
    
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public double getIvaValor() {
		return ivaValor;
	}
	public void setIvaValor(double ivaValor) {
		this.ivaValor = ivaValor;
	}
	public double getDevolucionIVA() {
		return devolucionIVA;
	}
	public void setDevolucionIVA(double devolucionIVA) {
		this.devolucionIVA = devolucionIVA;
	}
	public double getMontoAdicional() {
		return montoAdicional;
	}
	public void setMontoAdicional(double montoAdicional) {
		this.montoAdicional = montoAdicional;
	}
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public double getConsumo() {
		return consumo;
	}
	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}
	
    

}
