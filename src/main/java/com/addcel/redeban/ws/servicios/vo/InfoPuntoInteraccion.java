package com.addcel.redeban.ws.servicios.vo;

import java.util.List;

import com.addcel.redeban.model.vo.AbstractVO;

public class InfoPuntoInteraccion  extends AbstractVO{
	
    private String tipoTerminal;
    private String idTerminal;
    private String idAdquiriente;
    private long idTransaccionTerminal;
    private String modoCapturaPAN;
    private String capacidadPIN;
    private String tipoTarjeta;								// Credito, Debito, Credito Privada, Debito privada
    private String url; 
    private int idIntegrador;
    private int tipoTransaccion;
    
    private long idTransaccion;
    private String franquicia;
    private String numTarjeta;
    private int cuotas;
    private double montoTotal;
    private double montoImpuesto;
    private double baseImpuesto;
    private String referencia;    
    
    private List<InfoDispersiones> dispersiones;
    
    private String numAprobacion;
    
    private String nombreEmpresa;
    
    private String entidadAdquiriente;
    
    private String numeroReferencia;
    
    private int validaDatacredito;
    
    private String fechaVigencia;
    
    private String codigoSeguridad;
    

	public String getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getTipoTerminal() {
		return tipoTerminal;
	}

	public void setTipoTerminal(String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

	public String getIdTerminal() {
		return idTerminal;
	}

	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}

	public String getIdAdquiriente() {
		return idAdquiriente;
	}

	public void setIdAdquiriente(String idAdquiriente) {
		this.idAdquiriente = idAdquiriente;
	}

	public long getIdTransaccionTerminal() {
		return idTransaccionTerminal;
	}

	public void setIdTransaccionTerminal(long idTransaccionTerminal) {
		this.idTransaccionTerminal = idTransaccionTerminal;
	}

	public String getModoCapturaPAN() {
		return modoCapturaPAN;
	}

	public void setModoCapturaPAN(String modoCapturaPAN) {
		this.modoCapturaPAN = modoCapturaPAN;
	}

	public String getCapacidadPIN() {
		return capacidadPIN;
	}

	public void setCapacidadPIN(String capacidadPIN) {
		this.capacidadPIN = capacidadPIN;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getIdIntegrador() {
		return idIntegrador;
	}

	public void setIdIntegrador(int idIntegrador) {
		this.idIntegrador = idIntegrador;
	}

	public int getTipoTransaccion() {
		return tipoTransaccion;
	}

	public void setTipoTransaccion(int tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public List<InfoDispersiones> getDispersiones() {
		return dispersiones;
	}

	public void setDispersiones(List<InfoDispersiones> dispersiones) {
		this.dispersiones = dispersiones;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public int getCuotas() {
		return cuotas;
	}

	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}

	public double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public double getMontoImpuesto() {
		return montoImpuesto;
	}

	public void setMontoImpuesto(double montoImpuesto) {
		this.montoImpuesto = montoImpuesto;
	}

	public double getBaseImpuesto() {
		return baseImpuesto;
	}

	public void setBaseImpuesto(double baseImpuesto) {
		this.baseImpuesto = baseImpuesto;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNumAprobacion() {
		return numAprobacion;
	}

	public void setNumAprobacion(String numAprobacion) {
		this.numAprobacion = numAprobacion;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getEntidadAdquiriente() {
		return entidadAdquiriente;
	}

	public void setEntidadAdquiriente(String entidadAdquiriente) {
		this.entidadAdquiriente = entidadAdquiriente;
	}

	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	public int getValidaDatacredito() {
		return validaDatacredito;
	}

	public void setValidaDatacredito(int validaDatacredito) {
		this.validaDatacredito = validaDatacredito;
	}

 }
