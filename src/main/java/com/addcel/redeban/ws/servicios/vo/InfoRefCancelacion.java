package com.addcel.redeban.ws.servicios.vo;

public class InfoRefCancelacion {
    private String numAprobacion;

    private long idTransaccionAutorizador;

    public InfoRefCancelacion() {
    }

    public InfoRefCancelacion(
           String numAprobacion,
           long idTransaccionAutorizador) {
           this.numAprobacion = numAprobacion;
           this.idTransaccionAutorizador = idTransaccionAutorizador;
    }


    /**
     * Gets the numAprobacion value for this TipoInfoRefCancelacion.
     * 
     * @return numAprobacion
     */
    public String getNumAprobacion() {
        return numAprobacion;
    }


    /**
     * Sets the numAprobacion value for this TipoInfoRefCancelacion.
     * 
     * @param numAprobacion
     */
    public void setNumAprobacion(String numAprobacion) {
        this.numAprobacion = numAprobacion;
    }


    /**
     * Gets the idTransaccionAutorizador value for this TipoInfoRefCancelacion.
     * 
     * @return idTransaccionAutorizador
     */
    public long getIdTransaccionAutorizador() {
        return idTransaccionAutorizador;
    }


    /**
     * Sets the idTransaccionAutorizador value for this TipoInfoRefCancelacion.
     * 
     * @param idTransaccionAutorizador
     */
    public void setIdTransaccionAutorizador(long idTransaccionAutorizador) {
        this.idTransaccionAutorizador = idTransaccionAutorizador;
    }

}
