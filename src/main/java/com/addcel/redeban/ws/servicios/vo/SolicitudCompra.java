package com.addcel.redeban.ws.servicios.vo;

import com.addcel.redeban.model.vo.TransaccionRespuesta;
import com.addcel.redeban.model.vo.UsuarioVO;

public class SolicitudCompra {
	
	private long idUsuario;
	private String login;
	private String password;
	private String cvv2;
	private int idAplicacion;
	private String plataforma;
	private String dispositivo;
	private InfoPuntoInteraccion infoPuntoInteraccion;
    private IdPersona idPersona;
    private InfoMedioPago infoMedioPago;
    private InfoCompra infoCompra;
    private InfoRefCancelacion infoRefCancelacion;
    
    private String respuesta;
    
    private int idIntegrador;
    
    private long idTransaccion;
    
    private String codRespuesta;
    
    private int idTipoOperacion;
    
    private int idUsuarioComercio;
    
    private String referencia;
    
    private String referenciaServicio;
    
    private int idServicio;
    
    private TransaccionRespuesta trans;
    
    private UsuarioVO usuario;
    

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}

	public InfoPuntoInteraccion getInfoPuntoInteraccion() {
		return infoPuntoInteraccion;
	}

	public void setInfoPuntoInteraccion(InfoPuntoInteraccion infoPuntoInteraccion) {
		this.infoPuntoInteraccion = infoPuntoInteraccion;
	}

	public IdPersona getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(IdPersona idPersona) {
		this.idPersona = idPersona;
	}

	public InfoMedioPago getInfoMedioPago() {
		return infoMedioPago;
	}

	public void setInfoMedioPago(InfoMedioPago infoMedioPago) {
		this.infoMedioPago = infoMedioPago;
	}

	public InfoCompra getInfoCompra() {
		return infoCompra;
	}

	public void setInfoCompra(InfoCompra infoCompra) {
		this.infoCompra = infoCompra;
	}

	public InfoRefCancelacion getInfoRefCancelacion() {
		return infoRefCancelacion;
	}

	public void setInfoRefCancelacion(InfoRefCancelacion infoRefCancelacion) {
		this.infoRefCancelacion = infoRefCancelacion;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public int getIdIntegrador() {
		return idIntegrador;
	}

	public void setIdIntegrador(int idIntegrador) {
		this.idIntegrador = idIntegrador;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getCodRespuesta() {
		return codRespuesta;
	}

	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	public int getIdTipoOperacion() {
		return idTipoOperacion;
	}

	public void setIdTipoOperacion(int idTipoOperacion) {
		this.idTipoOperacion = idTipoOperacion;
	}

	public int getIdUsuarioComercio() {
		return idUsuarioComercio;
	}

	public void setIdUsuarioComercio(int idUsuarioComercio) {
		this.idUsuarioComercio = idUsuarioComercio;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getReferenciaServicio() {
		return referenciaServicio;
	}

	public void setReferenciaServicio(String referenciaServicio) {
		this.referenciaServicio = referenciaServicio;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public TransaccionRespuesta getTrans() {
		return trans;
	}

	public void setTrans(TransaccionRespuesta trans) {
		this.trans = trans;
	}

	public UsuarioVO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}


}
