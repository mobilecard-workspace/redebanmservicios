
package com.addcel.redeban.ws.servicios.vo;

import com.addcel.redeban.model.vo.AbstractVO;

public class SolicitudCompraRespuesta extends AbstractVO {
	
	private long idTransaccion;
    
    private String codRespuesta;

    private String descRespuesta;

    private String estado;

    private String fechaTransaccion;

    private String fechaPosteo;

    private String numAprobacion;

    private double costoTransaccion;

    private Long idTransaccionAutorizador;
    
    private double montoTotal;
    
    private double comision; 
    
    private int idIntegrador;
    
    private long idUsuario;

	public String getCodRespuesta() {
		return codRespuesta;
	}

	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	public String getDescRespuesta() {
		return descRespuesta;
	}

	public void setDescRespuesta(String descRespuesta) {
		this.descRespuesta = descRespuesta;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(String fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public String getFechaPosteo() {
		return fechaPosteo;
	}

	public void setFechaPosteo(String fechaPosteo) {
		this.fechaPosteo = fechaPosteo;
	}

	public String getNumAprobacion() {
		return numAprobacion;
	}

	public void setNumAprobacion(String numAprobacion) {
		this.numAprobacion = numAprobacion;
	}

	public double getCostoTransaccion() {
		return costoTransaccion;
	}

	public void setCostoTransaccion(double costoTransaccion) {
		this.costoTransaccion = costoTransaccion;
	}

	public Long getIdTransaccionAutorizador() {
		return idTransaccionAutorizador;
	}

	public void setIdTransaccionAutorizador(Long idTransaccionAutorizador) {
		this.idTransaccionAutorizador = idTransaccionAutorizador;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public int getIdIntegrador() {
		return idIntegrador;
	}

	public void setIdIntegrador(int idIntegrador) {
		this.idIntegrador = idIntegrador;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

}
